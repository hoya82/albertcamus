const gulp = require('gulp');
const newer = require('gulp-newer');
const pug = require('gulp-pug');
const babel = require('gulp-babel');
const less = require('gulp-less');
const cssnano = require('gulp-cssnano');
const rimraf = require('gulp-rimraf');

// MAIN PATHS
var paths = {
  app:'app',
};


const source = {
  assets: 'assets/**/*',
  lib: 'lib',
  page: {
    pug: ['src/**/*.pug', '!src/**/*.partial.pug'],
    js: ['src/**/*.js'],
    less: ['src/**/*.less'],
    data: ['src/**/*.json', 'src/**/*.xml'],
  }
}


//---------------
// TASKS
//---------------
gulp.task('clean', function() {
  console.log("Clean up.");
  return gulp.src(paths.app + "/*")
      .pipe(rimraf({force:true}));
});

gulp.task('vendor', () => {
  console.log('Copying vendor assets.');

  return gulp.src('lib/*')
      .pipe(newer(paths.app))
      .pipe( gulp.dest(paths.app + '/vendor') );
});

gulp.task('assets', function() {
    console.log('Copying assets...');
    return gulp.src(source.assets)
      .pipe(newer(paths.app))
      .pipe(gulp.dest(paths.app + '/assets'));
});

gulp.task('build:pug', () => {
  console.log('Building pug.');
  return gulp.src(source.page.pug)
      .pipe(pug())
//      .pipe(newer(paths.app))
      .pipe(gulp.dest(paths.app));
});

gulp.task('build:js', () => {
  console.log('Building javascript.');
  return gulp.src(source.page.js)
      .pipe(newer(paths.app))
      .pipe(babel({presets: ['es2015']}))
      .pipe(gulp.dest(paths.app));
});

gulp.task('build:less', () => {
  console.log('Building less.');
  return gulp.src(source.page.less)
      .pipe(newer(paths.app))
      .pipe(less())
      .pipe(cssnano())
      .pipe(gulp.dest(paths.app));
});

gulp.task('data', () => {
  console.log('Copying json/xml data.');
  return gulp.src(source.page.data)
      .pipe(newer(paths.app))
      .pipe(gulp.dest(paths.app));
});

//-------------
// WATCH
//-------------
gulp.task('watch', () => {
  gulp.watch('src/**/*', ['build']);
});



gulp.task('build', ['vendor', 'assets', 'data', 'build:pug', 'build:js', 'build:less']);
gulp.task('default', ['build']);

