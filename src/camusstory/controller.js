window.onSvg1Loaded = function() {
  let svg1 = document.getElementById('camus-social-network-teen');
  _.each(svg1.contentDocument.getElementsByClassName('people'), (el) => {
    el.addEventListener('mouseover', function(e) {
      let index = this.getAttribute('data-index') - 0;
      let people = writers_camus_social_network_teen[index];
      let bgColor = '#ffc';

      switch(people.rel) {
        case '우호적 지인':
          bgColor = '#ffc';
          break;
        case '대립적 지인':
          bgColor = '#fcc';
          break;
      }

      $('.tooltip')
        .html(people.detail)
        .css('top', e.pageY)
        .css('left', Math.min(e.pageX, $(window).width() / 2))
        .css('background-color', bgColor)
        .css('visibility','visible')
        .addClass('animated fadeIn');
    });

    el.addEventListener('mouseout', function() {
      $('.tooltip').removeClass('animated fadeIn').css('visibility','hidden');
    });
  });
}

window.onSvg2Loaded = function() {
  let svg2 = document.getElementById('camus-family-chart');
  _.each(svg2.contentDocument.getElementsByClassName('people'), (el) => {
    el.addEventListener('mouseover', function(e) {
      svg2.contentDocument.getElementsByClassName(this.getAttribute('data-popup'))[0].style.display="";
    });

    el.addEventListener('mouseout', function() {
      _.each(svg2.contentDocument.getElementsByClassName('popup'), (el) => el.style.display="none");
    });
  });
}