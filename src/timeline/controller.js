$(() => {
  const works = window.works;

  function recalc() {
    let left = $(".timeline .works").offset().left;
    let width = $(".timeline .spacer-blue:last-child").offset().left - left;
    $(".timeline .fiction, .timeline .nonfiction, .timeline .private").remove();
    _.each(works.fiction, (i) => {

      let start = $(".timeline .year"+i.start).offset().top;
      let end = $(".timeline .year"+i.end).offset().top;

      $("<div><img src='assets/img/hexagon-g.png' style='width:1em'/><div class='time-bar'>"+i.label+"</div></div>")
        .css({top:start, left:left, height:end-start})
        .addClass("fiction")
        .on('mouseover', function() {
          $(this).css({'z-index':10});
        })
        .on('mouseout', function() {
          $(this).css({'z-index':0});
        })
        .appendTo($(".timeline"));
    });
    _.each(works.nonfiction, (i) => {

      let start = $(".timeline .year"+i.start).offset().top;
      let end = $(".timeline .year"+i.end).offset().top;

      $("<div><img src='assets/img/hexagon-r.png' style='width:1em'/><div class='time-bar'>"+i.label+"</div></div>")
        .css({top:start, left:left+(width/3), height:end-start})
        .addClass("nonfiction")
        .on('mouseover', function() {
          $(this).css({'z-index':10});
        })
        .on('mouseout', function() {
          $(this).css({'z-index':0});
        })
        .appendTo($(".timeline"));
    });
    _.each(works.private, (i) => {

      let start = $(".timeline .year"+i.start).offset().top;
      let end = $(".timeline .year"+i.end).offset().top;

      $("<div><img src='assets/img/hexagon-b.png' style='width:1em'/><div class='time-bar'>"+i.label+"</div></div>")
        .css({top:start, left:left+(width/3*2), height:end-start})
        .addClass("private")
        .on('mouseover', function() {
          $(this).css({'z-index':10});
        })
        .on('mouseout', function() {
          $(this).css({'z-index':0});
        })
        .appendTo($(".timeline"));
    });
  }

  $(window).resize(_.throttle(recalc, 500));

  recalc();
});
