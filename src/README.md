= Albert Camus - 카뮈 이야기 =

 - 홈페이지 제작 기간: 2016.09

== 개발 인원 ==
 - 개발 총괄: 김현식( wbstory@storymate.net )
 - 디자인: 안지현
 - 테스팅: 유은순
 - 자료제공: 유은순, 조병준 외 다수

== 사용 기술 ==
=== source ===
 - pug
 - less
 - ECMAScript 2015

=== build system ===
 - Node.JS
 - npm
 - gulp