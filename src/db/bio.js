window.works = {
    "fiction":[
        {
            "start":1942,
            "end":1943,
            "label":"이방인(L'Etranger)"
        },
        {
            "start":1944,
            "end":1945,
            "label":[
                "칼리굴라(Caligula)",
                "오해(Le malentendu)"
            ]
        },
        {
            "start":1947,
            "end":1948,
            "label":"페스트(La Peste)"
        },
        {
          "start":1948,
          "end":1949,
          "label":"계엄령(L'Etat de Siege)"
        },
        {
          "start":1950,
          "end":1951,
          "label":"정의의 사람들(Les Justes)"
        },
        {
          "start":1956,
          "end":1957,
          "label":"전락(La Chute)"
        },
        {
          "start":1957,
          "end":1958,
          "label":"적지와 왕국(L'Exil et Le Royaume)"
        },
        {
          "start":1971,
          "end":1994,
          "label":"행복한 죽음(La mort heureuse)"
        },
        {
          "start":1994,
          "end":1995,
          "label":"최초의 인간(La premier homme)"
        }
    ],
    "nonfiction":[
        {
            "start":1931,
            "end":1934,
            "label":"젊은 시절의 글(Le premier Camus suivi de jeunesse d'Albert Camus)"
        },
        {
            "start":1937,
            "end":1938,
            "label":"안과 겉(L'Envers et L'Endroit)"
        },
        {
            "start":1939,
            "end":1940,
            "label":"결혼(Noces)"
        },
        {
            "start":1942,
            "end":1943,
            "label":"시지프의 신화(Le mythe de Sisyphe)"
        },
        {
            "start":1943,
            "end":1944,
            "label":"독일 친구에게 보내는 편지(Lettres a un ami allemand)"
        },
        {
            "start":1944,
            "end":1948,
            "label":"시사평론(Actuelles, chroniques 1944~1948)"
        },
        {
            "start":1951,
            "end":1952,
            "label":"반항적 인간(L'homme revolte)"
        },
        {
            "start":1954,
            "end":1956,
            "label":"여름(L'Ete)"
        },
        {
            "start":1957,
            "end":1958,
            "label":"단두대의 성찰(Reflexion sur la guillotine)"
        },
        {
            "start":1958,
            "end":1959,
            "label":"스웨덴 연설(Discours de Suede)"
        }
    ],
    "private":[
        {
            "start":1932,
            "end":1960,
            "label":"장 그르니에와의 서한집(Correspondance)"
        },
        {
            "start":1935,
            "end":1942,
            "label":"작가수첩 1(Carnets 1)"
        },
        {
            "start":1942,
            "end":1951,
            "label":"작가수첩 2(Carnets 2)"
        },
        {
            "start":1951,
            "end":1958,
            "label":"작가수첩 3(Carnets 3)"
        },
        {
            "start":1946,
            "end":1947,
            "label":"여행일기 1(Journaux de voyage 1)"
        },
        {
            "start":1949,
            "end":1950,
            "label":"여행일기 2(Journaux de voyage 2)"
        }
    ]
}