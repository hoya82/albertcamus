window.books = [
{ /*********************************************************************************************/
  /**  스토리형 작품 **/
  /*********************************************************************************************/
  "worktype":"fiction",
  "category":"중‧장편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"이방인 L'Étranger",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1942,
  "language":"프랑스어",
  "t_title":"이방인",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["태양(soleil)", "빛(lumière)", "우연(hasard)", "그건 아무래도 상관없다. (cela ne signifiait rien)"],
  "keywords2":["부조리(absurde)", "반항적 인간(homme révolté)", "실존주의(existentialisme)", "낯섦(étrangeté)", "무관심(indifférence)"],
  "story":"<p>1부: 알제에서 사는 프랑스인 청년 뫼르소(Meursault)는 어느 날 양로원에 있던 어머니가 돌아가셨다는 전보를 받고 가서 장례를 치르고 돌아온다. 다음 날 그는 애인인 마리(Marie Cardona)를 만나 희극 영화를 보고 해수욕을 즐기며 사랑을 나눈다. 같은 아파트에 사는 레몽과 친해진 뫼르소는 레몽과 정부 간의 분쟁에 휩쓸려 그의 계획에 동참한다. 며칠 후 뫼르소는 레몽과 함께 해변으로 놀러갔다가 그들을 미행하던 아랍인들과 마주치게 되며 싸움이 벌어져 레몽이 다치고 소동이 마무리 되지만 뫼르소는 답답함을 느끼며 시원한 샘으로 간다. 그곳에서 우연히 레몽을 찌른 아랍인을 다시 만난 뫼르소는 그가 꺼내는 칼의 강렬한 빛에 자극을 받아 자신도 모르게 품에 있던 권총의 방아쇠를 당긴다.</p><p>2부: 뫼르소는 체포되어 교도소로 이송된다.그 후 교도소에서 5개월 가량의 시간을 보내고 법정에 선다. 양로원 원장, 문지기, 토마 페레 영감, 레몽, 마송, 살라마노, 마리 등이 증인으로 세워진다. 재판 과정에서 그들의 증언들로 인해 어머니의 죽음과 장례에 대한 뫼르소의 태도가 커다란 스캔들을 일으키게 된다. 또한 그는 자신의 범행동기를 \"모두가 태양 탓이다\" 라고 대답한다.  결국 사형이 언도되어 독방에서 형의 집행을 기다리는 뫼르소는 사제가 권하는 속죄의 기도도 거절하고 자기는 과거에나 현재에나 행복하다고 느낀다.</p>",
  "background":"<p>시간적 배경 : 연도: 1940년 (20세기 초)</p><p>- 제1부: 6월~7월 사이, 뫼르소가 전보를 받는 목요일과 살인사건이 발생하는 일요일 사이 총 18일간.</p><p>- 제2부: 거의 1년에 걸쳐 있음. 하지만 실제로 서술된 이야기의 내용은 6, 7월</p><p>공간적 배경 : 북아프리카 알제. 양로원(마랑고), 벨쿠르 거리(뫼르소가 살고 있는 변두리 동네), 알제의 교외(살인 사건이 일어난 해변), 셀레스트네 식당, 형무소, 감방.</p><p>사회, 역사적 배경 : 알제리가 프랑스의 식민지였던 시대. 제2차 세계대전 후.</p>",
  "character_net":{
    "chart":"etranger.png",
    "character":{
      "뫼르소":"<p>소설에서의 역할: 주인공, 화자, 내레이터</p>사회 계급: 알제에 거주하는 프랑스인, 선박 중개 사무소에서 일하는 고용인</p><p>성격: 어떤 일이 있어도 거짓을 말하지 않고 감정표현을 절제한다. 감각적, 본능적, 육체적인 것에 민감하다. 전통적 관습에 대해 무관심하다. 주변 인물, 사건들에 어떠한 의미를 부여하지 않는다. 세상의 작위적인 개연성을 부정하고 우연성을 강조한다. 타인에게 낯설음을 느끼는 ‘이방인’으로서의 자의식을 가진다.</p>",
      "레몽 생테스":"<p>뫼르소와 같은 층에 살고 있다. 자신의 정부와 얽힌 일에 뫼르소를 끌어들여 그에게 편지를 써 달라, 경찰서에서 자신에게 유리한 증언을 해달라는 등의 부탁을 한다. 후에 친구인 마송의 별장에 뫼르소와 함께 놀러가지만 정부의 남자형제인 아랍인 무리와 얽히고 만다. 후에 뫼르소의 재판에 증인으로 선다. 뫼르소는 자신의 친구이며 결백하고 그가 해변에 있게 된 것은 우연이었다고 증언한다.</p>",
      "마송":"<p>레몽과 친구 사이.알제 교외에 작은 별장을 가지고 있다. 후에 뫼르소의 재판에 증인으로 선다. 뫼르소가 정직하고 솔직한 사람이라고 증언한다.</p>",
      "살라마노":"<p>뫼르소와 같은 층에 살고 있는 노인. 피부병에 걸린 개를 8년 째 키우고 있다. 개를 마구 때리곤 했으나 막상 개가 사라지자 깊은 슬픔에 빠진다. 후에 뫼르소의 재판에 증인으로 선다. 뫼르소가 자신의 개 일로 잘 대해주었으며 뫼르소를 이해해 주셔야 한다고 증언한다.</p>",
      "셀레스트":"<p>뫼르소가 자주 가는 식당의 주인. 후에 뫼르소의 재판에 증인으로 선다. 뫼르소의 살인에 대해 그것은 하나의 불운이라고 되풀이해서 증언한다.</p>",
      "마리 카르도나":"<p>뫼르소와 함께 일했던 타이피스트이며 그와 연인사이. 뫼르소가 자신을 사랑하는지 확인하고 싶어 하며 그와 결혼하기를 바란다. 후에 뫼르소의 재판에 증인으로 선다. 장례식 바로 다음날 뫼르소와 함께 해수욕을 하고 영화를 봤다고 했으며 울음을 터트리며 그는 잘못이 없다고 증언한다.</p>",
      "아랍인":"<p>레몽 정부의 남자형제와 같은 무리의 일원이며 뫼르소가 쏜 5발의 총알을 맞고 사망한다.</p>",
      "양로원 원장":"<p>빈소로 뫼르소를 안내하고 장례절차를 그에게 설명해준다. 뫼르소 어머니의 장례식에 참여하며 후에 뫼르소의 재판에 증인으로 선다. 뫼르소가 장례식 날 보인 침착함 때문에 놀랐으며 그가 엄마를 보려하지도 않고 울지도 않았다고 증언한다. </p>",
      "문지기":"<p>64세에 파리 출신. 빈소에서 장례식을 기다리며 뫼르소와 담배를 피고 사람들에게 커피를 제공한다. 후에 뫼르소의 재판에 증인으로 선다. 뫼르소가 엄마를 보려 하지 않았으며 담배를 피우고 잠을 잤으며 커피를 마셨다고 증언한다.</p>",
      "토마 페레":"<p>뫼르소 어머니의 친구. 그녀의 장례식에 참여하며 후에 뫼르소의 재판에 증인으로 선다. 장례식 날 너무 슬퍼서 다른 데 신경 쓸 겨를이 없었지만 뫼르소가 우는 모습을 봤냐는 질문에 못 봤다고 증언한다.</p>",
      "변호사":"<p>뫼르소의 사건을 맡아 그의 변호를 한다.</p>",
      "예심 판사":"<p>뫼르소의 살인행위에 의문을 가지고 그가 종교적으로 회개할 것을 강요한다. 뫼르소가 그에 동요하지 않자 더 이상 강요하지 않고 그의 진술을 구체화하는 데 도움을 준다.</p>",
      "형무소 부속사제":"<p>사형을 앞둔 뫼르소가 신을 받아들이기를 강요한다.</p>"
    }
  },
  "style":"<p>- 1인칭</p><p>- 단문으로 쓰여짐, 간결함, 종속절을 최대한 피하고 있음, 건조함, 주인공의 행동과 감각 서술 중심, 직접 경험한 사실들을 서사적으로 서술．</p>",
  "network1":`<div>
    <p>1. 시지프 신화 Le Mythe de Sisyphe, Albert Camus, 1942.<br/>
    - <이방인>과 유사하게 인간 실존과 부조리에 대한 성찰을 다룬다.<br/>
    - <a href='http://terms.naver.com/entry.nhn?docId=2012359&cid=41773&categoryId=44397' target='_blank'>[네이버 지식백과] 시지프의 신화 [Le Mythe de Sisyphe] (낯선 문학 가깝게 보기 : 프랑스문학, 2013. 11., 인문과교양)</a></p>
    <p>2. 행복한 죽음 La Mort heureuse, Albert Camus, 1971.<br/>
    - &lt;이방인&gt;을 쓰기 직전에 알베르 카뮈는 그의 첫 번째 소설인 &lt;행복한 죽음&gt;을 썼다. 그러나 그는 자신의 첫 번째 소설인 &lt;행복한 죽음&gt;의 집필을 중단하고, <이방인>을 썼다. <이방인>이 탄생할 수 있게 한 <행복한 죽음>은 카뮈가 죽은 뒤에 유작으로 세상에 출판되었다.<br />
    - &lt;행복한 죽음&gt;에서의 주인공인 ‘메르소(Mersault)’와 &lt;이방인&gt;의 ‘뫼르소(Meursault)’는 이름이 유사하며 살인을 기점으로 인생이 크게 변화한다는 공통점을 가지고 있다.<br/>
    - <a href='https://fr.wikipedia.org/wiki/La_Mort_heureuse' target='_blank'&lt;행복한 죽음&gt;에 대한 위키피디아 사이트</p>
  <div>`,
  "network2":`<div>
   <p> 1. Meursault, contre-enquête, ‘카멜 다우어(Kamel Daoud)’, éditions Barzakh, 2013 (ISBN 978-9931-325-56-7) et Actes Sud 2014 (ISBN 978-2330033729)<br/>
    - 이 소설의 화자는 &lt;이방인&gt;에 등장하는 ‘아랍인’의 형제이며 그의 관점에서 바라본 사건을 서술한다.<br/>
    -  <a href='http://www.babelio.com/livres/Daoud-Meursault-contre-enquete/573120' target='_blank'>‘카멜 다우어(Kamel Daoud)’의 소설 'Meursault, contre-enquête'(2013)에 대한 프랑스 책사이트 &lt;바벨리오(babelio)&gt;의 소개')</a></p>
   <p> 2. La Joie, Charles Pépin, éditions Allary, 2015.<br/>
    - &lt;이방인&gt;과 같은 줄거리지만 배경은 2000년대이다.<br/>
    - <a href='http://www.babelio.com/livres/Pepin-La-joie/677170' target='_blank'> ‘샤를 페펭(Charles Pépin)’의 소설 'La Joie'(2015)에 대한 프랑스 책사이트 &lt;바벨리오(babelio)&gt;의 소개')  </a></p>
  </div>`,
  "network3":`<div>
    <h2>만화</h2>
    <p>1. José Muñoz　(Éditions Futuropolis), 2012　(ISBN 9782754807685)　:　«Le roman dans son texte intégral accompagné de plus de 50　illustrations.»<br/>
    - 삽화가 들어간 &lt;이방인&gt;<br/>
    - <a href='http://www.gallimard.fr/Catalogue/FUTUROPOLIS/Albums/L-etranger' target='_blank'>‘José Muñoz’의 만화에 대한 프랑스 출판사 &lt;갈리마드(gallimard)&gt;의 소개')</a></p>
   <p> 2. Jacques Ferrandez (Éditions Gallimard BD), 2013 (ISBN 9782070645183) : « Jacques Ferrandez en offre une relecture […] en bande dessinée. »<br/>
    - 삽화가 들어간 &lt;이방인&gt;<br/>
    - <a href='http://www.telerama.fr/livre/bd-l-etranger-le-roman-d-albert-camus-adapte-par-jacques-ferrandez,96095.php' target='_blank'>‘Jacques Ferrandez’의 만화에 대한 프랑스 보도사이트 &lt;뗄레라마(télérama)&gt;의 소개')</a></p>

    <h2>무용</h2>
    <p>“L’Étranger.” Jean-Claude Gallotta, 2015.<br/>
    - &lt;이방인&gt;을 소재로 한 무용.<br/>
    - <a href='http://www.gallotta-danse.com/L-Etranger' target='_blank'>‘Jean-Claude Gallotta’의 공연 ‘L'Étranger’의 공식 소개페이지.</a></p>

    <h2>뮤지컬</h2>
    <p>Albert Camus lit l’Étranger Remix (Hélice Productions), Pierre de Mûelenaere, Orchid Bite Visuals. 2008 - 2014,<br/>
    - &lt;이방인&gt;을 소재로 한 뮤지컬.<br/>
    - <a href='http://heliceproductions.net/camusremix' target='_blank'>‘Pierre de Mûelenaere’. ‘les VJ Orchid Bites qui’의 공연, ‘Albert Camus lit l’Étranger Remix‘의 공식 소개페이지.</a></p>

    <h2>영화</h2>
    <p>1.　‘Lo Straniero’, ‘L’Étranger’ (이방인)，　Luchino Visconti(루치노 비스콘티)．　1967.<br/>
    - &lt;이방인&gt;을 각색한 영화로 비스콘티는 처음 이 책이 출간된 이래로 작품에 매료되었고, 1960년에 카뮈가 사망한 후 소설의 영화화를 시도했다.<br/>
    - <a href='https://fr.wikipedia.org/wiki/L%27%C3%89tranger_(film,_1967)' target='_blank'>Luchino Visconti(루치노 비스콘티)의 영화 ‘Lo Straniero’, ‘L’Étranger‘(이방인)，에 대한 위키피디아(wikipedia) 페이지.</a><br/>
    - <a href='http://www.allocine.fr/film/fichefilm_gen_cfilm=27797.html' target='_blank'>Luchino Visconti(루치노 비스콘티)의 영화 ‘Lo Straniero’, ‘L’Étranger‘(이방인)，에 대한 프랑스 영화사이트 &lt;알로 시네(AlloCiné)&gt;의 소개.</a><br/>
    - <a href='http://www.cinematheque.seoul.kr/rgboard/addon.php?file=filmdb.php&md=read&no=2513' target='_blank'>Luchino Visconti(루치노 비스콘티)의 영화 ‘Lo Straniero’, ‘L’Étranger‘(이방인)，에 대한 한국시네마테크협의회의 소개.</a></p>
    <p>2. “The Barber”, frères Coen．2001<br/>
    - &lt;이방인&gt;과 유사하게 인간 실존의 문제를 다룬 영화.<br/>
    - <a href='http://www.allocine.fr/film/fichefilm_gen_cfilm=29117.html' target='_blank'>코엔 형제(frères Coen)의 영화 '이발사(The Barber)'(2001)에 대한 프랑스 영화사이트 &lt;알로 시네(AlloCiné)&gt;의 소개.</a></p>

    <h2>음악</h2>
    <p>1. ‘Killing an Arab.’ - the Cure. 1978.<br/>
    해변에서 뫼르소가 아랍인을 살해하는 장면을 묘사.<br/>
    - <a href='https://en.wikipedia.org/wiki/Killing_an_Arab' target='_blank'>더 큐어(the Cure)의 ‘Killing an Arab.’에 대한 위키피디아(wikipedia) 페이지.</a><br/>
    - <a href='https://youtu.be/SdbLqOXmJ04' target='_blank'>더 큐어(the Cure)의 ‘Killing an Arab.’에 대한 유튜브(Youtube) 페이지.</a></p>
    <p>2. ‘l'etranger (gigue existentielle)’ - Tuxedomoon. 1982.<br/>
    - 엄마의 장례식에서 울지 않은 자신을 이상하다 생각하는 다른 이들에게 그건 자신의 잘못이 아니라고 한다.<br/>
    - <a href='https://www.youtube.com/watch?v=pLyHayWGp_c' target='_blank'>턱시도문(Tuxedomoon)의 ‘l'etranger (gigue existentielle)’에 대한 유튜브(Youtube) 페이지.</a></p>
 </div>`,
  "character_network":"<p>1942년 사르트르 Jean Paul Sartre가 이방인에 대한 해설 발표.</p><p>1954년 어떤 독일 친구가 알베르 카뮈에게 <이방인>을 연극으로 각색하여 상연하고자 한다는 계획을 제시.</p>",
  "social_network":`<div>
      <section>
        <h2>수상</h2>
        <ul>
          <li>노벨문학상, 1957년</li>
        </ul>
      </section>
      <section>
        <h2>비평</h2>
        <p>장 폴 사르트르 Jean Paul Charles Aymard Sartre, 1942년</p>
      </section>
      <section>
        <h2>논문</h2>
        <ul>
          <li>고선아, (2013), 알베르 카뮈의 이방인에 나타난 사물화 현상 연구 = Etude sur la reification dans l'Etranger d'Albert Camus, 한국교원대학교 대학원.</li>
          <li>김경희, (1990), A. Camus의 태양과 바다의 변신(Metamorphose)에 대한 이미지 : 이방인을 중심으로, 崇實大學校.</li>
          <li>김연수. (2003). [알베르 카뮈의 소설 〈이방인〉중, 뫼르소] 햇빛 눈부신 날에 울어본 적 있나요?. 월간 샘터, 34(02), 76-77.</li>
          <li>김용석, (2011), 알베르 카뮈의 <이방인 L'Etranger> 번역본 비교 연구, 韓國外國語大學校 大學院.</li>
          <li>김진하. (2014). 문체론적 접근을 통한 알베르 카뮈의 『이방인』 분석과 교육. 불어불문학연구, 99(0): 141-170</li>
          <li>김진하. (2015). 알베르 카뮈의 『이방인』 한국어 번역본들에 대한 문체론적 고찰. 불어불문학연구, 101(0): 57-102</li>
          <li>김호성. (2012). 카뮈의 『이방인』에 대한 불교적 이해. 동서비교문학저널,</li>
          <li>김혜동. (1983). 〈이방인〉을 통해 본 카뮈의 사회관. 불어불문학연구, 18(0): 133-147</li>
          <li>김혜동, (1985), 「이방인」 해석에 관한 비판적 고찰 : 1943-1977 = Etude critique sur les interpretations de L'Etranger de Camus, 梨花女子大學校 大學院.</li>
          <li>박홍규. (2011). 사이드와 카뮈. 인문연구, (61), 1-30.</li>
          <li>변광배. (2015). 『이방인』의 재판 장면 재탐사. 프랑스문화예술연구, 51, 205-234.</li>
          <li>유기환. (2010). 『이방인』의 살인사건과 리쾨르의 삼중의 미메시스. 프랑스어문교육, 35, 391-411.</li>
          <li>유기환. (2014). 왜 ‘이방인’인가 – 등장인물 뫼르소의 행동과 서술자 뫼르소의 언어 연구. 프랑스어문교육, 45, 207-230.</li>
          <li>이경철, (1987), <L'ETRANGER>에서의 빛의 Image 硏究, 아주대학교.</li>
          <li>이서규. (2014). 카뮈의 반항개념에 대한 고찰. 철학논총, 75, 211-242.</li>
          <li>최윤주. (2003). 불문학 : 알베르 카뮈의 『이방인 L'Etranger』연구 -"반대 오이디푸스"에서 "반(反) 오이디푸스"로-. 불어불문학연구, 56(1): 591-610</li>
          <li>Coppola. (2010). L’ADAPTATION DE L’ETRANGER DE CAMUS PAR VISCONTI. 프랑스문화예술연구, 34, 493-518.</li>
          <li>Lee Kie-Un. (2005). L'énigme de Meursault ou la dissimulation du langage. 프랑스어문교육, 19, 507-533.</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    - <a href='http://education.francetv.fr/litterature/premiere/video/albert-camus-commente-l-etranger' target='_blank'>카뮈가　이방인에　대해　말하다．（영상）</a><br/>
    - <a href='https://fr.wikipedia.org/wiki/L%27%C3%89tranger' target='_blank'>Wikipédia - L'Étranger</a><br/>
    - <a href='https://ko.wikipedia.org/wiki/%EC%95%8C%EB%B2%A0%EB%A5%B4_%EC%B9%B4%EB%AE%88' target='_blank'>Wikipedia – 알베르 카뮈</a><br/>
    -　Gallimard　출판사의　L'Étranger　표지（1942）<br/>
    <img src='https://commons.wikimedia.org/wiki/File:Camus23.jpg?uselang=ko' width='150'/><br/>
    －L'Etranger，　Mayo illustration  （1948）<br/>
    <img src='https://commons.wikimedia.org/wiki/File:Mayo_illustration_l_Etranger.jpg?uselang=ko' width='150'/>
 </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"중‧장편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"페스트 La Peste",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1947,
  "language":"프랑스어",
  "t_title":"페스트",
  "t_author":"김화영(2010), 최윤주(2014)",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["우정(amitié)", "병(maladie)", "페스트(peste)", "죽음(mort)", "전염병(épidémie)", "프랑스(france)"],
  "keywords2":["페스트(Peste)", "연대의식(sentiment de solidarité)", "실존주의(existentialisme)", "긍정(affirmation)", "저항(résistance)", "휴머니즘(humanisme)", "반항(révolte)", "부조리(absurde)", "철학(philosophie)", "종교(religion)", "부조리 상황(situation absurde)", "초월(transcendance)"],
  "story":`<div>
  <p>&lt;페스트&gt;는 총 5부로 구성되어 있다.</p>

  <h2>1부</h2>
  <p>오랑시는 알제리 해안에 면한 프랑스의 한 현청소재지로, 아주 평범한 도시이다. 194x년 4월 16일, 의사 리외가 사는 건물 층계에서 죽은 쥐가 발견되고, 리외는 이 사실을 건물 관리인에게 얘기하지만 그는 누군가의 장난이라고 단정짓는다. 알제리의 생활상을 취재하러 파리에서 온 기자 랑베르도 자신이 도착하자마자 일어난 쥐 사건에 대해 단순히 흥미로운 기삿거리로만 여긴다. 피곤해 보이는 수위를 부축하고 오던 파늘루 신부 역시 단순한 전염병일 거라며 웃어넘긴다. 얼마 안 가 곳곳에서 털이 젖어있는 채로 주둥이에서 피를 토하며 죽는 쥐들이 발견되자 리외는 시청의 쥐잡이 담당자에게 전화를 걸지만, 담당자는 별다른 반응을 보이지 않는다. 스페인 무용가 집에서 마주친 적 있는 타루 역시 쥐 사건은 수위의 몫이라며 대수롭지 않게 여긴다. 하지만 불과 며칠 만에 복도, 다락방, 공장, 학교 체육관, 카페 테라스, 간선도로, 그리고 산책로 등지에 이르기까지 도시 곳곳에서 수백 마리의 죽은 쥐들이 발견된다. 리외가 막연한 불길함을 느꼈던 쥐 사건에 대해서 시민들도 염려하기 시작한다. 시에서는 쥐 제거 작업에 들어가지만 상황은 점점 악화되어간다. 시를 혼란에 빠뜨렸던 죽은 쥐 소동이 점차 사그라들자 도시는 다시 평화를 되찾는 것처럼 보인다. 하지만 리외가 사는 건물의 수위가 쥐들과 똑같은 모습으로 죽는 일을 기점으로, 사람들이 한두 명씩 죽어나가기 시작한다. 푹푹 찌는 더위와 함께 더욱 많은 시민들이 알 수 없는 열병에 시달린다. 멍울과 반점, 그리고 고열에 시달려 헛소리를 하다 이틀 내에 죽음에 이르는 사람들을 보며 리외는 오랑에 퍼지는 유행병이 페스트라고 확신하게 된다. 리외는 페스트에 대항하는 자신의 직무를 떠올리며 매일 자기가 하는 일에 최선을 다해야 한다고 다짐한다. 시청 직원인 그랑은 리외를 도와 오랑의 사태에 대한 자료를 충실하게 제공한다. 페스트는 갈수록 거대한 위력을 나타내고, 이에 겁을 먹은 시 당국은 페스트 사태를 선포하고 도시의 문을 폐쇄하기에 이른다.</p>

  <h2>2부</h2>
  <p>당국이 페스트 발병을 공포하는 동시에 도시를 폐쇄하자 오랑 시민들은 두려움을 느낀다. 도시의 폐쇄로 유배당한 시민들에게는 오로지 단 몇 줄의 전보를 부치는 일만이 가능해졌다. 시민들은 세상의 수많은 망명자들에 대해 통감하며 쓸모없는 기억만을 붙잡고 살아가야 하는 입장에 놓인다. 하지만 다른 한 편으로 어떤 시민들은 불편하고 불안한 현실을 쾌락으로 보상받기 위해 영화관과 카페 등으로 몰려든다. 도시가 폐쇄되고 3주가 지나자 기자인 랑베르가 리외를 찾아와서 고향이 아닌 곳에 유배되었음을 한탄하며 도시 밖으로 나갈 수 있도록 건강 보증서를 작성해 달라고 부탁하지만 리외는 들어줄 수 없다며 거절한다. 리외는 매일같이 죽어 가는 환자들을 대하며 페스트에 맞서 싸워나간다. 자살을 시도한 적 있던 코타르는 모두가 불행해 보이는 현실에 만족감을 느끼는 듯 보인다. 파늘루 신부는 페스트가 신이 내린 형벌이라며 깊이 반성할 기회로 여기라며 설교한다. 여름이 시작되자 상황은 더욱 심각해진다. 어느 날, 타루는 리외를 찾아와 자원봉사대를 만들자고 제안하는데 리외는 흔쾌히 응하는 한편 타루의 목숨이 위험할지 모른다며 경고한다. 이 과정에서 타루와 리외 사이에 연대감이 싹트기 시작한다. 서술자는 작품에 개입하여 자신이 생각하는 영웅은 지극히 평범하게 자신의 맡은 바 소임을 충실히 다해내는 시청 직원 그랑이라고 말한다. 노老의사 카르텔은 혈청을 제조하기 위해 노력한다. 랑베르는 도시를 빠져나갈 방법이 없자 보건대 활동에 합류한다.</p>

  <h2>3부</h2>
  <p>8월이 되자 페스트의 위력은 거대해진다. 주로 변두리 지역에서만 사망자를 내던 페스트는 도시 중심부로까지 확산된다. 시민들은 페스트뿐만 아니라 방화, 약탈 그리고 폭동과도 같은 상황에도 놓인다. 사망자 수가 크게 늘자 별다른 장례식 없이 시신들은 구덩이에 파묻히게 된다. 그러나 땅이 부족해지자 화장터를 이용하게 되고 시민들은 자포자기 상태에 빠진다.</p>

  <h2>4부</h2>
  <p>페스트는 여전히 강력하고 보건대 대원들은 심한 피로에 빠져있다. 그러나 코타르만은 낙담하지 않은 채 과거에 자신이 겪었던 고통을 이제는 모두가 겪는다며 만족스러워한다. 타루는 코타르의 초청을 받아 시립 오페라 극장에서 공연을 관람하지만, 배우가 무대 위에서 페스트에 전염된 듯 갑자기 쓰러지자 서둘러 공연이 마무리 된다. 랑베르는 리외 곁에서 일한 뒤 검문소 근처로 거처를 옮겨 탈출의 기회를 엿보지만 결국 마지막 순간 탈출을 포기하고 리외와 타루의 곁에 남아 함께 싸우기로 결정한다. 오통 판사의 아들이 페스트에 감염되어 살아남을 가능성이 희박해지자 카스텔이 새로 만든 혈청의 시험 대상이 된다. 그러나 아이는 모두가 지켜보는 가운데 고통스러워하며 죽는다. 파늘루 신부는 신을 온전히 믿거나 그렇지 않을 거라면 완전히 부정하라는 설교를 하면서 리외의 우려를 사고, 얼마 안 있어 이름을 알 수 없는 병으로 고통스럽게 죽고야 만다. 페스트가 폐렴형으로 변하면서 사망자들은 기하급수적으로 늘어난다. 시민들 사이에서는 이기주의가 만성한다. 오통 판사는 당국의 실수로 수용소에 여전히 머물러 있음을 리외에게 알리며 도와줄 것을 요구하는데, 수용소에서 나오자마자 그는 자원봉사를 위해 다시 수용소로 떠나 모두를 놀라게 한다. 어느 날 갑자기 사라졌던 그랑이 페스트에 걸려 돌아오자 리외와 타루는 그를 보살핀다. 새로운 혈청으로 그랑은 병에서 벗어나고, 점차 살아있는 쥐들이 시민들의 눈에 띄기 시작한다.</p>

  <h2>5부</h2>
  <p>페스트가 후퇴하는 조짐을 보이고, 시민들은 기뻐하는 내색을 감추지만 마음 깊은 곳에서 희망을 품는다. 사망자 수치가 계속해서 낮아지자 도청은 도시 문을 재개할 때까지 2주의 여유를 두겠다고 발표한다. 그러나 코타르는 페스트 이전으로 돌아가 자신이 저질렀던 범죄들에 대해 수사를 받을 것을 두려워하며 망연자실한다. 도시 문이 열리기 며칠 전 타루는 페스트에 걸려 사경을 헤매다 결국 리외 곁에서 죽는다. 타루의 사망 이튿날 리외 또한 아내의 사망을 전해 듣는다. 마침내 도시의 문이 열리고 오랑 시민들은 감격스러워한다. 마지막 장에서 서술자는 자신이 의사 리외였음을 밝힌다. 그는 페스트균은 완전히 사라지지 않으며 언젠가 다시 돌아올 것이라는 사실을 알고 있기에 이 기쁨은 언제든 위협받을 수 있다고 전망하며 연대기를 마무리한다.</p>
  </div>`,
  "background":`<div>
    <h2>시간적 배경</h2>
    <ul>
      <li>1부 : 194x년 4월 ~ 5월</li>
      <li>2부 : 194x년 6월 ~ 7월</li>
      <li>3부 : 194x년 8월</li>
      <li>4부 : 194x년 9월 ~ 12월</li>
      <li>5부 : 이듬해 1월 ~ 2월</li>
    </ul>
    <h2>공간적 배경</h2>
    <ul>
      <li>알제리 오랑시</li>
    </ul>
  </div>`,
  "character_net":{
    "chart":"peste.png",
    "character":{
      "베르나르 리외":"작품의 서술자로, 페스트에 맞서 투쟁하는 의사이다. 그는 작품에서 인간성을 상징하는 인물이다.",
      "장 타루":"외지인 검사이다. 리외의 이웃이며 코타르와 가깝게 지낸다. 타루와 함께 자원봉사대를 구성하며 세속적인 성자가 되고자 노력한다. 그는 이야기의 끝부분에서 사망한다. 그는 작품에서 저항을 상징하는 인물이다.",
      "조제프 그랑":"시청 직원으로 자신의 본분에 충실한 인물이다. 로마네스크적 완벽에 이르기 위해 그는 첫 문장을 계속해서 썼다 지우며 책을 쓴다.",
      "코타르":"그랑의 자살을 신고했으며, 페스트로부터 이익을 얻은 유일한 인물이다. 그는 이야기의 끝에서 경찰에 의해 체포된다. 그는 작품에서 악을 상징하는 인물이다.",
      "파늘루 신부":"예수회의 성직자이자 학자이다. 이름을 알 수 없는 병으로 사망한다. 페스트를 하나님이 내린 재앙으로 해석한다.",
      "레이몽 랑베르":"오랑의 보건상태를 취재하기 위해 파리에서 온 기자이다. 그는 어떻게 해서든 유배된 도시로부터 탈출하고 싶어하지만, 결국 탈출을 포기하고 함께 페스트에 맞선다. 그는 뒤늦은 저항을 상징하는 인물로 그려진다.",
      "수위 미셸":"리외가 사는 건물의 수위로, 페스트의 첫 번째 희생자가 된다.",
      "카스텔":"리외의 동료로 새로운 혈청 백신을 개발하기 위해 노력한다.",
      "오통 판사":"그는 처음에 페스트에 대해 무관심하지만, 어린 아들이 페스트로 사망한 이후 그 전염병에 맞서 자원봉사를 시작한다. 그는 결국 사망하지만, 작품에서 사랑하는 사람의 죽음 이후에 저항하는 인물로 그려진다.",
      "리외의 아내":"그녀는 소설의 시작 부분에서 요양원으로 떠나지만, 5부에서는 죽음을 맞이하는 것으로 나온다."
    }
  },
  "style":"<p>서술자가 사건과 인물의 내면을 설명하며 극의 진행과정 중간에 서술자가 개입하는 양상을 보이기에 3인칭 전지적 작가 시점으로 보인다. 그러나 마지막 5장에서 서술자가 등장인물 리외라는 것이 밝혀진다.</p>",
  "network1":`<div>
    <p>&lt;이방인&gt; : 본 작품의 1부 후반에서 <b>‘최근 알제리를 온통 떠들썩하게 만들었던 체포 사건에 대해서 이야기 했는데, 그것은 어떤 상사의 젊은 직원이 해변에서 아랍인 한 명을 살해한 사건’</b>이라며 &lt;이방인&gt;에서 뫼르소가 살인을 저지른 상황에 대해 언급하고 있다.</p>
    <p>&lt;롤랑 바르트에게 보내는 편지&gt;에서 ‘알제리 해안에 위치했고 프랑스의 도청 소재지에 불과한’ 오랑시가 제2차 세계 대전 당시 나치의 탄압을 받던 프랑스를 상징하며, 등장인물 타루와 리외를 주축으로 활동하는 보건대는 레지스탕스 운동을 의미한다고 언급했다.</p>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":"<p><a href='https://fr.wikipedia.org/wiki/Roland_Barthes'>롤랑 바르트</a></p>",
  "social_network":`<div>
      <section>
        <h2>논문</h2>
        <ul>
          <li>윤석진, “Camus의 「La Peste」에 나타난 인간조건에 관한 연구”, 한국외국어대학교, 1989</li>
          <li>양은경, “Albert Camus의 La Peste, 그 구조와 주제의 비극성”, 건국대학교 대학원, 1995</li>
          <li>이랑아, “A. Camus의 인간적 반항과 사랑연구 : "La Peste"를 중심으로 = Une e'tude sur la revolte humaine et l'amour dans "La Peste" d'Albert Camus”, 숙명여자대학교, 1997</li>
          <li>이양재, “L'Etranger, La peste, La chute를 통해본 Albert Camus의 세계의 이중성과 그 변모 = (La) dualite du monde chez A. Camus et l'aspect de son evolution paru dans l'etranger, la peste et la chute”, 서울대학교 대학원, 1985</li>
          <li>박우경, “Camus의 「La Peste」에 나타난 연대의식에 관한 연구”, 아주대학교, 1986</li>
          <li>장영숙, “Temps du passe dans La Peste : analyse stylistique”, S.N., 1984</li>
          <li>강은경, “A. Camus의 <La Peste>에 나타난 침묵에 관한 연구”, 단국대학교, 1995</li>
          <li>이상화, “Camus의 La Peste와 기독교 사상”, 성심여자대학교, 1982</li>
          <li>문영택, “Albert Camus의 문학에 있어서의 반항과 구원의 관계 = Relation entre la revolte et la delivrance dane les oeuvres d'Albert Camus”,  충남대학교 교육대학원, 1979</li>
          <li>이숙희, “A. Camus의 종교관 연구”, 숙명여자대학교, 1984</li>
          <li>박흠련, “Camus에 있어서의 부조리와 반항에 관한 몇가지 고찰 = Quelques considerations sur l'absurdite et la revolte chez Camus, 이화여자대학교 대학원, 1971</li>
          <li>이은숙, “『페스트』에 나타난 공간의 의미연구”, 울산대학교 교육대학원, 2001</li>
          <li>Anderson, “Maternal presence and narrative voice in the novels of Albert Camus: "Une solitude a deux" (Algeria, France)”, University of California, 2001</li>
          <li>이양재, “L’Etranger와 La Peste의 관계 연구 = Etude sur relation entre l' Etranger et la Peste”, 불어불문학연구, Vol.22 No.1, 1987</li>
          <li>이신철, “Albert Camus의 &lt;&lt;La Peste&gt;&gt; 고찰 : Au point de vue de sa recreation des mythes 그 신화의 재창조성을 중심으로 = Une Etude sur &lt;&lt;La Peste&gt;&gt; d'Albert Camus”, 동아논총, Vol.19 No.1, 1982</li>
          <li>최성민, “La Peste를 통해서 본 Camus의 휴머니즘에 관한 하나의 고찰 = A Study on Camu's Humanism in &lt;La Peste&gt;”, 한국문화연구원 논총, Vol.57 No.-, 1990</li>
          <li>나애리, “작품 <페스트>의 객관적 서술양식  작품 &lt;페스트&gt;의 객관적 서술양식 : 자유 간접 화법을 중심으로”, 논문집, Vol.11 No.-, 1993</li>
          <li>이우동, “A. Camus의 La Peste 연구 = Etude sur Albert Camus (Ⅱ), 연구논문집, Vol.14 No.1, 1974</li>
          <li>정순철, “『페스트』의 공포와 등장 인물들의 공동체 의식 : Camus의 「La peste」를 中心으로 = Characters' Community Consciousness and their Fear in the Peste”, 논문집, Vol.15 No.1, 1994</li>
          <li>변광배, “알베르 카뮈의 『페스트』 : 나, 우리, 반항 = Albert Camus"s La Peste: I, We and Revolt”, 외국문학연구, Vol.53 No.-, 2014</li>
          <li>김점석, “쟝 타루의 죽음을 통해 본 『페스트』의 의미 = La mort de Jean Tarrou et le sens de La Peste”, 한국프랑스학논집, Vol.50 No.-, 2005</li>
          <li>이문호, “‘이방인’, ‘페스트’ 그리고 ‘전락’에 나타난 부조리”, 철학연구, Vol. 35, No.-, 1983</li>
          <li>김종우, “판늘루 신부를 통해 본 알베르 카뮈의 종교관 = La religion chez Albert Camus vu à travers l'abbé Paneloux”, 프랑스문화예술연구,  Vol. 46 No.-, 2013</li>
        </ul>
      </section>
      <section>
        <h2>공연</h2>
        <ul>
          <li><a href='http://www.playdb.co.kr/playdb/PlaydbDetail.asp?sReqPlayNo=72895' target='_blank'>연극 「2015 산울림 고전극장 - 페스트 (산울림 고전극장, 2015.02.04. ~ 2015.02.15.)</a></li>
          <li><a href='http://www.playdb.co.kr/playdb/PlaydbDetail.asp?sReqPlayNo=83344' target='_blank'>연극 「2015 앙코르 고전극장 – 페스트」 (산울림 고전극장, 2015.10.21. ~ 2015.10.31.)</a></li>
          <li><a href='http://www.playdb.co.kr/playdb/PlaydbDetail.asp?sReqPlayNo=86051' target='_blank'>연극 「페스트」 (공간 소극장, 2015.11.20. ~ 2015.11.21.)</a></li>
          <li><a href='http://www.playdb.co.kr/playdb/PlaydbDetail.asp?sReqPlayNo=92325' target='_blank'>연극 「기억을 기억하다 - 페스트」 (정의로운 천하극단 걸판, 2016.05.18. ~ 2016.05.19.)</a></li>
          <li><a href='http://peste.modoo.at/' target='_blank'>뮤지컬 「페스트」 (노우성 연출, 2016.07.22. ~ 2016.09.30.)</li>
          <li><a href='http://ticket.interpark.com/gate/TPGate.asp?Where=Naver&GPage=http://ticket.interpark.com/Ticket/Goods/GoodsInfo.asp?GoodsCode=16008416&NaPm=ct%3Dir2xv7uw%7Cci%3D7af4713e2583f00ffde754c8e4c7a2e388cf2ced%7Ctr%3Dslsl%7Csn%3D115%7Chk%3D3326d7daa534a8878d746590accb3f14708483f9' target='_blank'>연극 「페스트」 (극단 송곳, 2016.08.01. ~ 2016.08.07.)</a></li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li>초판본: <img src='http://81.169.222.198/still/kunst/pic570/397/411202500.jpg' width="150"/></li>
      <li>권기희 글, 이철희 그림, <a href='http://book.naver.com/bookdb/book_detail.nhn?bid=7271725' target='_blank'>"페스트 (문학고전의 감동을 만화로 만난다)", 채우리, 2013</a></li>
      <li><a href='https://fr.wikipedia.org/wiki/La_Peste' target='_blank'>위키피디아 프랑스어 “페스트”</a></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"중‧장편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"전락 La Chute",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1956,
  "language":"프랑스어",
  "t_title":"전락",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["부조리(absurde)", "반항(révolte)", "전락(chute)", "과오(faute)", "에고이즘(égoïsme)", "재판관 겸 참회자(juge-pénitent)", "죄의식(culpabilité)"],
  "keywords2":["전락(chute)", "죄의식(culpabilité)", "부조리(absurde)", "반항(révolte)"],
  "story":`<div>
    <h2>1부</h2>
    <p>운하와 회색빛의 도시 네덜란드의 수도 암스테르담의 한 술집 ‘멕시코시티’. 네덜란드어에 서툴러 진을 주문하지 못 하고 있는 한 40대 남자에게 파리의 전직 변호사였던 클라망스가 다가가 끝없이 이야기를 늘어놓는다. 남자가 돌아가야 할 때가 되자 클라망스는 진을 대접하고 다리목까지 그를 바래다준다.</p>

    <h2>2부</h2>
    <p>남자는 클라망스가 ‘재판관 겸 참회자(juge-pénitent)’라는 사실에 호기심을 갖는다. 클라망스의 고백에 따르면 그는 파리에서 명성을 날리던 변호사였다. 특히 그는 가난하고 힘없는 과부와 고아들을 위해 싸워 덕망이 높았다. 그는 레지옹 도뇌르 훈장을 탈 기회가 여러 번 있었음에도 점잖게 거절하고 가난한 자들에겐 한 푼의 사례비도 받지 않음으로써 스스로 진정한 보상을 받는다고 느껴왔다. 많은 사람들의 존경 속에서 항상 정상에 있다는 느낌을 지닌 채 다른 사람들 위에 군림하고 또 그들과의 관계에서 우월감을 느끼며 살아왔다. 그는 파리에서 변호사로서 ‘양심상의 평화’를 만끽하며 만족스러운 삶을 영위해왔던 것이다.​ 하지만 클라망스가 파리에서 누렸던 우월감을 바탕으로 한 이와 같은 만족스러웠던 삶과 ‘양심상의 평화’는 센 강의 퐁데자르를 건너던 중 듣게 된 정체 모를 웃음소리로 인해 흔들린다.</p>

    <h2>3부</h2>
    <p>이 웃음소리는 그가 파리에서 직접 겪었던 한 사건에 대한 기억과 깊은 관련이 있다. 클라망스는 자신이 체험했던 ‘떳떳하지 못한 일들’에 대해 이야기하며 문제의 그 ‘기억’을 끄집어낸다. 그는 문제의 웃음소리를 듣기 몇 년 전에 센 강의 퐁루아얄 다리의 난간에 기대어 강물을 굽어보고 있던 한 젊은 여자를 본 적이 있었다. 그 때 그는 이 젊은 여자를 외면하고 가던 길을 계속 갔지만 곧 그녀가 강으로 뛰어든 소리와 점차 잦아들어가는 여자의 비명을 듣게 된다. 그는 달려가서 그녀를 구하고 싶었지만 너무 늦고야 말았다. 구하기엔 너무 멀다고 판단하고 길을 계속 갔던 것이다.</p>

    <h2>4부</h2>
    <p>이 사건은 후일 변호사 클라망스의 명성을 더럽히는 얼룩이자 오점이 된다. 그는 이 사건으로 인해 다른 사람들로부터 죽어가는 사람을 구하지 않았다는 비난어린 심판을 받게 될까 봐 속으로 두려워하고 있었다. 클라망스는 더 이상 사람들이 자신을 존경하는 것을 원치 않게 된다.</p>

    <h2>5부</h2>
    <p>클라망스는 여자의 투신자살을 목도한 이후로 끊임없이 죄책감에 시달렸다. 그는 오히려 더 많은 크고 작은 과오들을 저지르기 시작했고 남자에게 그 일화들을 고백하는 동시에 자신의 행위들에 대해 변론한다.</p>

    <h2>6부</h2>
    <p>그는 파리의 변호사 사무실을 닫고 ‘멕시코시티’에서 될 수 있는 대로 공공연하게 스스로를 고발해 오고 있다. 그가 과거에 퐁데자르 위에서 들었던 정체 모를 웃음소리는 바로 사람들의 의구심과 자신의 죄책감에서 유래한 비난 어린 심판이었던 것이다. 그는 그렇게 정상에서 지옥으로 전락하는 것을 경험하게 된 것이다.</p>
  </div>`,
  "background":"<p>네덜란드의 수도 암스테르담의 술집 ‘멕시코시티’,</p><p>파리 센 강 퐁루아얄 다리 · 퐁데자르 다리,</p>",
  "character_net":{
    "chart":"la_chute.png",
    "character":{
      "장 밥티스트 클라망스":"가난하고 소외된 자들을 위해 싸워 명망이 높았던 변호사였다. 그러나 센 강 다리 위에서 젊은 여자가 투신하는 것을 목격하고도 구하지 않았던 일을 계기로 죄책감을 가지고 재판관 겸 참회자(juge-pénitent)가 된다.",
      "40대 남성":"'멕시코시티'에서 진을 주문하지 못하다가 클라망스의 고백을 듣게 된다."
    }
  },
  "style":"<p>1인칭 시점, 독백체.</p>",
  "network1":`<div>
    <p>『적지와 왕국』 에 수록될 예정이었으나 따로 발표됨.</p>
    <p>『반항하는 인간』 : 이 작품이 보여주는 지식인의 고뇌를 먼저 다루었음.</p>
  </div>`,
  "network2":"-",
  "network3":`<div>
    <p>1975년 니쿠 니타이(Nicou Nitai)가 &lt;전락&gt;을 원맨쇼로 각색하여 텔 아비브(Tel Aviv)의 심타 극장(Théâtre de la Simta)과 카로프 극장(Théâtre Karov)에서 3,000회 이상 공연함.</p>
  </div>`,
  "character_network":"",
  "social_network":`<div>
      <section>
        <h2>논문</h2>
        <ul>
          <li>하수현, “Albert Camus의 La Chute에 나타난 주인공의 전락의 과정에 관한 연구”, 연세대학교 대학원, 2003  </li>
          <li>이양재, “L'Etranger, La peste, La chute를 통해본 Albert Camus의 세계의 이중성과 그 변모”, 서울대학교 대학원, 1985</li>
          <li>고인숙, “『전락 La Chute』에 대한 소고”, 인문과학연구, Vol.1 No.-, 1982  </li>
          <li>최윤주, “‘죽음'의 테마를 통해서 본 Albert Camus의 'La Chute' : 정신분석적 해석의 한 시도, 성심여자대학 대학원, 1991</li>
          <li>권영옥, “「(La) Chute」에 나타난 주인공의 언어의 유희”, 서울대학교 대학원, 1977</li>
          <li>박옥희, “작품 L'Etranger와 La Chute에서의 주어인칭과 동사문제”, 성균관대학교, 1983  </li>
          <li>진경환, “Albert Camus의 소설 La chute에 나타난 물의 이미지 분석”, 한국외국어대학교, 1989</li>
          <li>권이오, “알베르 까뮈의 『전락 La Chute』에 나타나는 의사소통 기능에 관한 연구”, 프랑스어문교육, Vol.16 No.-, 2003</li>
          <li>권이오, “알베르 까뮈 텍스트 『전락』의 화법과 시점연구”, 프랑스어문교육, Vol.34 No.-, 2010</li>
          <li>권이오, “알베르 카뮈의 『전락』을 통해 본 열림의 미학”, 프랑스어문교육, Vol.31 No.-, 2009</li>
          <li>권이오, “까뮈의 작품 「전락」에 대한 연극적 고찰”, 프랑스어문교육, Vol.11 No.-, 2001</li>
          <li>권이오, “알베르 까뮈의 『전락』에 나타난 내레이션과 화자”, 프랑스어문교육, Vol.19 No.-, 2005  </li>
          <li>김언지, “La chute 의 주인공을 통해서 본 인간의 죄의식”, Foreign language teaching, Vol.- No.13, 1982</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='http://www.alalettre.com/camus-oeuvres-la-chute.php' target='_blank'>Résumé de la Chute</a></li>
      <li><a href='https://fr.wikipedia.org/wiki/La_Chute_(roman)' target='_blank'>위키백과 프랑스어판 “전락”</a></li>
      <li>초판 이미지: <img src='http://classiques.uqac.ca/classiques/camus_albert/chute/chute_L12.jpg' width="150"/></li>
    </ul>
  </div>`,
  "legends":"-"
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"중‧장편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"행복한 죽음 La mort heureuse",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1971,
  "language":"프랑스어",
  "t_title":"행복한 죽음",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["죽음(mort)", "행복(bonheur)", "바다(mer)", "시간(heure)", "돈(argent)", "돌(pierre)"],
  "keywords2":["욕망(désir)", "부조리(l'absurdit)", "이원론(dualisme)"],
  "story":`<div>
    <h2>제１부　자연적인　죽음</h2>
    <p>제１장：화사하고 싸늘한 ４월 아침 10시 파트리스 메르소(Patrice Mersault)는 자그뢰스(Zagreus)의 별장으로 간다. 그리고　안락의자에 앉아 있던 자그뢰스를 그의 총으로 살해한 후 그가 보여 주었던 유언장 형식의 편지를 꺼내 테이블 위에 올려 놓는다. 자신의 지문이 묻어 있는 총을 닦아 죽은 자그뢰스의 손에 쥐어준 뒤 가져간 가방에 돈 다발을 쑤셔 넣는다. 그　뒤，　누구의 눈에도 띄지 않게 그 집을 나와 저녁이 되도록 깊은 잠을 잔다.</p>
    <p>제２장：(과거로　거슬러　올라감) 항구에 면해 있는 사무실에 근무하고 있는 메르소는　동료　에마뉘엘과　함께　셀레스트가　운영하는　단골식당에서　이야기를　나누며　식사를　한다． 집으로　돌아온　메르소는　어머니의 방에 누워 어머니와 석유램프 밑에서 먹던 저녁을 생각한다. 그는　２시　５분에　사무실로　들어서　６시까지　일을　하고　집으로　돌아와　저녁을　먹고　담배를　피우며　지나가는　행인들을　구경한다．</p>
    <p>제３장：　애인인 마르트(Marthe)와　함께　영화관으로　간　메르소는　낯선　남자가　마르트를　바라보는　시선에　분노를　느끼고　다음날　그녀　애인들의　이름을　말해달라고　한다．그리고　그들　중 그녀의 첫　애인이었던 자그뢰스를 소개받게　된다. 　처음에　메르스는　자그뢰스가　언짢았지만　점차　그에게　관심과　흥미를　느끼게　되며 일종의 우정을 느낀다.</p>
    <p>제４장：　일요일 오후 자그뢰스와 메르소 사이에 처음으로 의사 소통이 이루어진다. 오랫동안 품어왔던 자신의 희망이 이제 실현의 단계에 와 있음을 느낀 자그뢰스는 메르소에게 금고 안의 권총과　편지봉투　한　통을 보여주며 생명의　부정과　돈，　행복에　관해　이야기한다．</p>
    <p>제５장：　일요일 저녁 메르소는 세를 준 옆방 통장이，　카르도나（Cardona）와 말을 나눈다. 생애 최대의 곤경에 처해 있는 통장이가 ‘나는 어머니를 사랑한다’라고 말하면 메르소는 전혀 다른 뜻으로 그 말을 해석한다. 매일매일 반복되고 있는 거대한 무의미 앞에 선 메르소는 불현듯 ‘아니다’라고 말할 수 있는 반항만이 자신 속에 있는 참된 무엇이라는 생각을 한다. 짐을 꾸려 메르소는 마르세이유로 떠나는 배에 오른다.</p>
    <h2>제２부　의식적인　죽음</h2>
    <p>제１장：　프라하의 어느 골목에 위치한 호텔에 투숙한 메르소는 저녁의 거리로 나온다. 행상인 노파가 절인 오이에서 세계가 자신에게 주고 있는 고독을 느낀 메르소는 근처 식당으로 들어간다. 그곳에서 메르소는 뚫어지게 자신만을 바라보고 있는 아코디언 연주자를 본다. 며칠 뒤 그가 장님이라는 것을 안 메르소는 태양과 태양이 드러내고 있는 초록빛과 그 안에 살고 있을 여자들에 대한 동경으로 운다. </p>
    <p>제２장：　유럽의 절반을 횡단하는 기차 안에서 메르소는 스스로에게 다시 한번 다가오는 운명을 맞아들여 보자는 생각을 한다. 생존하기 위해서는 누구에게나 시간이 필요하다. 비인에 도착한 메르소는 편지로만 왕래하던 알제리에 사는 세 명의 여대생들에게 편지를 쓴다. 새삼 자신이 행복을 위해 태어났다는 것을 깨닫고는 바다로 향한다.</p>
    <p>제３장：　일명 ‘세계를　앞에　둔 집’에서 메르소는 방학을 맞은 세 명의 여대생들과 생활한다. 태양아래 맨　살갗을 드러내 놓고 있는 여대생들의 육체에서 자연의 힘을 본 메르소는 다문 입 속의 침묵과 같이 목석처럼 잠들어 있는 이 세계의 의미를 파악하고 싶은 억제할 수 없는 충동을 느낀다.</p>
    <p>제４장：바다가 품고 있는 새로운 세계를 탐욕스럽게 바라보고 있던 메르소는 작열하는 태양과 바다가 있는 티파사에 집을 구한다. 심장의 고동을 오후 두시의 태양에 맞춰놓고 누워 시간과 시간 사이에 어른거리는 영원한 무엇을 바라본다. 메르소는 비로소 자신의 내부에 숨어 있던 죽음의 공포에서 벗어난다.</p>
    <p>제５장：한 달여를 늑막염으로 누워 있던 메르소는 한밤중의 바다로 들어간다. 밤의 적막과 고요가 주는 흥분 속에서 그는 문득 헤엄쳐 가고 있는 물 밑의 세계가 자신을 끌어당기고 있음을 느낀다. 돌아와 기름이 떠 있는 주전자의 물로 차를 꿇여 마신 메르소는 자신의 앞에 와 있는 다른 세계의 무엇을 본다. 혈기 넘치는 건강한 생명으로 그것을 맞고 싶다고 생각한 메르소는 의사의 치료를 거부한다. 거듭되는 고열 속에서 살해 후 처음으로 찾아온 자그뢰스와 대면한다. 자그뢰스와 한 방향을 향한 메르소는 자신의 배에서 목으로 올라오고 있는 조약돌을 느낀다. 마지막으로 대지가 주는 과즙을 제물로 받아들인 그는 조약돌이 심장 위에 멈춰 있는 육체를 돌려 바다―움직이지 않는 세계의 진실―를 향하여 선다.</p>
  </div>`,
  "background":`<div>
    <h2>시간적　배경</h2>
    <ul>
      <li>1936년</li>
    </ul>
    <h2>공간적 배경</h2>
    <ul>
      <li>제１부：북아프리카 알제.　어머니가　머물렀던　방，　이웃，　카르도나의　방，　자그뢰스의　방　－　닫힌　공간，　부동성，　정적인　공간</li>
      <li>제２부：프라하（호텔，　교회，　식당），기차，　드레스덴,　바우젠,　괴를리츠,　리그니츠,　슐레지엔의 평원， 브로추아프，　비엔나，　알제，　‘세계를　앞에　둔 집’，　바다　－　열린　공간．<br/>북쪽　－　어두움,　물,　하강의　이미지<br/>남쪽　－　태양,　빛,　상승의 이미지<br/>슈누아　언덕　위의　집</li>
    </ul>
  </div>`,
  "character_net":{
    "chart":"la_mort_heureuse.jpg",
    "character":{
      "파트리스 메르소":"주인공. 인간의 가장 중요한 부분인 누군가를 또는 무엇인가를 사랑하고 있다라는 감정마저 의식의 ‘잠’에 빼앗겨 버린 채 살아간다．어느　날 불구자인 자그뢰스가 나타난다. 이 남자의 암시로 인한 살인을 저지르고 나서야 잠자던 메르소의 의식은 깨어나, 삶이 다른 그 무엇도 아닌, 자기 자신에 대해 생각할 것을 요구하고 있음을 알아차린다. 이때부터 메르소의 내부에 있던, 행복을 향한 맹렬한 의지 또한 일어나기 시작한다. 살인에 대한 보상으로 얻어진 자유로 메르소는 자신의 삶과 맞대면을 향한 여행을 떠난다.",
      "마르트":"메르소와　연인관계였던　여자．　메르소와　자그뢰스　로부터　‘겉모습’이라고　종종　불리운다．",
      "자그뢰스":"젊음 위에 부까지 움켜쥐어 그 자신 행복의 절정에 이르렀다고 믿는 순간 두 다리를 잃는 사고를 당한　그는　다른 세계의 꿈을 꾸기 시작하고　뫼르소에게　자신을　죽이라는　암시를　던진다．",
      "카르도나":"메르소의　이웃으로　자신의　엄마가　죽고　난　뒤　개와　단　둘이　지내고　있다．",
      "카트린":"‘세계를　앞에　둔 집’에서　메르소와　함께　생활한다．",
      "클레르":"‘세계를　앞에　둔 집’에서　메르소와　함께　생활한다．",
      "로즈":"‘세계를　앞에　둔 집’에서　메르소와　함께　생활한다．",
      "뤼시엔":"메르소와　결혼，　그　후　메르소는　슈누아라는　지역에서　고독을　맛보며　자신의　행복을　만끽하게　된다．"
    }
  },
  "style":"<p>- 3인칭 관찰자　시점</p><p>- 문체적 특징 : 간결하며 주변　묘사가 많음, 사건 서술 중심．</p>",
  "network1":`<div>
    <h2>문학</h2>
    <ul>
      <li>결혼　Noces，알베르　카뮈　Albert Camus，　1938.</li>
      <li>시지프의　신화 Le mythe de Sisyphe，알베르　카뮈　Albert Camus，　1942.</li>
      <li>이방인 L'Étranger，알베르　카뮈　Albert Camus，　1942.</li>
      <li>작가수첩Ⅰ Carnets I，알베르　카뮈　Albert Camus， 1962.</li>
      <li>작가수첩Ⅱ　Carnets II，알베르　카뮈　Albert Camus，　1964．</li>
      <li><a href='https://fr.wikipedia.org/wiki/La_Mer_et_les_Prisons' target='_blank'>바다와　감옥　La Mer et les Prisons，　로제　키요　Roger Quilliot，1956．</a></li>
      <li><a href='https://fr.wikipedia.org/wiki/Correspondance_Albert_Camus,_Ren%C3%A9_Char' target='_blank'>카뮈를　추억하며，Correspondance Albert Camus, Jean Grenier 장　그르니에，1981．</a></li>
      <li><a href='https://fr.wikipedia.org/wiki/Camus_(Sarocchi)'>Camus, Jean Sarocchi, 1968.</a></li>
    </ul>

    <h2>논평</h2>
    <p>Reviewed Work: <a href='http://www.jstor.org/stable/40126104?seq=1#page_scan_tab_contents' target='_blank'>Cahiers Albert Camus. I: La mort heureuse by Albert Camus, Renée B. Lang, Books Abroad, Vol. 46, No. 2 (Spring, 1972), p. 260</a></o>

    <h2>단행본</h2>
    <ul>
      <li>『알베르　카뮈의　미학』，　김세리，　한국학술정보, 2008. 4. 15.</li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":"<p>«알베르　카뮈　노트»　편집　책임<br/>장－클로드　브리스빌，　로제　그르니에，　로제　키요，　폴　비알라네</p>",
  "social_network":`<div>
      <section>
        <h2>논문</h2>
        <ul>
          <li>김혜동,（1991）， "「행복한 죽음」에서 「이방인」으로 ", 論文集/20(-), 27-39, 慶熙大學校</li>
          <li>박미리,　（1982）， "Camus 의 「La Mort heureuse」에 나타난 돌의 이미지 연구", 梨花女子大學校 大學院</li>
          <li>이기언, (1986), Camus의 인물들에 있어서 Dualisme : L'Envers et l'Endroit, noces 그리고 La Mort heureuse를 중심으로, 연세대학교 대학원</li>
          <li>이미경,(2006)， "카뮈(Albert Camus)의 『La mort heureuse』를 중심으로 본 공간과 인물에 관한 연구", 숙명여자대학교 대학원</li>
          <li>Abdel-Kader, C. (1973). Personnages dans La mort heureuse et L'etranger.</li>
          <li>Anderson, K. H. (1984, July). Justification and Happiness in Camus's La mort heureuse. In Forum for Modern Language Studies (Vol. 20, No. 3, pp. 228-246). Oxford University Press.</li>
          <li>ANDRIANNE, R. (2012). Eros et cosmos dans La Mort heureuse de Camus. Revue Romane, 2.</li>
          <li>De Col, F. (1988). Les thématiques du soleil et de la mort dans L'étranger, La mort heureuse et La peste d'Albert Camus.</li>
          <li>De Paola, D. (1972). La mort heureuse by Albert Camus: the Dionysian motif.</li>
          <li>Dunwoodie, P. (1972). " La Mort heureuse" et" Crime et Châtiment"; une étude comparée. Revue de littérature comparée, 46(4), 494.</li>
          <li>Etienney, F. (2004). Le bonheur dans trois oeuvres d'Albert Camus:" La Mort heureuse"," L'Envers et l'endroit"," Noces (Doctoral dissertation).</li>
          <li>Gaëlle About. (1981). Le bonheur dyonisiaque dans les premiers textes de Camus: L'Envers et l'endroit, Noces, La mort heureuse, L'Eté (Doctoral dissertation).</li>
          <li>George Strauss, (1975), A reading of Albert Camus'La Mort heureuse, Neophilologus Vol. 59, No. 2. 199-212 0028-2677 AHCI.</li>
          <li>Henry, M. N. (1981). From Albert Camus' La mort heureuse to L'etranger. University Microfilms International.</li>
          <li>John K. West,(2000), Political or Personal: The Role of the Chenoua Landscape in Camus's La Mort heureuse, The French Review, Vol. 73, No. 5 (Apr., 2000), pp. 834-844.</li>
          <li>KIRSTEEN H. R. ANDERSON, (1984), JUSTIFICATION AND HAPPINESS IN CAMUS'S LA MORT HEUREUSE, Forum for Modern Language Studies Vol. 20, No. 3. pp. 228-246 0015-8518 SCOPUS</li>
          <li>Lea, B. (2013). Le bonheur dans La mort heureuse, Camus (1936). Publications Docs-en-stock. com.</li>
          <li>Lee, H. J. (1990). La sensibilité de Camus dans" Noces" et" La Mort heureuse" (Doctoral dissertation).</li>
          <li>Margaret M. Willen, (1996), The Morality of Revolt in Gide's L'immoraliste and Camus' La mort heureuse, Dalhousie French Studies Vol. 34 (Spring 1996), pp. 77-90</li>
          <li>Petropoulou, Z. (1992). L'e" criture figur6e du language sensoriel dansL'Etranger, La Peste, La Mort Heureuse et Noces, d'Albert Camus. Strategies of Rhetoric, 19, 94.</li>
          <li>Povéda, M. (2008). Albert Camus et la sensualité dans Noces, La Mort heureuse et Le Premier homme (Doctoral dissertation).</li>
          <li>Sehn, J. P. (1979). A contrastive analysis of Camus's La Mort heureuse and L'Etranger.</li>
          <li>SHIMADA, K. (1981). Caligula entre La Mort heureuse et L'Etranger. Etudes de Langue et Littérature Françaises Tokyo, (38), 122-139.</li>
          <li>Strauss, G. (1975). A Reading of Albert Camus' La mort heureuse. Neophilologus, 59(2), 199-212.</li>
          <li>Viallaneix, P. (1973). Le premier Camus: suivi de Écrits de jeunesse d'Albert Camus (Vol. 2). Gallimard.</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://fr.wikipedia.org/wiki/La_Mort_heureuse' target='_blank'>Wikipédia -　La Mort heureuse</a></li>
      <li>표지　（이미지）:<img src='http://a.decitre.di-static.com/img/fullsize/albert-camus-la-mort-heureuse/9782070277896FS.gif' width='150'></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"중‧장편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"최초의 인간 Le premier homme",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1994,
  "language":"프랑스어",
  "t_title":"최초의 인간",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":1995,
  "keywords1":["이야기(récits)", "유년기의 기억(souvenirs d'enfance)", "가난(pauvreté)", "알제(alger)", "가족(famille)", "프랑스인(français)"],
  "keywords2":["알제리 전쟁(La guerre d‘Algerie)", "식민주의(colonialisme)", "가난(pauvreté)", "가족(famille)", "자전적 소설(roman autobiographique)", "유작(oeuvre posthume)"],
  "story":`<div>
    <h2>제1부 아버지를 찾아서</h2>
    <p>앙리 코르므리는 임신한 아내 뤼시와 함께 알제리에 도착한다. 마을에 도착하자마자 아이를 낳은 부부는 아들의 이름을 자크로 짓는다.</p>
    <p>40년 후, 한 남자(자크)가 생브리외의 '프랑스 추모 묘역'에 도착한다. 그 곳에는 전사한 아버지 앙리가 묻혀있다. 앙리는 자크가 생후 한 살 때 전사했기 때문에 자크는 아버지에 관해서 그다지 많은 생각을 하지 않으며 살았다. 어머니의 간곡한 부탁으로 처음으로 아버지의 묘지에 방문한 자크는 묘비의 연대를 읽으며 자신의 현재 나이보다 죽은 아버지의 나이가 더 어리다는 사실을 상기하곤 커다란 연민의 감정을 경험한다. 자크는 아버지에 대해 자신이 아는 것이 거의 없음을 깨닫고 아버지와 관련된 정보를 찾고 싶다는 생각을 갖게 된다.</p>
    <p>알제로 향하는 배에 탄 자크는 승객들이 낮잠을 청하는 것을 보며 낮잠을 좋아하지 않았던 유년시절의 자신을 떠올린다. 꿈속에서 자크는 장, 피에르, 막스, 조제프와 같은 동네 친구들과 자신을 매섭게 혼내곤 하던 할머니를 만난다. 알제에 도착한 자크는 어머니 뤼시의 집으로 향한다. 자크는 뤼시에게 아버지에 관해 묻는다. 자크는 아버지 앙리가 알제 주아브 연대에 징집되었으며 뤼시에게 부상당했다는 편지를 보낸 후 전사했다는 것을 알게 된다.</p>
    <p>자크는 가난했던 자신의 어린 시절을 떠올린다. (여기에서부터 어머니의 이름 뤼시가 카트린으로 바뀐다) 그는 어머니와 할머니에 관한 일화들을 회고하는데 특히 가장으로서의 책임감으로 자크를 무섭게 혼내며 키웠던 할머니에 대해 주로 생각한다. 할머니와 어머니 모두 글을 읽지 못한다. 심지어 그의 어머니는 어릴 적 앓은 병으로 귀까지 잘 들리지 않는다. 자크는 삼촌 에르네스트(에티엔)과 조제팽에 관한 기억들도 떠올린다.</p>
    <p>초등학교 마지막 학년의 담임선생님이었던 베르나르는 자크와 피에르, 그리고 프뢰리의 총명함을 알아보고 이들을 중고등학교에 진학시키려고 한다. 자크 역시 중고등학교에 진학하고 싶어하지만 할머니는 자크도 하루빨리 일을 해야 한다며 반대한다. 베르나르는 자크의 집에 찾아가 할머니를 설득시키고 한 달 동안 매일같이 수업이 끝난 후 아이들을 모아놓고 공부를 시킨다. 선생님의 노력으로 자크는 중고등학교 장학생 시험에 합격한다.</p>

    <h2>제2부 아들 혹은 최초의 인간</h2>
    <p>자크가 중학교에 진학한 이후로 가족들 사이에는 침묵이 커져만 간다. 학교의 일에 관해서는 가족들이 아는 바가 없었기 때문이다. 학교에서도 자크는 수줍은 성격 탓에 쉽게 적응하지 못한다. 부모의 직업을 적어내던 중 자크는 커다란 수치심을 느낀다. 자크는 자신이 아무것도 부러워하지 않으면서 왜 때때로 수치스러워 하는 지에 대해 골몰한다.</p>
    <p>자크는 성이나 죽음에 대해서도 생각하지만 여전히 그의 생각을 지배하는 것은 가난이다. 자크가 3학년이 되자 할머니는 그에게 돈을 벌어야 한다고 말한다. 자크는 방학 동안만이라도 일을 하기 위해서 점주에게 오래 일할 수 있다고 거짓말을 해야 하는 것과 그토록 좋아하는 여름의 하늘과 바다를 마음껏 즐길 수 없다는 사실에 고통스러워하지만, 월급을 할머니에게 줄 때는 자신이 집안의 가난을 조금 덜었다는 생각에 긍지를 느끼기도 한다. 고등학교 2학년에 진학한 자크는 늘 자신을 때리던 할머니에게 대들기도 하고 축구부의 정식 골키퍼로 활동하기도 한다. 자크는 실존에 대한 생각을 시작한다.</p>
  </div>`,
  "background":"<p>자크의 어린 시절, 알제리.</p>",
  "character_net":{
    "chart":"le_premier_homme.jpg",
    "character":{
      "자크 코르므리":"주인공. 카뮈의 어린 시절을 대변하는 인물.",
      "앙리 코르므리":"자크의 아버지로, 그가 자신의 유년기를 떠올리게 된 계기가 된 인물.",
      "카트린 코르므리":"자크의 어머니. 유약한 성격이며, 어릴 때 병을 앓아 귀가 안 들리는 장애가 있고 글을 읽지 못 함.",
      "할머니":"집안의 가장으로 단 한 번도 일을 쉬어본 적이 없을 정도로 생활력이 강함.",
      "에티엔":"앙리의 외삼촌. 유년 시절의 앙리와 각별하게 지냈음.",
      "조제팽":"앙리의 외삼촌. 동생인 에티엔과 다르게 날카로운 성격을 지님.",
      "베르나르":"앙리의 초등학교 시절 담임선생님으로, 총명한 앙리가 중고등학교에 진학할 수 있게 해 주었음.",
      "피에르":"앙리의 절친한 친구로 중고등학교 생활을 함께 함."
    }
  },
  "style":"<p>3인칭 시점. 다소 문장의 길이가 길며 회고적인 문체를 사용함.</p>",
  "network1":"-",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>뤼시앵 카뮈(Lucien Camus) : 카뮈의 아버지. 자크의 아버지 앙리처럼 알제리 원주민 보병으로 징집당해 마른 전투에서 부상을 입고 생브리외 군인 병원에서 사망하였음.</p><p>카트린 카뮈(Catherine Camus) : 카뮈의 어머니. 자크의 어머니 카트린의 실제 인물. 남편이 전사한 후 친정어머니와 같은 집에서 동생 에티엔, 조제프와 함께</p><p>루이 제르맹(Louis Germain) : 카뮈의 스승으로 베르나르의 실제 인물.</p>",
  "social_network":`<div>
      <section>
        <h2>논문</h2>
        <ul>
          <li>박규현, “카뮈의 첫 작품 『안과 겉』과 마지막 작품 『최초의 인간』 비교연구”, 인문과학연구, 제48집, 2005.</li>
          <li>오은하, “『최초의 인간』 : 카뮈의 알제리 찾기 (Le Premier homme : la recherche de l'Algerie d'Albert Camus)”, 불어불문학연구, 제100집, 2014.</li>
          <li>Just, Daniel. "LITERATURE AND ETHICS: HISTORY, MEMORY, AND CULTURAL IDENTITY IN ALBERT CAMUS'S LE PREMIER HOMME." The Modern Language Review 105.1 (2010): 69-86. Web.</li>
          <li>Hughes, Edward J. "Building the Colonial Archive: The Case of Camus's "Le Premier Homme"" Research in African Literatures 30.3 (1999): 176-93. Web.</li>
          <li>Vistica, Rita R. "A Woman Called "Jesus": Reflections on Catherine Cormery in Camus's "Le Premier Homme" (1994)." Pacific Coast Philology 31.1 (1996): 120-33. Web.</li>
          <li>Dubois, Lionel. "Le Premier Homme, Le Roman Inachevé D'Albert Camus." The French Review 69.4 (1996): 556-65. Web.</li>
          <li>Guérin, Jeanyves. "Des Chroniques Algériennes Au Premier Homme: Pour Une Lecture Politique Du Dernier Roman De Camus." Esprit (1940-) 211 (5) (1995): 5-16. Web.</li>
        </ul>
      </section>
      <section>
        <h2>영화</h2>
        <p>포스터 이미지: <img src='https://arunwithaview.files.wordpress.com/2013/05/le-premier-homme.jpg' width='150'></p>
        <p>Gianni Amelio, "Le Premier Homme"<br/><a href='https://www.youtube.com/watch?v=LL7aHHIonfA' target='_blank'>https://www.youtube.com/watch?v=LL7aHHIonfA (Bande annonce)</a></p>
        <p><a href='https://fr.wikipedia.org/wiki/Le_Premier_Homme_(film)' target='_blank'>https://fr.wikipedia.org/wiki/Le_Premier_Homme_(film) (프랑스 위키페디아)</a></p>
      </section>
    </div>`,
  "digital_works":`<div>
    <p>유튜브 영상 - <a href='https://www.youtube.com/watch?v=usphG1sZY4E' target='_blank'>Albert Camus et «LE PREMIER HOMME»</a> (Les conférences de l'Université de Nantes)</p>
    <p><a href='https://fr.wikipedia.org/wiki/Le_Premier_Homme'>프랑스어 위키페디아 ‘최초의 인간 Le Premier Homme’ 페이지</a></p>
    <p>초판본 이미지 <img src='http://www.devoir-de-philosophie.com/images_dissertations/30234.jpg' width='150'></p>
  </div>`,
  "legends":"<p>미완성본. 카뮈는 이 원고를 끝마치지 못하고 교통사고로 사망했음.</p>"
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"단편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"간부(『적지와 왕국』 첫 단편)　La Femme adultère",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1957,
  "language":"프랑스어",
  "t_title":"간부",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["사막(désert)", "바다(mer)", "욕망(désir)"],
  "keywords2":["적지(exill)","왕국(royaume)","고독(solitaire)","연대(solidaire)","대조(contraste)","해방(libération)","부조리(absurdité)","실존주의(existentialisme)"],
  "story":"자닌은 남편이 벌어오는 돈의 혜택을 충분히 받으며 살고 있으나 언젠가부터 삶의 권태 속으로 빠져든다. '덧문을 반쯤 닫아놓은 어슴푸레한 집안에서 세월을 흘려보낸' 그녀에게 '여름, 해변가, 소풍, 심지어 하늘까지도 머나먼 것'이었다. 포목상인 남편 마르셀의 강요로 마지못해 알제리의 사막지대로 남편의 포목 장사에 따라 나선 자닌은 생각과는 달리 모래가 아닌 돌 천지이고 추운 사막에 염증을 느끼게 된다. 호텔에서 사랑이 식어버린 남편과 함께 자다가 답답해진 자닌은 잠자리를 뛰쳐나와 사막의 밤이라는 자연과 간음을 한다．",
  "background":"<p>－시간적　배경：　12월　경　겨울</p>－공간적　배경： 알제리　남부 사막지방</p>",
  "character_net":{
    "chart":"La_Femme_adultère.jpg",
    "character":{
      "자닌 Janine":"소설에서의 역할: 주인공<br/>포목상인 남편 마르셀의 강요로 마지못해 알제리의 사막지대로 남편의 포목 장사에 따라 나서게　된다. 남편을　사랑해서가 아니라 타성과 필요에 의해서　함께 살아가고　있다. 호텔에서 사랑이 식어버린 남편과 함께 자다가 답답해진 자닌은 잠자리를 뛰쳐나와 사막의 밤이라는 자연과 간음을 한다.",
      "마르셀 Marcel":"자닌의　남편, 전쟁　후　사업이　어려워지자　중개상을　거치지　않고　직접　아랍상인들에게　옷감을　팔기　위하여　자닌과　함께　고원지대와　남쪽의　마을을　찾아다니기로　한다."
    }
  },
  "style":"<p>전지적 작가 시점. 주변 묘사가 많다. 사건 서술 중심. </p>",
  "network1":`<div>
    <ul>
      <li>안과 겉 L'envers et l'endroit, 1937. 알제에서 보낸 작가의 가난한 어린 시절과 외부 세계의 발견이라는 모티프의 공통점으로 이 작품과 연결됨.</li>
      <li><a href='#시지프의 신화'>시지프의　신화 Le mythe de Sisyphe，1942.</a></li>
      <li><a href='#이방인'>이방인 L'Étranger，1942.</a></li>
      <li><a href='#전락'>전락　La chute, 1956.</a></li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"－　피에르외젠　클레렝　Pierre-Eugène Clairin의　12점의　석판화와　함께　1954년 11월에　롬발디(Rombaldi) 출판사에서 &lt;간부&gt;가　출간됨.",
  "character_network":`<div>
    <p>－　피에르외젠　클레렝　Pierre-Eugène Clairin의　12점의　석판화와　함께　1954년 11월에　롬발디(Rombaldi) 출판사에서 &lt;간부&gt;가 출간됨.<br/>
    <a href='http://www.saintloupdenaud.com/pierre-eugene-clairin.html' target='_blank'>피에르외젠 클레렝 소개 페이지(불어)</a></p>
  </div>`,
  "social_network":`<div>
      <section>
        <h2>해설</h2>
        <ul>
          <li>Owen J. Miller, &lt;L'exil et le royaume: Cohérence du recueil&gt;, 《Albert Camus 6 Camus nouvelliste:L'exil et le royaume》, La Revue des Lettres Modernes(Minard, 1973)</li>
          <li>Roger Grenier, 《Albert Camus Soleil et Ombre》(Gallimard, 1987)</li>
          <li>Roger Quilliot, &lt;Prèsentation de L'exil et le royaume&gt;, Albert Camus, 《Théâtre, Récits, Nouvelles, Textes》 réunis et annotés par Roger Quillot, Bibliothèque de la pléiade, (Gallimard, 1962)</li>
        </ul>
      </section>
      <section>
        <h2>논문</h2>
        <ul>
          <li>김진식, (1994)，알베르　카뮈의　『適地와　王國』　연구　，　한국불어불문학회,</li>
          <li>박홍규，(2001)，　사이드와　카뮈，　영남대학교　인문과학연구소　인문연구　제61호 1-30(총 30페이지)</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://fr.wikipedia.org/wiki/L%27Exil_et_le_Royaume' target='_blank'>Wikipedia －L'Exil et le Royaume</a></li>
      <li>적지와　왕국　(이미지)<br/>
        <img src='http://libresavoir.org/ressources/img/l-Exil-et-le-royaume_Albert-Camus.jpg' width='150'/>
        <img src='http://www.ameliesourget.com/wp-content/uploads/2014/11/Camus.jpg' width='150'/></li>
      <li>간부　(이미지)<br/>
        <img src='http://catalogue.drouot.com/images/perso/phare/LOT/104/12121/227.jpg'/></li>
      <li><a href='http://www.edition-originale.com/en/literature/signed-books-and-manuscripts/camus-la-femme-adultere-1954-32641' target='_blank'>피에르외젠　클레렝　Pierre-Eugène Clairin의　석판화</a></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"단편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"배교자 혹은 혼미해진 정신 (『적지와 왕국』 둘째 단편) Le Renégat ou un esprit confus",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1957,
  "language":"프랑스어",
  "t_title":"배교자",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["사막(désert)","우상(fétiche)","노예(esclave)"],
  "keywords2":["적지(exill)","고독(solitaire)","연대(solidaire)","대조(contraste)","부조리(absurdité)","실존주의(existentialisme)"],
  "story":"<p>프랑스의 개신교 고장 출신의 한　젊은이가 카톨릭으로 개종하여 선교사가 되어 위험한 지역이라고 사람들이 만류하는 알제리에 있는 미개인 부락으로 들어간다. 하지만　결국 혀를 잘리고 감금되어 일상적으로 폭행을 당하며 지내게 되어 오히려 미개인들의 우상을 맹신하는 노예가 된다. 그리고　그는　프랑스 병사들이 이 부락에 당도한 후에 온 선교사 일행을 우상의 명령으로 죽이고 만다. </p>",
  "background":"<p>－공간적　배경：　알제리　사막</p>",
  "character_net":{
    "chart":"Le_Renégat_ou_un_esprit_confus.jpg",
    "character":{
      "한　젊은이　(이름　없음)":"<p>소설에서의 역할: 주인공<br/>프랑스의 개신교 고장 출신의 젊은이. 카톨릭으로 개종하여 선교사가 된다. 그 후 위험한 지역이라고 사람들이 만류함에도 불구하고 알제리에 있는 미개인 부락으로 들어가지만 혀를 잘리고 감금되어 일상적으로 폭행을 당하며 지내게 된다. 그리고　결국　무자비한 야만성과 폭력에 의해 정신마저 미개인들의 우상의 노예가 된다. 폭력에 저항하지 못하고 굴종하고 마는 나약한 한 개인의 모습, 야만적인 폭력에 길들여져 버리는 정신적 노예의 비참하게 파괴되는 인간성을　나타낸다.</p>"
    }
  },
  "style":"<p>1인칭 화법. 현재와 과거를 오가며 사건 묘사.</p>",
  "network1":`<div>
    <ul>
      <li>안과 겉 L'envers et l'endroit, 1937. 알제에서 보낸 작가의 가난한 어린 시절과 외부 세계의 발견이라는 모티프의 공통점으로 이 작품과 연결됨.</li>
      <li><a href='#시지프의 신화'>시지프의　신화 Le mythe de Sisyphe，1942.</a></li>
      <li><a href='#이방인'>이방인 L'Étranger，1942.</a></li>
      <li><a href='#전락'>전락　La chute, 1956.</a></li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
      <section>
        <h2>해설</h2>
        <ul>
          <li>Owen J. Miller, &lt;L'exil et le royaume: Cohérence du recueil&gt;, 《Albert Camus 6 Camus nouvelliste:L'exil et le royaume》, La Revue des Lettres Modernes(Minard, 1973)</li>
          <li>Roger Grenier, 《Albert Camus Soleil et Ombre》(Gallimard, 1987)</li>
          <li>Roger Quilliot, &lt;Prèsentation de L'exil et le royaume&gt;, Albert Camus, 《Théâtre, Récits, Nouvelles, Textes》 réunis et annotés par Roger Quillot, Bibliothèque de la pléiade, (Gallimard, 1962)</li>
        </ul>
      </section>
      <section>
        <h2>논문</h2>
        <ul>
          <li>김진식　. (1994)，알베르　카뮈의　『適地와　王國』　연구　，　한국불어불문학회,</li>
          <li>박홍규，(2001)，　사이드와　카뮈，　영남대학교　인문과학연구소　인문연구　제61호 1-30(총 30페이지)</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://fr.wikipedia.org/wiki/L%27Exil_et_le_Royaume' target='_blank'>Wikipedia －L'Exil et le Royaume</a></li>
      <li>적지와　왕국　（이미지）<br/>
        <img src='http://libresavoir.org/ressources/img/l-Exil-et-le-royaume_Albert-Camus.jpg' width='150'/>
        <img src='http://www.ameliesourget.com/wp-content/uploads/2014/11/Camus.jpg'/></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"단편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"말없는 사람들 (『적지와 왕국』 셋째 단편)　Les Muets",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1957,
  "language":"프랑스어",
  "t_title":"말없는 사람들",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["파업(grève)","바다(mer)"],
  "keywords2":["적지(exill)","왕국(royaume)","고독(solitaire)","연대(solidaire)","대조(contraste)","해방(libération)","부조리(absurdité)","실존주의(existentialisme)"],
  "story":"<p>술통을 만드는 영세공장의 직원들이 선박과 액체 운반용 자동차가 생산되는 바람에 술통　제조업은　위기에　처하게　된다. 술통이 많이 팔리지 않아서 물가가 올라도 봉급이 오르지 않게　되고　박봉에　시달린　노동자들은 20일간 파업을 한다. 　그러나 아무 소득도 없이 공장에 복귀한다. 다만 항의의 표시로 사장과 대화를 하지 않고 묵묵히 일만 하는데 오후에 사장의 어린 딸이 갑자기 쓰러져서 구급차에 실려 갔다는 소식을 듣게 된다. 큰　술통을　수선하는　통장이 이바르는 봉급을 인상해 주지 않고 불경기의 책임을 직원들에게만 전가하며 야박하게 군 사장이지만 아버지의 대를 물려 술통 제조와 수선 사업을 하는 그가 직원들에게 다정하게 대했었던 평상시를 떠올리며 사장에게 위로의 말을 전할까 하다가 그냥 퇴근하고 만다. 귀가한 그는 바다가 보이는 테라스에서 아내　페르낭드와 함께 앉아 그날 있었던 일들을 모두 말해 주면서 자기가 젊었더라면 바다 건너 저쪽으로 떠났으리라는 회한에 잠긴다.</p>",
  "background":"<p>- 시간적 배경: 겨울</p><p>- 공간적 배경: 알제리 부둣가, 술통공장</p>",
  "character_net":{
    "chart":"Les_Muets.jpg",
    "character":{
      "이바르　Yvars":"소설에서의 역할: 주인공<br/>술통을 만드는 영세공장의 직원. 아내　페르낭드와　아들　아이를　거느린　중년의　가난한　가장이다. 다른　노동자들과　함께　느끼는　결속이나　동지애의　상징인　‘침묵’과　치명적인　병에　걸린　아이　앞에서　상심한　공장주에　대한　동정　사이에서　갈등한다. 봉급을 인상해 주지 않고 불경기의 책임을 직원들에게만 전가하며 야박하게 굴었던 사장이지만 아버지의 대를 물려 술통 제조와 수선 사업을 하는 그가 직원들에게 다정하게 대했었던 평상시　태도를 떠올리며 사장에게 위로의 말을 전할까 하다가 그냥 퇴근하고 만다.　공장주에게　동정의　말을　함으로서　자신에　대한　충실성을　극복하는데　실패하자　이바르는　타자에　대한　불충실의　슬픔을　뼈저리게　느낀다. 노사관계의 극단적인 갈등 속에서도 가족을 부양하기 위해 일하지 않을 수 없는 가장을　나타낸다.",
      "페르낭드　Fernande":"이바르의　아내. 이바르, 아들과　함께　살고　있다. 이바르와　함께　젊었더라면　바다　저쪽으로　떠났으리라는　회한에　잠긴다.",
      "라살 Lassalle":"술통공장의　주인. 아버지의　사업을　물려받아　공장을　꾸려가고　있으며　거의　모든　직공들을　오래전부터　알고　있었다.<br/>직공들을　아꼈으나　술통　제조업이　위기에　처하는　바람에　직원들의　봉급을　올리지　않는다.",
      "에스포지토 Esposito":"이바르의　옆자리에서　일하는　키　크고　풍채　좋은　갈색머리의　털보.",
      "마르쿠 Marcou":"조합대표, 테노리노　가수　같은　용모.",
      "사이드 Said":"공장에서　유일한　아랍인.",
      "발레스테르　Ballester":"공장　감독, 직공들　중　제일　연장으로　파업을　반대했다.",
      "발르리　Valery":"나이　어린　공원."
    }
  },
  "style":"<p>3인칭 관찰자 시점. 사건 서술 중심</p>",
  "network1":`<div>
    <ul>
      <li>안과 겉 L'envers et l'endroit, 1937. 알제에서 보낸 작가의 가난한 어린 시절과 외부 세계의 발견이라는 모티프의 공통점으로 이 작품과 연결됨.</li>
      <li><a href='#시지프의 신화'>시지프의　신화 Le mythe de Sisyphe，1942.</a></li>
      <li><a href='#이방인'>이방인 L'Étranger，1942.</a></li>
      <li><a href='#전락'>전락　La chute, 1956.</a></li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
      <section>
        <h2>해설</h2>
        <ul>
          <li>Owen J. Miller, &lt;L'exil et le royaume: Cohérence du recueil&gt;, 《Albert Camus 6 Camus nouvelliste:L'exil et le royaume》, La Revue des Lettres Modernes(Minard, 1973)</li>
          <li>Roger Grenier, 《Albert Camus Soleil et Ombre》(Gallimard, 1987)</li>
          <li>Roger Quilliot, &lt;Prèsentation de L'exil et le royaume&gt;, Albert Camus, 《Théâtre, Récits, Nouvelles, Textes》 réunis et annotés par Roger Quillot, Bibliothèque de la pléiade, (Gallimard, 1962)<li>
        </ul>
      </section>
      <section>
        <h2>논문</h2>
        <ul>
          <li>김진식, (1994),알베르 카뮈의　『適地와　王國』　연구,　한국불어불문학회,</li>
          <li>박홍규, (2001), 사이드와 카뮈, 영남대학교 인문과학연구소 인문연구 제61호 1-30(총 30페이지)</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://fr.wikipedia.org/wiki/L%27Exil_et_le_Royaume' target='_blank'>Wikipedia －L'Exil et le Royaume</a></li>
      <li>적지와　왕국　(이미지)<br/>
        <img src='http://libresavoir.org/ressources/img/l-Exil-et-le-royaume_Albert-Camus.jpg' width='150'/>
        <img src='http://www.ameliesourget.com/wp-content/uploads/2014/11/Camus.jpg' width='150'/></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"단편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"손님(『적지와 왕국』 넷째 단편)　L'Hôte",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1957,
  "language":"프랑스어",
  "t_title":"손님",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["사막(désert)","바다(mer)","고원(hauteur)"],
  "keywords2":["적지(exill)","왕국(royaume)","고독(solitaire)","연대(solidaire)","대조(contraste)","해방(libération)","부조리(absurdité)","실존주의(existentialisme)"],
  "story":"<p>고원에 위치한 학교에서 아이들을 가르치는 교사 다뤼에게 어느　날 늙은 헌병이 양식을 꿔 가고 갚지 않은 사촌을 죽인 아랍인을 끌고 와서 자신은 일손이 모자라 근무지로 빨리 돌아가야 하니 죄수를 하룻밤 여기서 재우고 내일 아침에 여기서 20킬로미터 떨어져 있는 탱기의 경찰서로 끌고 가서 인계해 주라고 맡겨 놓는다. 다뤼는 자기 일이 아니라고 거절하지만 늙은 헌병은 전시에는 직무에 상관없이 무슨 일이든지 분담해서 해야 된다며 강제로 죄수를 탱기까지 끌고 가는 일을 맡기는데 다뤼는 이 죄수를 탱기에 넘겨 주지 않겠다고 말한다. 늙은 헌병 발뒤시는 만일 그렇게 해도 같은 편(같은 프랑스인)을 고발하지는 않겠다며 다뤼가 자신을 모욕했다고 불쾌해 하면서 학교를 떠난다.</p><p>다뤼는 죄수를 학교에서 하룻밤 재우고 나서 아침을 함께 먹고 탱기를 향해 두 시간쯤 죄수와 함께 걸은 후에 죄수에게 이틀은 견딜 수 있는 식량과 돈을 주며 두 시간만 걸으면 닿게 되는 탱기로 가는 동쪽 길과 고원을 횡단해서 하루만 걸어가면 목초지와 가장 가까이 있는 유목민들을 만나게 되는 남쪽 길을 가르쳐주며 이제는 혼자서 가라고 말해준다. 죄수는 어리둥절해 하다가 홀로 길을 가는데 다뤼가 학교로 되돌아오다가 조망이 좋은 곳에서 뒤를 돌아보니 죄수는 감옥에 갇히기 위해 탱기로 가고 있었다. 학교로 돌아온 다뤼는 흑판에 분필로 쓴 글씨를 보게 된다. - '너는 우리의 형제를 넘겨 주었다. 그 대가를 치르리라.' 다뤼는 하늘과 고원, 그리고 저 너머 바다에 이르기까지 펼쳐진 보이지 않는 땅들을 응시하고 있었다. 그가 그토록 사랑했던 이 광막한 고장에서 그는 혼자였다.</p>",
  "background":"<p>－시간적　배경：　10월　중순</p><p>－공간적　배경：　알제리　고원의　고립된　학교</p>",
  "character_net":{
    "chart":"hote.jpg",
    "character":{
      "다뤼　Daru":"소설에서의 역할: 주인공<br/>고원에 위치한 학교에서 아이들을 가르치는 교사. 알제리인은 아니지만 1830년부터 1962년까지 132년에 걸친 기나긴 식민지 통치기간 동안 이곳에서 태어나 고향이 된 이 땅에 정을 붙이며 오래 살아온 프랑스인 다뤼는 척박하고 가난한 고장인　알제리에 대한 애정을　가지고　있다. 죄수를 학교에서 하룻밤 재우고 나서 아침을 함께 먹고 탱기를 향해 두 시간쯤 죄수와 함께 걸은 후에 죄수에게 이틀은 견딜 수 있는 식량과 돈을 주며 두 시간만 걸으면 닿게 되는 탱기로 가는 동쪽 길과 고원을 횡단해서 하루만 걸어가면 목초지와 가장 가까이 있는 유목민들을 만나게 되는 남쪽 길을 가르쳐주며 이제는 혼자서 가라고 말해준다.",
      "아랍인　un arabe":"양식을 꿔 가고 갚지 않은 사촌을 죽였다. 발뒤시에　의해　다뤼가　있는　학교로　이송되어　다뤼와　함께　보낸다. 다뤼가　탱기로 가는 동쪽 길, 유목민들을 만나게 되는 남쪽 길을　소개해　주지만　스스로　탱기로　가는　길을　택한다.",
      "발뒤시　Balducci":"늙은　헌병. 엘　아뫼르에서　아랍인　죄수를　호송해　왔다. 다뤼에게　죄수를 탱기까지 끌고 가는 일을 맡긴다."
    }
  },
  "style":"<p>3인칭 관찰자 시점. 사건 서술 중심. 문장이 간결하다. </p>",
  "network1":`<div>
    <ul>
      <li>안과 겉 L'envers et l'endroit, 1937. 알제에서 보낸 작가의 가난한 어린 시절과 외부 세계의 발견이라는 모티프의 공통점으로 이 작품과 연결됨.</li>
      <li><a href='#시지프의 신화'>시지프의　신화 Le mythe de Sisyphe，1942.</a></li>
      <li><a href='#이방인'>이방인 L'Étranger，1942.</a></li>
      <li><a href='#전락'>전락　La chute, 1956.</a></li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":`<div>
    <section>
      <h2>만화</h2>
      <ul>
        <li>손님, 자크　페랑데즈　Jacques ferrandez, 2014, 문학동네.<br/>
          <a href='http://m.book.naver.com/bookdb/book_detail.nhn?biblio.bid=7409626' target='_blank'>http://m.book.naver.com/bookdb/book_detail.nhn?biblio.bid=7409626</a><br/>
          <img src='http://www.dokamo.nc/wp-content/uploads/2012/03/BD-couverture2.jpg' width='150'/><br/>
          <a href='http://livre.fnac.com/a2741354/Jacques-Ferrandez-L-hote' target='_blank'>http://livre.fnac.com/a2741354/Jacques-Ferrandez-L-hote</a></li>
      </ul>
    </section>
    <section>
      <h2>영화</h2>
      <ul>
        <li><a href='http://www.allocine.fr/film/fichefilm_gen_cfilm=221570.html' target='_blank'>"Loin des hommes" - David Oelhoffen, 2014. (손님　L'Hôte)</a></li>
      </ul>
    </section>
  </div>`,
  "character_network":"",
  "social_network":`<div>
      <section>
        <h2>해설</h2>
        <ul>
          <li>Owen J. Miller, &lt;L'exil et le royaume: Cohérence du recueil&gt;, 《Albert Camus 6 Camus nouvelliste:L'exil et le royaume》, La Revue des Lettres Modernes(Minard, 1973)</li>
          <li>Roger Grenier, 《Albert Camus Soleil et Ombre》(Gallimard, 1987)</li>
          <li>Roger Quilliot, &lt;Prèsentation de L'exil et le royaume&gt;, Albert Camus, 《Théâtre, Récits, Nouvelles, Textes》 réunis et annotés par Roger Quillot, Bibliothèque de la pléiade, (Gallimard, 1962)</li>
        </ul>
      </section>
      <section>
        <h2>논문</h2>
        <ul>
          <li>김진식, (1994), 알베르　카뮈의　『適地와　王國』　연구, 한국불어불문학회,</li>
          <li>박홍규, (2001), 사이드와　카뮈, 영남대학교　인문과학연구소　인문연구　제61호 1-30(총 30페이지)</li>
          <li>이상돈, 박명화, (2014), 정의의　아포리아(aporia)외형의　유예제도에　대한　법문학적　이해－카뮈의　&lt;손님&gt;을　예로　하여, 고려대학교　법학원, 고려법학, 72권, 단일호, p.209-252.</li>
          <li>조병준, (2014), 카뮈의　「손님」과　사르트르의　「벽」이　보여주는　서술공간　비교, 한국불어불문학회, 불어불문학연, 100권, p.623-662.</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://fr.wikipedia.org/wiki/L%27Exil_et_le_Royaume' target='_blank'>Wikipedia －L'Exil et le Royaume</a></li>
      <li>적지와　왕국　(이미지)<br/>
        <img src='http://libresavoir.org/ressources/img/l-Exil-et-le-royaume_Albert-Camus.jpg' width='150'/>
        <img src='http://www.ameliesourget.com/wp-content/uploads/2014/11/Camus.jpg'/></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"단편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"요나 혹은 작업 중의 예술가 (『적지와 왕국』 다섯 째 단편) Jonas ou L'Artiste au travail",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1957,
  "language":"프랑스어",
  "t_title":"요나",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["사막(désert)","바다(mer)","연대(solidaire)"],
  "keywords2":["적지(exill)","왕국(royaume)","대조(contraste)","고독(solitaire)","해방(libération)","부조리(absurdité)","실존주의(existentialisme)"],
  "story":"<p>화가인 요나는 그림 그리는 재주로 명성을 얻기 시작하면서 결혼도 하고 자식들도 낳아서 키우게 되는데 아파트를 얻어 화실을 만들어서 그림을 그리지만 그의 명성 때문에 찾아오는 사람들로 인해 그의 그림 그리는 작업은 방해를 받게 된다. 하지만 그는 자신의 직업과 명성에 관심을 가지는 사람들을 물리칠 수 없어서 그런 내색을 하지 못한다. 결국 그는 그를 방문하는 사람들에게 시달리면서 작업을 하다가 내리막길로 접어들게 되고 그림도 거의 그리지도 못하면서 매스 미디어와 사람들의 비판에 시달리게 된다.　찾아오는 사람들도 줄어들지만 그는 아파트의 층고가 유난히 높은 것을 이용해서 아파트의 윗부분에 횃대 같은 다락을 만들어서 그 위에 홀로 칩거하며 그림을 그리려고 시도한다.　결국 찾아오던 사람들은 그의 모습이 보이지 않자 전부 발길을 끊게 되고 오랜 친구인 라토와 아내　루이즈만이 그의 안부를 물을 뿐이다. 그는 아예 다락에서 내려오지도 않은 채 끼니를 거르며 그림도 그리지 않고 상념에 몰두해 있다가 쓰러지게 된다．그의 화폭의 한가운데에 요나가 아주 작은 글씨로 써 놓은 단어는 '솔리테르(solitaire : 고독)'라고 읽어야 할지 '솔리데르(solidaire : 연대의)'라고 읽어야 할지 알 수 없었다.</p>",
  "background":"<p>- 공간적 배경: 파리의 중산층 주거구역</p>",
  "character_net":{
    "chart":"yona.jpg",
    "character":{
      "질베르　요나　Gilbert Jonas":"소설에서의 역할: 주인공<br/>화가. 재능으로 인해 성공하여 얼마간의 부와 함께 명성을 얻지만 그게 오히려 족쇄가 되어 사생활을 공공연히 침해당하고 팬들에게 충실하기 위해 예술적인 성취를 소홀히 하게 되며 타인의 비평에 현혹되어 타의에 추종하는 그림을 그리다가 화가로서　내리막길에 접어들게　된다．",
      "루이즈　Louise":"요나의　아내. 근면한　성격이며　남편인　요나에게　헌신한다.<br/>출판이　요나의　관심거리일　때는　문학에　전념했으며　그가　그림에　열중하자　그녀는　조형예술에　헌신하고　요나를　데리고　미술회, 전람회를　쫓아다녔다. 아이가　생긴　후　요나가　다락에서　작업에　몰두할　때도　식사, 식료품을　마련해　주었다.",
      "라토 Rateau":"고등학교　때부터　요나와　친했던　친구. 건축가이다."
    }
  },
  "style":"",
  "network1":`<div>
    <ul>
      <li>안과 겉 L'envers et l'endroit, 1937. 알제에서 보낸 작가의 가난한 어린 시절과 외부 세계의 발견이라는 모티프의 공통점으로 이 작품과 연결됨.</li>
      <li><a href='#시지프의 신화'>시지프의　신화 Le mythe de Sisyphe，1942.</a></li>
      <li><a href='#이방인'>이방인 L'Étranger，1942.</a></li>
      <li><a href='#전락'>전락　La chute, 1956.</a></li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":`<div>
    <h2>만화</h2>
    <ul>
      <li>요나, GRAPHIC NOVEL Jonas oder der Künstler bei der Arbeit</li>
      <li><a href='http://www.katiafouquet.com/' target='_blank'>Albert Camus / Katia Fouquet Edition Büchergilde</a></li>
    </ul>
  </div>`,
  "character_network":"",
  "social_network":`<div>
      <section>
        <h2>해설</h2>
        <ul>
          <li>Owen J. Miller, &lt;L'exil et le royaume: Cohérence du recueil&gt;, 《Albert Camus 6 Camus nouvelliste:L'exil et le royaume》, La Revue des Lettres Modernes(Minard, 1973)</li>
          <li>Roger Grenier, 《Albert Camus Soleil et Ombre》(Gallimard, 1987)</li>
          <li>Roger Quilliot, &lt;Prèsentation de L'exil et le royaume&gt;, Albert Camus, 《Théâtre, Récits, Nouvelles, Textes》 réunis et annotés par Roger Quillot, Bibliothèque de la pléiade, (Gallimard, 1962)</li>
        </ul>
      </section>
      <section>
        <h2>논문</h2>
        <ul>
          <li>김진식, (1994), 알베르　카뮈의　『適地와　王國』　연구, 한국불어불문학회,</li>
          <li>박홍규, (2001), 사이드와　카뮈, 영남대학교　인문과학연구소　인문연구　제61호 1-30(총 30페이지)</li>
          <li>송가현,(2001), 까뮈의 예술에 대한 소명의식: Jonas ou l'artiste at travail를 중심으로, 학위논문(석사), 연세대학교.</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://fr.wikipedia.org/wiki/L%27Exil_et_le_Royaume' target='_blank'>Wikipedia - L'Exil et le Royaume</a></li>
      <li>적지와 왕국 (이미지)<br/>
        <img src='http://libresavoir.org/ressources/img/l-Exil-et-le-royaume_Albert-Camus.jpg' width='150'/>
        <img src='http://www.ameliesourget.com/wp-content/uploads/2014/11/Camus.jpg'/></li>
      <li>요나(이미지)<br/>
        <img src='https://sabariscon.files.wordpress.com/2014/02/jonas.jpg' width='150'/></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"단편소설",
  "author":"알베르 카뮈 Albert Camus",
  "title":"자라나는　돌(『적지와 왕국』 여섯 째 단편)　La Pierre qui pousse",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1957,
  "language":"프랑스어",
  "t_title":"자라나는 돌",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["돌(pierre)","고독(solitaire)"],
  "keywords2":["적지(exill)","왕국(royaume)","연대(solidaire)","부조리(absurdité)","실존주의(existentialisme)"],
  "story":"<p>브라질의 한 지방인 이과페에 홍수를 방지하기 위해 작은 제방을 만들러 간 프랑스의 토목기사 다라스트. 그는 시장, 판사, 경찰서장 등의 환대를 받다가 흑인 요리사 한 사람을 사귀게 되는데 그는 유조선에 탔다가 배에 불이 나서 간신히 배를 탈출하여 서투른 헤엄으로 익사하지 않고 해안에 닿게 된다. 그는　위급할 당시에 자기를 살려주면 50 킬로그램의 돌을 머리에 이고 행진하겠다고 예수께 맹세했었는데 이제 그 맹세를 실천하겠다고 말한다.　다라스트는 흑인 요리사와 함께 주민들의 축제에 동참하는데 그는 밤에 숙소로 돌아가지만 그 흑인 요리사는 밤새 축제에서 춤을 추다가 무거운 돌을 머리에 이고 교회를 향해 걸음을 옮긴다. 그러나 밤새 춤을 추어서 지칠대로 지친 요리사는 돌의 무게에 고통스러워 하다가 결국 돌을 땅에 떨어뜨리고 피와 땀으로 범벅이 된다. 다라스트는 그 돌을 대신 들고는 교회로 가지 않고 요리사의 오두막집으로 걸음을 옮겨서 방 한가운데의 불이 피워져 있던 자리에 내려놓는다.</p>",
  "background":"<p>－ 공간적　배경:　브라질의 한 지방인 이과페</p>",
  "character_net":{
    "chart":"La_Pierre_qui_pousse.jpg",
    "character":{
      "다라스트 D'Arrast":"소설에서의 역할: 주인공<br/>프랑스의 토목기사. 브라질의 한 지방인 이과페에 홍수를 방지하기 위해 작은 제방을 만들러 갔다. 흑인　요리사의　맹세를　대신　이행해준다. 동료애, 그리고 사람과 고통을 함께 짊어진다는 연대의식을　보인다.",
      "흑인　요리사 un coq":"유조선에 탔다가 배에 불이 나서 간신히 배를 탈출하여 서투른 헤엄으로 익사하지 않고 해안에 닿게 되자 위급할 당시에 자기를 살려주면 50 킬로그램의 돌을 머리에 이고 행진하겠다고 예수께 맹세했었다.　돌의 무게에 고통스러워 하다가 결국 돌을 땅에 떨어뜨리고 피와 땀으로 범벅이 된다.",
      "소크라트 Socrate":"이과페까지　다라스트를　데려다　준　운전사.　다라스트에게　흑인　요리사를　소개시켜　준다."
    }
  },
  "style":"<p>- 3인칭 관찰자 시점. 사건 서술 중심. 문장이 간결하다.</p>",
  "network1":`<div>
    <ul>
      <li>안과 겉 L'envers et l'endroit, 1937. 알제에서 보낸 작가의 가난한 어린 시절과 외부 세계의 발견이라는 모티프의 공통점으로 이 작품과 연결됨.</li>
      <li><a href='#시지프의 신화'>시지프의　신화 Le mythe de Sisyphe，1942.</a></li>
      <li><a href='#이방인'>이방인 L'Étranger，1942.</a></li>
      <li><a href='#전락'>전락　La chute, 1956.</a></li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
      <section>
        <h2>해설</h2>
        <ul>
          <li>Owen J. Miller, &lt;L'exil et le royaume: Cohérence du recueil&gt;, 《Albert Camus 6 Camus nouvelliste:L'exil et le royaume》, La Revue des Lettres Modernes(Minard, 1973)</li>
          <li>Roger Grenier, 《Albert Camus Soleil et Ombre》(Gallimard, 1987)</li>
          <li>Roger Quilliot, &lt;Prèsentation de L'exil et le royaume&gt;, Albert Camus, 《Théâtre, Récits, Nouvelles, Textes》 réunis et annotés par Roger Quillot, Bibliothèque de la pléiade, (Gallimard, 1962)</li>
        </ul>
      </section>
      <section>
        <h2>논문</h2>
        <ul>
          <li>김진식, (1994), 알베르　카뮈의　『適地와　王國』　연구, 한국불어불문학회,</li>
          <li>박홍규, (2001), 사이드와　카뮈, 영남대학교　인문과학연구소　인문연구　제61호 1-30(총 30페이지)</li>
          <li>변광배，(2013), 카뮈, 서약, ‘우리’：「자라나는　돌」의　두　장면을　중심으로，　한국　프랑스학회　2013년도　추계　학술대회.</li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://fr.wikipedia.org/wiki/L%27Exil_et_le_Royaume' target='_blank'>Wikipedia －L'Exil et le Royaume</a></li>
      <li>적지와　왕국　(이미지)<br/>
        <img src='http://libresavoir.org/ressources/img/l-Exil-et-le-royaume_Albert-Camus.jpg' width='150'/>
        <img src='http://www.ameliesourget.com/wp-content/uploads/2014/11/Camus.jpg' width='150'/></li>
      <li>자라나는　돌　(이미지)<br/>
        <img src='http://image.anobii.com/anobi/image_book.php?item_id=01f228a08f8f8bdab8&time=&type=4' width='150'/></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"희곡",
  "author":"알베르 카뮈 Albert Camus",
  "title":"칼리굴라　Caligula",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1938,
  "language":"프랑스어",
  "t_title":"칼리굴라",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["달(lune)","자유(liberté)","죽음(mort)","해방(libération)","불행(malheur)","신(dieu)","그건 아무래도 상관없다.(cela ne signifiait rien)"],
  "keywords2":["부조리(absurdité)","실존주의(existentialisme)"],
  "story":"<p><b>1938년 집필, 1945년 초연.</b></p><p>제1막(제11장): 사랑하던 여동생 드루실라의 죽음에 괴로워 하던 왕 칼리굴라가 궁정에서 사라진 지 3일째다.</p><p>돌아온 그는 시인처럼 순수했던 모습에서 변해 이제 자유를 위해 절대적이고 광포한 권력을 휘두르려 한다.</p><p>제2~4막: 제2막(제14장)은 3년 뒤, 왕에 의해 행해지는 살해, 강간 등 죽음 앞에 몸부림치는 그의 악행이 여러모로 묘사된다.</p><p>하지만 아들을 잃은 자, 아내를 능욕당한 자도  칼리굴라 앞에선 두려움으로 대항하지 못하고, 아비를 잃은 시인 스키피오도 그를 마냥 미워할 수조차 없게 된다. 역모를 알게 된 왕은 기다렸다는 듯이 행동하며 두려움의 기색은 보이지 않고 귀족들을 당황시킨다.</p><p>제3막(제6장)은 칼리굴라가 자신의 논리에 지친 모습을 보여주며, “휴식”을 원한다.</p><p>제4막(제14장)은 폭정에 대한 결과로서의 귀족들의 반란이 준비되고, 칼리굴라 역시 그가 마음껏 행사하는 ‘자유’의 바탕을 의심스러워하면서, 죽음을 수락한다. 결국 왕은 케레아와 노귀족의 손에 죽게 된다.</p>",
  "background":"<p>시간적 배경 - 로마 제국시대 칼리굴라 황제 집정시기<br/>로마 제국 제3대 황제 가이우스 시저 Gaius Caesar를 모델로 함.<br/>(AD 12. 8. 31~ 41. 1. 24.)</p><p>공간적 배경 - 로마 제국, 칼리굴라의 궁전</p>",
  "character_net":{
    "chart":"caligula.jpg",
    "character":{
      "칼리굴라 Caligula":"주인공, 로마제국의 황제. 누이동생이 사망하자 인간의 유한성에 회의를 느끼며 ‘모든 인간은 죽는다.’는 절대적인 부조리를 인식하게 된다. ‘인간들은 죽는다. 그래서 행복하지 못하다.’는 결론을 내린 그는 스스로 인간임을 포기하고 신이 되고자 한다. 드루실라의 죽음을 계기로 칼리굴라는 불가능한 것을 열망하기 시작하며 불가능한 것을 추구한 까닭에 점점 무자비하게 변해간다. 그리고 끝내 자신의 부조리 논리가 틀렸다는 것을 인정하고 그가 고의로 야기한 암살에 의해서 죽는다.",
      "드루실라 Drusilla":"칼리굴라의 연인이자 여동생. 칼리굴라가 부조리를 인식하는 계기가 된다.",
      "케소니아 Caesonia":"칼리굴라의 네 번째 부인, 칼리굴라를 사랑하여 그를 도울 수밖에 없는 인물이다. 사랑으로 칼리굴라의 모든 것을 수용, 복종한다.",
      "헬리콘 Hélicon":"이전에 노예였으며 칼리굴라를 통해 해방되었다. 칼리굴라의 악행을 참는 귀족들을 가리켜 “고통도 맛본 적 없고 위험도 무릅써 본 적 없는” 탐욕스러운 거짓말쟁이라고 비난한다. 그는 군주가 폭정을 가하는 상황에서 귀족이나 민중이 행할 수 있는 유일한 자유는 ‘반항’이라는 것을 안다.",
      "케레아 Cherea":"문예가, 지식인. 칼리굴라의 폭정이 두려워서가 아니라, 그의 ‘철학’을 위험하게 여긴 유일한 인물이다.",
      "세넥투스(노귀족) Senectus":"죽음과 권력 앞에서 비굴한 모습을 보이는 늙은 귀족이다.",
      "메텔루스 Metellus":"세습귀족",
      "레피두스 Lepidus":"세습귀족, 칼리굴라가 그의 아들을 살해하고 레피두스가 웃도록 명령하였다.",
      "옥타비우스 Octavius":"세습귀족, 칼리굴라가 그의 아내를 매춘부로 만든다.",
      "메레이아 Mereia":"세습귀족",
      "Mucius 무키우스":"세습귀족, 칼리굴라가 그의 아내를 범한다.",
      "스키피오 Scipion":"젊은 시인. 자신의 아버지를 죽인 칼리굴라의 순수한 영혼을 사랑하는 시인이다. 칼리굴라의 극단적 행동에 반기를 든다."
    }
  },
  "style":"<p>희곡으로서 막과 장으로 구성되어 있으며 행동과 대사로 사건이 전개됨. 사건 중심으로 흐름.</p>",
  "network1":`<div>
      <p>1. 시지프의 신화 Le mythe de Sisyphe, 알베르 카뮈 Albert Camus, 1942.<br/>
      - &lt;칼리굴라&gt;와 유사하게 인간 실존과 부조리에 대한 성찰을 다룬다.<br/>
      - <a href='http://terms.naver.com/entry.nhn?docId=2012359&cid=41773&categoryId=44397' target='_blank'>[네이버 지식백과] 시지프의 신화 [Le Mythe de Sisyphe] (낯선 문학 가깝게 보기 : 프랑스문학, 2013. 11, 인문과교양)</a></p>
      <p>2. 이방인 L'Étranger, 알베르 카뮈 Albert Camus, 1942.<br/>
      - &lt;칼리굴라&gt;와 유사하게 인간 실존과 부조리에 대한 성찰을 다룬다.<br/>
      - <a href='https://fr.wikipedia.org/wiki/L%27%C3%89tranger' target='_blank'>&lt;이방인&gt;에 대한 위키피디아 사이트</a></p>
  </div>`,
  "network2":`<div>
     <p>1. 열두 명의 카이사르 Vie des douze Césars, 수에토니우스 Suétone,  119, 122<br/>
      - &lt;칼리굴라&gt;는 ‘수에토니우스(Suétone)’의 ‘열두 명의 카이사르(Vie des douze Césars)’에서 소재를 차용했다.<br/>
      - <a href='https://fr.wikipedia.org/wiki/Vie_des_douze_C%C3%A9sars' target='_blank'>&lt;열두 명의 카이사르&gt;에 대한 위키피디아 사이트</a></p>
  </div>`,
  "network3":`<div>
    <h2>공연</h2>
    <p>1. 칼리굴라_리믹스 (Caligula_Remix), 연출가 마크 보프레(Marc BeauprE)<br/>
      - &lt;칼리굴라&gt;를 소재로 한 음악극.<br/>
      - <a href='http://2013.umtf.or.kr/html/sub02/sub_01_01.php' target='_blank'>‘마크 보프레(Marc BeauprE)’의 공연, ‘칼리굴라_리믹스 (Caligula_Remix)’에 대한 ‘2013 의정부국제 음악극축제’ 사이트의 소개.</a></p>

    <h2>연극</h2>
    <p>2. 칼리큘라, 연출 최진아<br/>
    - &lt;칼리굴라&gt;를 소재로 한 연극.<br/>
    - <a href='http://me2.do/G2q1sKge' target='_blank'>‘최진아’의 공연, ‘칼리큘라’에 대한 ‘한국문화예술위원회’ 사이트의 소개.</a></p>

    <h2>오페라발레</h2>
    <p>3. Stéphane Bullion - Caligula, emperor of Rome<br/>
    Choreography by Nicolas le Riche<br/>
    - &lt;칼리굴라&gt;를 소재로 한 오페라발레.<br/>
    - <a href='https://www.youtube.com/watch?v=72w6QN3mcEY' target='_blank'>Stéphane Bullion의 ‘Caligula, emperor of Rome’에 대한 유튜브(Youtube) 페이지.</a></p>
  </div>`,
  "character_network":"<p>폴 외틀리 Paul Œttly<br/>1945년 칼리굴라 초연 연출</p>",
  "social_network":`<div>
      <section>
        <h2>논문</h2>
        <ul>
          <li>문나리. (2006),카뮈(Albert CAMUS)의 ｢Caligula｣에서의 행위소 모델 연구, 서울 : 숙명여자대학교 대학원,</li>
          <li>부새롬,(2014),칼리큘라는 왜, 무엇과 싸우고 있는가, 공연과 이론. 통권 55호 (2014년 가을) pp.87-90</li>
          <li>송민숙, (1998), 알베르 카뮈의 극 칼리귤라연구: 부조리와 잔혹, 불어불문학연구 36권 1호 시작쪽수 177p, 전체쪽수 18p 1226-4350 KCI</li>
          <li>송민숙. (2013). 〈칼리굴라 리믹스〉_사운드 리믹스를 통해 부조리를 전달하는 퀘벡 테르데좀므 극단의 〈칼리굴라 리믹스〉. 공연과이론, (50), 259-263.</li>
          <li>양기찬, (2014),&lt;칼리큘라&gt;, 삶의 가치에 대한 논증, 공연과 이론. 통권 55호 (2014년 가을) pp.83-86.</li>
          <li>최진아 , 부새롬 , 양기찬 , 박연숙 , 손찬호 , 김영은 , 정낙현 , 이양숙 , 반무섭 , 오세곤 [외],(2014), &lt;칼리큘라&gt; &lt;토론&gt;,공연과 이론. 통권 55호 (2014년 가을) pp.91-126.</li>
          <li>하은정,(2006), 알베르 카뮈의 칼리귈라에서 나타난 죽음과 자유의 의미, 서울 : 연세대학교 대학원.</li>
        </ul>
        <h2>책</h2>
        <ul>
          <li><a href='http://www.amazon.fr/Caligula-Camus-Transhistoriques-Sophie-Bastien/dp/9042019689/ref=sr_1_11?s=books&ie=UTF8&qid=1454407765&sr=1-11&keywords=Ignazio+Silone++Le+pain+et+le+vin' target='_blank'>Sophie Bastien, Caligula et Camus, Editions Rodopi B.V., 2006.</a></li>
        </ul>
      </section>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://en.wikipedia.org/wiki/Caligula_(play)' target='_blank'>Wikipedia -Caligula (play)</a></li>
      <li><a href='https://fr.wikipedia.org/wiki/Caligula_(Camus)' target='_blank'>Wikipedia -Caligula (Camus)</a></li>
      <li>표지 (이미지)<br/>
        <img src='http://www.musanostra.fr/caligula1.jpg' width='150'/></li>
      <li>1945년 초연 광고 (이미지)<br/>
        <img src='http://images.delcampe.com/img_large/auction/000/156/517/471_001.jpg' width='150'/></li>
      <li><a href='https://www.youtube.com/watch?v=zEECzApPQ_E&list=PLspayHD-jYsEq-heK9tUAMDnM90ztG5nF' target='_blank'>칼리굴라 관련 영상</a></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"희곡",
  "author":"알베르 카뮈 Albert Camus",
  "title":"오해 Le malentendu",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1944,
  "language":"프랑스어",
  "t_title":"오해",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["살인(meurtre)","범죄(crime)"],
  "keywords2":["부조리(absurde)","오해(malentendu)","살인(meurtre)","비극(tragédie)","인간의 조건(condition humaine)"],
  "story":"<p>마르타와 어머니는 작은 여관을 경영한다. 그들은 고단한 삶에서 벗어나기 위해 습관처럼 부유한 1인 투숙객들을 죽이고 돈을 훔친다. 어느 날, 돈이 많아보이는 한 남자가 여관으로 들어선다. 그는 20년 전 어머니와 마르타를 두고 돈벌이를 하러 나선 후 20년 만에 고향에 돌아온 어머니의 아들이자 마르타의 오빠인 얀이다. 얀은 어머니와 누이를 놀라게 해 줄 생각에 아내 마리아와 아이들을 남겨 두고 어머니가 경영하는 여관으로 온 것이다. 하지만 마르타와 어머니가 자신을 알아보지 못하자 얀은 자신의 신분을 밝히지 않은 채 그 여관에 묵을 결심을 한다. 얀은 다른 투숙객들과 다르게 마르타와 어머니에게 계속해서 말을 걸지만, 얀의 의도와 다르게 그들의 대화는 자꾸만 오해를 불러일으킨다. 밤중에 마르타와 어머니는 얀을 죽이고 강물에 던져버린다. 살인을 끝마치고 돌아온 마르타는 얀의 여권을 발견하고 그제서야 얀이 자신의 가족임을 알아챈다. 어머니는 자신이 아들을 죽인 것이나 다름없다고 말하며 목을 매어 자살한다. 이튿날 아침, 얀이 죽은 사실을 알 리 없는 마리아가 나타나 그의 신분을 밝힌다. 마르타는 마리아에게 자신과 어머니가 얀을 죽였다고 얘기한 후 자살하기 위해 강물로 향한다. 마리아는 신에게 구원을 요청하지만 버림받는다.</p>",
  "background":"<p>- 시간적 배경 : 깊은 밤</p><p>- 공간적 배경 : 여관</p>",
  "character_net":{
    "chart":"Le_malentendu.jpg",
    "character":{
      "얀 Jan":"‘어머니’의 아들이며 ‘마르타’와는 남매관계로, 20년 만에 고향에 돌아왔다. 어머니와 누이로부터 환영받고 싶지만 그들은 얀을 알아보지 못한다. 그들에게 자신을 설명할 말을 찾지 못한 채로 여관에 묵는다.",
      "마리아 Maria":"얀의 아내이다. 얀이 자신을 두고 어머니와 누이에게로 향하는 것을 이해하지 못 한다. 그녀는 낯선 환경과 낯선 나라에 대해 강한 불편함을 느낀다.",
      "어머니 La mère":"얀의 어머니이다. 별다른 희망감 없이, 현실로부터 도피하고 싶은 마음과 부유해지고 싶은 욕망으로 자신의 딸과 함께 살인을 저지른다. 마지막 살인을 끝으로 더 이상 누군가를 죽이고 싶어 하지 않는다.",
      "마르타 Martha":"반항적인 한편 열정적인 인물이다. 행복해지기 위해서는 살인을 통해 부유해져야 한다고 생각한다.",
      "늙은 하인":"-"
    }
  },
  "style":"<p>(희곡의 특성 상) 지문과 대사로 극이 진행된다.</p>",
  "network1":"<p>알베르 카뮈, &lt;이방인&gt; : &lt;이방인&gt;에서 뫼르소가 살인을 저지르고 수감되었을 때 찾아 읽게 되는 신문쪼가리 속에 나오는 기사내용이 본 작품에 매우 유사한 형태로 차용되었다.</p>",
  "network2":"-",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    <ul>
      <li>2004. 06. 연극 '보헤미안 랩소디' (류근혜 연출, 열린극단)</li>
      <li>2006. 연극 '오해' (나카지마 마코토 연출)</li>
      <li>2011. 연극 '오해' (윤종현 연출, 중앙대학교 연극학과 계절시리즈)</li>
      <li>2015. 02. 연극 '터'</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <ul>
      <li>초판본<br/>
        img src='http://81.169.222.198/still/kunst/pic570/397/411202500.jpg' width='150'/></li>
      <li><a href='http://book.naver.com/bookdb/book_detail.nhn?bid=7271725' target='_blank'>권기희 글, 이철희 그림, "페스트 (문학고전의 감동을 만화로 만난다)", 채우리, 2013</a></li>
      <li><a href='https://fr.wikipedia.org/wiki/La_Peste' target='_blank'>위키피디아 프랑스어 “페스트”</a></li>
    </ul>
  </div>`,
  "legends":"<p>고전 비극의 ‘3단일 법칙Unités de trois’ 형식을 따르고 있다. (*3단일 법칙 :고전 비극에서 지켜지는 규칙으로, 극 중에서 다뤄지는 사건의 기간이 하루를 초과해서는 안 된다는 '시간의 일치 unité de temps', 사건이 전개되는 장소가 한 곳으로 제한되어야 한다는 '공간의 일치unité de lieu', 그리고 극의 행위 즉 줄거리가 일관되어야 한다는 '행동의 일치 unité d'action' 세 가지로 구성되어 있다.</p>"
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"희곡",
  "author":"알베르 카뮈 Albert Camus",
  "title":"계엄령 L’État de Siège",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1948,
  "language":"프랑스어",
  "t_title":"계엄령",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["사랑(amour)","페스트(Peste)","두려움(peur)","존재(existence)"],
  "keywords2":["페스트(Peste)","실존주의(existentialisme)","저항(résistance)","복종(soumission)","사랑(amour)","독재(dictature)"],
  "story":`<div>
    <p>&lt;계엄령&gt;은 총 3부로 구성되어 있다.</p>
    <h2>1부</h2>
    <p>요새화된 스페인 마을 카디스에 혜성 하나가 나타난다. 몇몇 사람들은 카디스에 저주가 내리는 것이라며 불안해한다. 스페인어로 무無를 뜻하는 이름의 ‘나다’는 냉소적인 태도로 사람들을 비웃는다. 이내 페스트가 도시에 등장하지만 총독은 단순한 골칫거리로 치부한다. 결혼하기로 되어 있는 빅토리아와 디에고는 두려움에 빠진다. 한편 한 남자(페스트)가 총독 궁에 나타나 총독의 자리를 넘겨받기를 원한다. 남자는 민중들의 말살을 주도하고, 여비서는 남자의 명령을 받아 수첩에 적힌 사람들의 이름에 표시를 한다. 표시가 하나면 요주의 인물이라는 뜻이고, 둘이면 페스트에 감염되었다는 뜻이며 표시가 셋이면 말살이 결정되었다는 뜻이다. 그 남자를 두려워 한 총독은 냉큼 자신의 자리를 남자에게 넘기고는 도망친다. 페스트가 도시를 지배한다.</p>
    <h2>2부</h2>
    <p>남자(페스트)는 무죄인 사람들을 유죄로 만들어야 한다고 이야기한다. 그의 명령을 받은 시장과 여비서는 ‘존재증명서’를 발급받지 못 하는 수상쩍은 사람들을 조사하기 시작한다. 많은 사람들이 수용소에 갇힌다. 자신의 존재 이유를 증명하지 못 하는 어부의 이름에 체크하고, 증명서가 없는 여자의 집을 빼앗아 관청 사무실로 만드는 등 그의 횡포는 날이 갈수록 심해진다. 페스트에 감염된 디에고가 자신의 연인인 빅토리아의 집으로 찾아오자 빅토리아의 아버지인 판사는 디에고를 내쫓으려 한다. 갈등을 중재하려던 판사의 부인, 즉 빅토리아의 어머니와 빅토리아는 도리어 판사와 사이가 나빠지게 된다. 디에고는 자신이 페스트에 걸렸다는 사실에 큰 두려움을 느끼고 혼란에 빠진다. 여비서가 아무런 감정 없이 사공을 죽이는 것을 눈앞에서 목격한 디에고는 분노에 차고 페스트에 맞서기로 결심한다. 디에고가 여비서의 뺨을 때리며 페스트에 대한 공포를 극복하고 반항하기로 결심하자, 디에고의 몸에 새겨져 있던 페스트의 표시가 점차 희미해지기 시작한다.</p>
    <h2>3부</h2>
    <p>디에고는 페스트에 반항하는 것만이 그것을 퇴치할 수 있는 유일한 방법임을 깨닫고 사람들에게 두려움을 벗어나라고 이야기한다. 한 남자가 여비서의 수첩을 빼앗고 사람들은 고위층 사람들의 이름에 줄을 긋기 시작한다. 남자(페스트)는 당황해 하고 나다는 도망친다. 갑자기 빅토리아가 죽자, 분노한 디에고는 남자에게 빅토리아의 목숨과 자신의 것을 교환하자고 제안한다. 디에고를 죽이라는 남자(페스트)의 명령에 여비서는 긍지 있는 자들을 이길 수 없다며 강하게 반발한다. 남자(페스트)는 디에고에게 도시를 돌려주겠다고 한 후 돌아선다. 여비서는 남자(페스트) 못지않게 사랑도 고집스럽다고 말한다. 남자(페스트)가 사라지고 나다는 바다에 투신한다. 어부는 바다가 모든 사람들이 뭉쳐서 한 덩어리 되라 했다며 소리친다.</p>
  </div>`,
  "background":"<p>공간적 배경 - 스페인, 카디스</p>",
  "character_net":{
    "chart":"siege.jpg",
    "character":{
      "페스트 (=남자)":"페스트가 의인화되어 나타난 인물로서 막강하고 절대적인 폭력을 상징한다. 투지와 사랑으로 뭉친 민중들 앞에 무력해진다.",
      "여비서":"남자(페스트)의 명령에 따라 사람들을 죽이는 일을 한다. 그러나 이에 저항하는 디에고의 모습을 보며 그를 지지하게 된다.",
      "디에고":"판사의 딸인 빅토리아와 결혼할 예정이다. 사람들에 대한 연민과 정의로 가득 찬 청년으로, 공포 속에서도 용감하게 페스트에 맞서며 사랑하는 빅토리아를 구해내는 대신 자신을 희생한다. 디에고와 민중들의 저항의 힘에 페스트는 일시적으로 물러난다.",
      "빅토리아":"판사의 딸이자 디에고의 약혼녀이다.",
      "나다":"만물을 경멸하며 신을 부정한다. 유일하게 존재하는 것은 허무 뿐이라고 믿는다. 페스트의 재앙 가운데에서도 냉소적인 태도를 유지한다. ‘무엇도 믿지 않는’ 그의 특성은 페스트와 여비서로 하여금 그가 죽음을 돕기에 적합한 인물이라고 판단하게 만든다.",
      "판사":"부르주아를 대변하는 인물이다.",
      "판사부인":"부르주아를 대변하는 인물이다.",
      "시장":"남자와 여비서를 도와 사람들을 조사한다.",
      "총독부(전령)":"시민들의 눈과 귀를 속이는 동시에 뒤로는 자신들의 안위만을 챙긴다."
    }
  },
  "style":"<p>희곡의 특성 상 대사와 지문으로 구성되어있다.</p>",
  "network1":`<div>
    <ul>
      <li>&lt;페스트&gt; : &lt;계엄령&gt;은 장 루이 바로가 카뮈에게 자신의 초안을 토대로 페스트 신화에 관계된 다이얼로그를 써달라고 제안한 것을 시작으로 쓴 것으로, &lt;페스트&gt;의 주제를 극화시킨 작품이다. 그러나 작품의 일러두는 말에서 카뮈는 ‘누가 뭐라고 하든, &lt;계엄령&gt;은 결코 내 소설을 각색한 것이 아니라는 점을 분명히 해둘 필요가 있다’고 밝혔다.</li>
      <li>&lt;칼리굴라&gt; : 칼리굴라의 “요컨대 내가 페스트의 역할을 대신 하는 것이지.” 라는 대사가 등장한다. 이 대사는 &lt;계엄령&gt;에서 남자와 여비서가 페스트 역할을 하며 민중들을 죽이는 장면을 떠올리게 한다.</li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":"<p><a href='https://ko.wikipedia.org/wiki/%EC%9E%A5%EB%A3%A8%EC%9D%B4_%EB%B0%94%EB%A1%9C' target='_blank'>장 루이 바로(Jean-Louis Barrault)</a> : &lt;계엄령&gt;은 1948년, 장 루이 바로가 카뮈에게 자신의 초안을 토대로 페스트 신화에 관계된 다이얼로그를 써달라고 제안한 것을 시작으로 합작하여 극본을 쓴 후 상연했다.</p>",
  "social_network":`<div>
    <ul>
      <li>김미경, “Camus의 희곡 작품에 나타난 부조리의 전개”, 이화여대 석사학위논문, 1974</li>
      <li>Reinach, Théodore. De l'état de siège: étude historique et juridique. F. Pichon, 1885.</li>
    </ul>
  </div>`,
  "digital_works":`<div>
    <ul>
      <li>초판본 이미지<br/>
        <img src='http://pictures.abebooks.com/EDCONROY/7344783749.jpg' width='150'/></li>
      <li><a href='https://fr.wikipedia.org/wiki/L%27%C3%89tat_de_si%C3%A8ge' target='_blank'>위키피디아 프랑스어판 “계엄령”</a></li>
      <li><a href='https://www.youtube.com/watch?v=fQqhu1wxtS8' target='_blank'>유튜브 ETAT DE SIEGE (Pourquoi Camus? 인터뷰)</a></li>
      <li><a href='https://www.youtube.com/watch?v=tPvYyzl4nuE&list=UU1cg8OEiWEydFuGK1Rwq6gw#action=share' target='_blank'>유튜브 Etat de Siège -Théâtre de Poche-</a></li>
    </ul>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"fiction",
  "category":"희곡",
  "author":"알베르 카뮈 Albert Camus",
  "title":"정의의 사람들　Les Justes",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1949,
  "language":"프랑스어",
  "t_title":"정의의 사람들",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["러시아(russie)","정의(justice)","혁명(révolution)","삶(vie)","사랑(amour)"],
  "keywords2":["테러리즘(terrorisme)","혁명(révolte)","사회참여(engagement)","삶(vie)","사랑(amour)"],
  "story":`<div>
    <h2>1막</h2>
    <p>러시아의 한 아파트 안.</p><p>혁명가인 칼리아예프, 아넨코프, 스테판, 도라 그리고 부아노프가 한 아파트 내에서 세르게이 대공을 공격하기 위한 계획을 도모한다. 스테판은 시인인 칼리아예프를 믿지 못하겠다며 자신이 폭탄을 던지고 싶다고 하지만 칼리아예프는 자신이 던지겠다고 얘기하며, 둘 사이에서 견해 차이가 발생한다.</p>
    <h2>2막</h2>
    <p>이튿날, 같은 장소.</p><p>그룹의 수장인 아넨코프와 도라는 부아노프와 스테판 그리고 칼리아예프를 점차 자신들의 혁명일원들로 받아들인다. 세르게이 대공에 대한 다음 날의 공격을 준비하며, 다섯 명의 테러리스트들이 느끼는 긴장감이 점점 고조된다. 칼리아예프가 첫 폭탄을 던지고, 부아노프가 두 번째 폭탄을 던지기로 한다. 그러나 마차에 대공의 어린 조카들이 타고 있는 것을 발견한 칼리아예프는 차마 폭탄을 던지지 못한다.</p>
    <h2>3막</h2>
    <p>이틀 후, 다시 한 번 암살시도가 이루어진다. 작전에 성공하지만 칼리아예프는 즉시 체포되어 수감된다.</p>
    <h2>4막<h2/>
    <p>감옥.</p><p>칼리아예프는 수감되고 사형을 선고받는다. 경찰의 수장인 스쿠라토프는 그에게 동료들을 밀고할 것을 강요하며, 만일 그렇지 않을 경우 공공연한 배신자로 낙인찍겠다고 하지만 그는 꿋꿋하게 스쿠라토프의 제안을 거절한다. 독실한 기독교 신자인 대공비는 칼리아예프의 영혼을 구원하기 위해 그의 사면을 요청하겠다며 감옥으로 찾아오지만, 칼리아예프는 이 또한 거부한다. 칼리아예프는 자신이 빼앗은 생명의 대가로 자신의 목숨을 내놓겠다며 테러 행위를 스스로 정당화한다.</p>
    <h2>5막</h2>
    <p>칼리아예프는 동지들을 밀고하라는 총감의 협박을 꿋꿋하게 견디고 결국은 형장으로 나간다. 그의 연인인 도라는 죽음으로 그와 하나가 되기를 원하며 다음 번 폭탄을 던지기로 결심한다.</p>
  </div>`,
  "background":"<p>러시아, 모스크바.</p>",
  "character_net":{
    "chart":"Les_Justes.png",
    "character":{
      "도라":"사랑과 정의, 그리고 연민으로 가득 찬 인물로, 칼리아예프를 사랑한다.",
      "칼리아예프":"사랑과 인생을 사랑하는 젊은 시인으로서, 행복과 사랑을 애호하며 스스로의 행동에 당위성을 부여하고자 킬러를 자처한다. 인간이 혁명보다 우선시 되어야 하느냐를 계속해서 고민한다.",
      "스테판":"극단주의적 인물. 칼리아예프와 대조적으로 혁명에는 한계가 없다고 믿는다.",
      "아넨코프":"그룹의 지도자로서 인정 많은 인물이다.",
      "부아노프":"혁명에 있어서는 대단히 열정적이나 스스로에 대한 확신이 부족하다."
    }
  },
  "style":"<p>희곡의 특성 상 대사와 지문으로 구성됨.</p>",
  "network1":"-",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>폴　외틀리 : 1949.12.15. 에베르토 극장(Théâtre Hébertot)에서 ‘정의의 사람들’ 연출</p><p>세르주 레지아니 : 1949.12.15. 에베르토 극장에서 ‘정의의 사람들’ 칼리아예프 역</p>",
  "social_network":`<div>
      <section>
        <h2>서평</h2>
        <p><a href='http://www.catholicnews.co.kr/news/articleView.html?idxno=14621' target='_blank'>가톨릭 뉴스 (서평) 정의의 사람들</a></p>
      <section>
        <h2>논문</h2>
        <ul>
          <li>장영, “Une etude sur la justice et l'humanite chez Camus : 작품 Les justes를 중심으로”, 숙명여자대학교 대학원, 국내석사, 1985.</li>
          <li>손보현, “알베르 까뮈의 「정의의 사람들」에 나타난 반항의 의미 = (Le) sens de la revolte dans Les Justes d'Albert CAMUS”, 아주대학교 대학원, 국내석사, 2000</li>
          <li>유기환, “정의의 배반성 : 정의의 사람들 Les Justes = Double Face de la Justice” 상명대학교 논문집, Vol.17, No.-, 1986</li>
          <li>박동혁, “연극 연출의 사회적 조건에 대한 연구 : Albert Camus의 Les justes를 중심으로”, 성균관대학교 대학원, 국내석사, 1982</li>
        </ul>
      </section>
      <section>
        <h2>연극</h2>
        <ul>
          <li><a href='http://ticket.interpark.com/Ticket/Goods/GoodsInfo.asp?GoodsCode=11015523' target='_blank'>링키지 프로젝트 2011 - ‘정의의 사람들’ (2011/12/08 ~ 2011/12/10)</a></li>
          <li><a href='http://www.playdb.co.kr/playdb/PlaydbDetail.asp?sReqPlayNo=57287' target='_blank'>연극 ‘정의의 사람들’ (2013/12/25 ~ 2013/12/29)</a></li>
          <li><a href='http://www.playdb.co.kr/playdb/PlaydbDetail.asp?sReqPlayNo=84984' target='_blank'>연극 ‘정의의 사람들’ (2014/04/07 ~ 2014/04/07)</a></li>
          <li><a href='http://ticket.interpark.com/Ticket/Goods/GoodsInfo.asp?GoodsCode=15011111' target='_blank'>연극 〈Les Justes - 정의의 사람들〉 (2015.10.14 ~ 2015.10.22)</a></li>
        </ul>
    </div>`,
  "digital_works":`<div>
    <p><a href='https://fr.wikipedia.org/wiki/La_Mer_et_les_Prisons#Du_bon_usage_de_la_Peste' target='_blank'>알베르 카뮈, ‘페스트’ (Du bon usage de la Peste)</a></p>
    <h2>유튜브 영상</h2>
    <ul>
      <li><a href='https://www.youtube.com/watch?v=sIe2Hs71mwc' target='_blank'>Les Justes' d'Albert Camus - Club Théâtre Sciences Po Aix 2013</a></li>
      <li><a href='https://www.youtube.com/watch?v=tfY94jPhpiw' target='_blank'>Les Justes performed by Harvard's La Troupe at Cornell University</a></li>
      <li><a href='https://www.youtube.com/watch?v=Ho4gNlZUzWM' target='_blank'>Les Justes d'Albert Camus, présentée par la Troupe de l'Echo</a></li>
    </ul>
    <p><a href='https://fr.wikipedia.org/wiki/Les_Justes'>프랑스어 위키페디아 ‘정의의 사람들 Les Justes’ 페이지</a></p>
    <p>초판본 이미지<br/>
      <img src='http://www.devoir-de-philosophie.com/images_dissertations/30234.jpg' width='150'/></p>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
    /**  비스토리형 작품 **/
    /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"젊은 시절의 글 Écrits de jeunesse",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1973,
  "language":"프랑스어",
  "t_title":"젊은 시절의 글",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2008,
  "keywords1":["몽상(rêverie)","예술(art)","행복(bonheur)","삶(vie)","사상(pensée)"],
  "keywords2":["모음집(recueil)","시(poésie)","젊음·청춘시절(jeunesse)","예술(art)","생각·사고(idée)"],
  "story":`<div>
    <p>카뮈의 초기 미발표 원고(1931년~1934년)를 모아놓은 『젊은 시절의 글 Écrits de jeunesse』에는 젊은 시절의 카뮈가 공책에 남겨놓은 열아홉 편의 짧은 글이 수록되어 있다. 음악과 예술을 비롯해 다양한 주제를 다루고 있으며, 문학과 철학에 대한 그 당시 카뮈의 생각을 담고 있다.</p>
    <ul>
      <li>1931년 : 「시」, 「어느 사산아의 마지막 날」.</li>
      <li>1932년 : 「새로운 베를렌」, 「제앙 릭튀스」, 「세기의 철학」, 「음악에 대한 시론(試論)」, 「직관들」.</li>
      <li>1933년 : 「독서 노트」, 「무어인의 집」, 「용기」, 「지중해」, 「죽은 여자 앞에서」, 「사랑하는 존재의 상실」, 「신과 그의 영혼의 대화」, 「모순들」, 「가난한 동네의 병원」, 「합일 속의 예술」.</li>
      <li>1934년 : 「멜뤼진의 책」, 「가난한 동네의 목소리들」.</li>
    </ul>
    <p>「시」는 지평선, 밤, 누이의 이미지를 중심으로 슬픈 내면을 표현하고 있다. 카뮈가 고등학교 친구들과 1931년 만든 문학과 예술 월간지 '쉬드 Sud' 창간호(1931년 12월)에 실린 작품이다. 비알라네(Paul Viallaneix) 편집의 '젊은 시절의 글'에는 실려 있지 않고 레비-발렌시(Lévi-Valensi)가 편집한 갈리마르 출판사의 플레이아드판 카뮈 전집에는 실려 있다. 「어느 사산아의 마지막 날」은 카뮈가 발표한 첫 산문이다. '쉬드 Sud'지 창간호에 실린 글이다. 비알라네(Paul Viallaneix) 편집의 '젊은 시절의 글'에는 실려 있지 않고 레비-발렌시(Lévi-Valensi)가 편집한 갈리마르 출판사의 플레이아드판 카뮈 전집에는 실려 있다. 「새로운 베를렌」은 카뮈가 처음으로 자신의 이름을 서명한 텍스트로 내적 갈등을 중점적으로 다루고 있다(“자신의 영혼으로는 신에게 기도했고 머리로는 죄를 저지른 인간말이다.”). '쉬드 Sud'지 4호(1932년 3월)에 실린 글이다. 「제앙 릭튀스」에서 카뮈는 가난한 자가 자신의 비참함과 슬픔을 이야기하는 제앙 릭튀스의 저서 『가난뱅이의 혼잣말』을 분석한다. '쉬드 Sud'지 6호(1932년 5월)에 실린 글이다. 「세기의 철학」에서 카뮈는 기대하던 베르그송의 저서인 『모럴과 종교의 두 가지 원천』에 실망했다고 평한다. '쉬드 Sud'지 7호(1932년 6월)에 실린 글이다. 또, 「음악에 대한 시론(試論)」에서는 음악에 대한 쇼펜하우어와 니체의 철학을 나누어 소개하고, 카뮈 자신의 생각을 드러낸다. '쉬드 Sud'지 7호(1932년 6월)에 실린 글이다. 「직관들」은 앙드레 지드의 『사랑의 시도, 혹은 허무한 욕망론』에서 따온 행복에 관한 글귀를 앞머리에 붙이고, 전체적으로는 몽상의 결과로 나온 여러 글들을 엮은 작품이다. 1932년 10월로 표시되고 서명되어 있지만 각 글의 순서는 연구자에 따라 조금씩 다르다. 「독서 노트」는 이후 카뮈가 쓰는 『작가수첩』과 비슷한 형태를 보인다. 1933년 4월 로 표시되고 서명 되어 있다. 「용기」는 일부분만 남은 짧은 원고로 병든 노인과 관련된 글이며, 『안과 겉』에 실린 텍스트와 유사하다. 이 원고는 ｢무어인의 집｣의 공책 마지막 4페이지를 차지하고 있다. 「지중해」 는 원색과 역동적인 풍경, 라틴적 정서와 뜨거운 태양을 중심 이미지로 담고 있는 시 작품이다. 1933년 10월로 표시되어 있다. 「죽은 여자 앞에서」와 「사랑하는 존재의 상실」은 말하고자 하는 바가 다른 두 가지의 글이지만 죽음이라는 문제를 공통적으로 다룬다. 두 원고 모두 제목이 없는데 전자는 “보라! 그 여자는 죽었다”라는 첫 문장에서 힌트를 얻어 비알라네가 ‘죽은 여자 앞에서’로 제목을 붙였다. 후자는 1933년 10월로 표시되어 있고 첫 문장을 따서 ‘사랑하는 존재의 상실’이라는 제목을 붙었다. 「신과 그의 영혼의 대화」는 제목 그대로 신과 그의 영혼의 대화를 담고 있는데 주로 신이 자신의 고독과 권태에 대해 푸념하고 있다. 1933년에 작성된 것으로 추정된다. 「모순들」은 삶에 대한 생각을 전개하는 아주 짧은 글인데 반항, 솔직함, 삶 등의 핵심어가 '이방인'을 상기시킨다. 「가난한 동네의 병원」은 폐결핵에 대해 이야기 하는 카뮈의 자전적 글이다. 1933년으로 표시되어 있다.  「합일 속의 예술」은 카뮈의 예술관을 드러낸 글이다. 그는 예술이 삶을 초월한 곳에 있으며 삶의 ‘정지’ 속에, ‘합일’ 속에 존재한다고 쓰고 있다. 1933년 4월 이전에 쓴 원고로 추정된다. 「멜뤼진의 책」은 고양이와 요정 이야기를 다룬 꽁트이자 그 꽁트를 쓰는 과정도 포함하고 있다. 시몬 이에는 이 글을 1934년 12월에 선물로 받았다고 밝혔다. 카뮈가 크리스마스에 아내인 시몬 이에 Simone Hié 에게 헌정한 글인 「가난한 동네의 목소리들」은 아픈 어머니를 이야기하는 글로 안정감이 돋보인다. 1934년 12월 25일로 표시되어 있다.</p>
  </div>`,
  "network1":`<div>
    <p>『안과 겉 L'Envers et l'Endroit』 – 「용기(일부분만 남은 원고)」와 유사한 부분이 등장한다. “그들은 다섯 식구였다. 할머니, 그녀의 작은아들과 큰딸, 그리고 큰 딸의 두 아이. 아들은 거의 벙어리에 가까웠다.” “그들은 다섯 식구였다 – 할머니, 작은아들, 맏딸, 그리고 맏딸의 두 아이. 아들은 거의 벙어리였다.(『안과 겉』의 ‘아이러니’)”</p>
    <p>『작가수첩 Carnets』 – 카뮈가 1935년부터 쓰기 시작하는 『작가수첩』은 1933년에 쓴 「독서 노트」와 유사한 형태를 보인다.</p>
  </div>`,
  "network2":"<p>제앙 릭튀스 Jehan Rictus, 『가난뱅이의 혼잣말 Les Soliloques du pauvre』 -  「제앙 릭튀스-가난의 시인」은 글 전체에서 제앙 릭튀스의 저서 『가난뱅이의 혼잣말』을 다루고 있다.</p><p>앙드레 지드 Andre Gide, 『사랑의 시도, 혹은 허무한 욕망론 La tentative amoureuse, ou le traité du vain désir』 – 「직관들」의 머리말로 다음 글귀를 인용했다. “나는 달리 아무것도 더 바랄 것이 없다는 듯, 오직 행복하기만을 바랐다.”</p>앙리 베그르송 Henri Bergson, 『모럴과 종교의 두 가지 원천 Les Deux sources de la morale et de la religion』 - 「세기의 철학」에서 카뮈는 베그르송의 신간인 『모럴과 종교의 두 가지 원천』에 대한 자신의 생각을 이야기 하고 있다.</p>",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    <ul>
      <li>김병익, 「카뮈 읽기.8(젊은 시절의 글, 시사평론)」, 『본질과현상』 통권33호 (2013년 가을), pp.213-232.</li>
      <li>가니 메라드(Ghani Merad), «폴 비알라네의 『초기 알베르 카뮈와 그의 젊은 시절 글』(Paul Viallaneix: Le premier Camus suivi de Ecrits de jeunesse d‘Albert Camus)», Revue Romane, Bind 10 (1975), pp.435-437.</li>
      <li>폴 비알라네(Paul Viallaneix),  『알베르 카뮈 노트 2: 초기 알베르 카뮈와 그의 젊은 시절 글(Cahiers Albert Camus n° 2: Le Premier Camus suivi de Ecrits de jeunesse d'Albert Camus)』 (1973), Gallimard</li>
      <li>김웅권, 「2장. 최초의 인간」, 『무어의 집』 (1992), 대성</li>
    </ul>
    </div>`,
  "digital_works":`<div>
    <ul>
      <li>알베르 카뮈에 대한 위키(<a href='https://ko.wikipedia.org/wiki/%EC%95%8C%EB%B2%A0%EB%A5%B4_%EC%B9%B4%EB%AE%88' target='_blank'>한국</a>/<a href='https://fr.wikipedia.org/wiki/Albert_Camus' target='_blank'>프랑스</a>)항목</li>
      <li><a href='http://lanouvellecompagnie.org/lnc/images/stories/flexicontent/images_documents/les soliloques du pauvre - jehan rictus.pdf' target='_blank'>제앙 릭튀스, 『가난뱅이의 혼잣말』의 원문 자료 (텍스트)</a></li>
      <li><a href='http://img.kb.dk/tidsskriftdk/pdf/rro/rro_0010-PDF/rro_0010_94965.pdf' target='_blank'>사회연결망 자료(Ghani Merad 저술)</a></li>
    </ul>
  </div>`,
  "legends":"<p>폴 비알라네 Paul Viallaneix가 편집하여 1973년 출간됨.</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"안과 겉 L'Envers et l'Endroit",
  "location":"알제리 알제 Algérie Alger",
  "publisher":"샤를로 Charlot",
  "published_year":1937,
  "language":"프랑스어",
  "t_title":"안과 겉",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":1988,
  "keywords1":["죽음(mort)","삶(vie)","사랑(amour)","인간(homme)","아이러니(ironie)"],
  "keywords2":["에세이(essai)","모음집(recueil)","풍경(paysage)","철학·사상(philosophie)","부조리(absurde)"],
  "story":"<p>장 그르니에 Jean Grenier 에게 헌사한 글이자 카뮈의 작품 중 최초로 출간된 『안과 겉』에는 카뮈가 젊은 시절(1935~1936년, 22세)에 쓴 에세이들이 수록되어 있다.</p><p>「서문」, 「아이러니」, 「긍정과 부정의 사이」, 「영혼 속의 죽음」, 「삶에의 사랑」, 「안과 겉」. 짧은 글들로 이루어진 『안과 겉』에서는 빛과 어둠처럼 삶에서 발견할 수 있는 ‘안과 겉’을 다루고자 한 카뮈의 글을 접할 수 있다.</p><p>카뮈는 초판이 절판된 지 20년도 더 지나서야 『안과 겉』에 실린 글들이 서투르다고 재판에 덧붙인 「서문」에 밝혔다. 가족들이 영화관으로 가버리고 홀로 남겨진 노인, 아무도 듣지 않지만 계속 이야기를 늘어놓는 노인, 아무도 슬퍼하지 않는 죽음을 맞이한 노인. 이처럼 서로 다른 세 죽음을 이야기 하는 「아이러니」. 전쟁에서 전사한 아버지와 아픈 어머니에 대한 언급이 있는 「긍정과 부정의 사이」는 카뮈의 자전적인 이야기를 담고 있다. 프라하의 한 호텔에서 숙박하는 동안 경험하는 누군가의 죽음을 고찰하는 내용의 「영혼 속의 죽음」. 「삶에의 사랑」에서는 삶 속에서 사랑을 발견하는 순간에 대한 자세한 묘사가 돋보인다. 「안과 겉」은 죽음에 대비하며 자신의 무덤을 준비하는 여자의 이야기를 통해 삶과 죽음에 대해 다루고 있다.</p>",
  "network1":"<p>『젊은 시절의 글』 – 『젊은 시절의 글』의 「용기」와 유사한 부분이 『안과 겉』의 「아이러니」에 등장한다.</p><p>“그들은 다섯 식구였다 – 할머니, 작은아들, 맏딸, 그리고 맏딸의 두 아이. 아들은 거의 벙어리였다.(『안과 겉』의 「아이러니」)”</p><p>“그들은 다섯 식구였다. 할머니, 그녀의 작은아들과 큰딸, 그리고 큰 딸의 두 아이. 아들은 거의 벙어리에 가까웠다.(『젊은 시절의 글』의 「용기」)” </p>",
  "network2":"-",
  "network3":"<p>장 그르니에 Jean Grenier : 소설가이자 철학자로 카뮈에게 많은 영향을 끼쳤으며, 카뮈가 이 책을 헌정한 스승이다.</p>",
  "character_network":"",
  "social_network":`<div>
    <ul>
      <li>이소연, 「알베르 카뮈의 『안과 겉』에서 나타난 지중해적 요소 연구」 (2008), 연세대학교.</li>
      <li>이경해, 「Camus의, Noces, L'Envers et l'Endroit, L'Etranger에 나타난 태양의 양면성과 삶과 죽음의 관계」, 『한국프랑스학논집』 제47집(2004) pp.221-234.</li>
      <li>태선영, 「『안과 겉』에 나타난 CAMUS의 평형의식(EQUILIBRE)」, 『인문과학연구』 2 (1995) pp.187-211.</li>
      <li>이해방, 「인문과학편 : 카뮈의 &lt; 표리 &gt; 에 나타난 「유년시절」연구」, 『상명대학교 논문집』 17권 (1986) pp.361-382.</li>
      <li>김동현, 「A. Camus의 "표리"(L' Envers et L'Endroit)에 관한 연구 : A Camus의 에쎄연구,1」, 『아주대학교 논문집』 4 (1982) pp.5-16.</li>
      <li>정태철, 「Albert Camus의 《L'Envers et l'Endroit》 考」, 『한국프랑스학논집』 제2집(1975) pp.21-39.</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p>위키(<a href='https://ko.wikipedia.org/wiki/%EC%95%8C%EB%B2%A0%EB%A5%B4_%EC%B9%B4%EB%AE%88' target='_blank'>한국</a>/<a href='https://fr.wikipedia.org/wiki/Albert_Camus' target='_blank'>프랑스</a>/<a href='https://fr.wikipedia.org/wiki/L%27Envers_et_l%27Endroit' target='_blank'>L'Envers et l'Endroit</a>)</p>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/envers_et_endroit/Camus_envers_et_endroit.pdf' target='_blank'>불어 원문 텍스트 자료</a></p>
  </div>`,
  "legends":"<p>『안과 겉』의 초기 번역 제목은 『표리(表裏)』였다.</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"결혼 Noces",
  "location":"알제리 알제 Algérie Alger",
  "publisher":"샤를로 Charlot",
  "published_year":1939,
  "language":"프랑스어",
  "t_title":"결혼",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":1987,
  "keywords1":["자연(nature)","삶(vie)","죽음(mort)","사랑(amour)","행복(bonheur)"],
  "keywords2":["에세이(essai)","모음집(recueil)","알제리(algérie)","사색(méditation)","실존주의(existentialisme)"],
  "story":"<p>카뮈의 초기작품인 『결혼 Noces』은 지중해의 아름다운 풍경과 자연 속에서 느끼는 감정에 대한 자세한 묘사를 담고 있다.</p><br/><p>「티파사에서의 결혼 Noces à Tipasa」에서 카뮈는 화려한 자연의 모습에 전율을 느끼고 감동을 표현한다. “태양 속에서, 압생트의 향기 속에서, 은빛으로 철갑을 두른 바다며, 야생의 푸른 하늘, 꽃으로 뒤덮인 폐허, 돌더미 속에 굵은 거품을 일으키며 끓는 빛 속에서 신들은 말한다.” </p><p>반면, 「제밀라의 바람 Le vent à Djémila」에서는 죽음에 대한 각성을 보여준다. “죽음에 대한 나의 모든 공포는 삶에 대한 질투에서 온다는 것을 알아차리게 된다.” </p><p>「알제의 여름 L'été à Alger」에서 작가는 관능에 대한 감각을 이야기 한다. “이제 바야흐로 그 냄새는 인간과 대지의 결혼을 축성하고, 이 세상에서 유일하고 참으로 씩씩한 사랑을, 끝내 썩어 없어질 그러나 너그러운 사랑을 일깨워준다.”</p><p>「사막 Le désert」에서는 이탈리아의 풍경을 통해 얻은 교훈과 인간의 운명에 대해 말한다. “더 이상 나아가지 말고 걸음을 멈추어야 할 곳은 다름 아닌 바로 이 균형의 위이다.”</p><br/><p>총 네 편의 글로 이루어진 『결혼』에서 카뮈는 인간이 아닌 자연에 몰입해 그 일부가 되어 자연을 감상하면서 그것의 영원함과 위력을 느끼지만 자연은 카뮈에게 유한한 인간 존재의 조건을 환기시키기도 한다.</p>",
  "network1":"<p>여름 L'été : 『결혼』을 떠올리게 하는 섬세한 묘사로 이루어진 짧은 8편의 글로 이루어졌다. 『결혼』과 『여름』의 공간적 배경이 겹치기도 해 자연스럽게 연결된다.</p>",
  "network2":"-",
  "network3":"<p>피에르외젠　클레렝　Pierre-Eugène Clairin의 채색목판화로 1952년 &lt;썽팜므아미데리브르(Cent femmes amies des livres)&gt; 출판사에서 &lt;결혼&gt;이 출간됨.</p>",
  "character_network":"",
  "social_network":`<div>
    <ul>
      <li>이찬규, 「알베르 카뮈의 결혼에 나타난 소리풍경」, 『프랑스어문교육』 제50집 2015,  pp.175-195.</li>
      <li>이해방, 「카뮈의 〈결혼〉에 나타난 침묵의 양상과 의미」, 한국외국어대학교, 1985.</li>
      <li>김현자, 「「결혼 Noces｣에 나타난 인간과 세계의 일치 - 카뮈의 근원적 긍정의 세계」, 『불어불문학연구』16권, 1981, pp.109-123.</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p><a href='https://fr.wikipedia.org/wiki/Albert_Camus' target='_blank'>‘알베르 카뮈’ 위키</a></p>
    <p><a href='https://fr.wikipedia.org/wiki/Noces_(Camus)' target='_blank'>‘결혼’ 위키</a></p>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/noces/camus_noces.rtf' target='_blank'>원문(불어) 텍스트 파일</a></p>
  </div>`,
  "legends":"<p>완역 초판 1987</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"시지프 신화 Le mythe de Sisyphe",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"시지프 신화",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["비평(Eassais)","부조리(absurde)","시지프의 신화(Le mythe de Sisyphe)"],
  "keywords2":["카뮈(Camus)","시지프의 신화(Le mythe de Sisyphe)","부조리(absurde)"],
  "story":"<p> 카뮈의 철학적 에세이이다. “참으로 진지한 철학적 문제는 오직 하나뿐이다. 그것은 바로 자살이다.”(p.267)라는 문장으로 시작하는 이 에세이는 “과연 죽음에 이를 정도의 논리란 존재하는 것일까”(p.274)를 추적하는 흐름으로 전개된다. 카뮈는 이를 “부조리의 추론”(p.274)이라고 부른다. 카뮈는 본 에세이에서 부조리를 나열하며 설명하고 있는데 부조리란 “시간 속에 속하여 전존재를 다하여 거부했어야 하는 내일을 바라고 있는 공포. 이러한 육체의 반항”(p.280)과 “세계의 두꺼움과 낯섦”(p.281)이라 말한다. 또한 카뮈는 부조리의 성질에 관해 “부조리는 인간 안에 있는 것도 아니고, 세계 안에 있는 것도 아니고 오직 양자가 함께 있는 가운데 있을 뿐”(p.299)라 말하며 인간이 원하는 바와 세계가 인간에게 제공하는 양자 사이에서의 간격이 부조리라 말한다. 이러한 부조리의 추론을 전개해가며 카뮈는 인간은 부조리에서 도피할 수 없으며 그 상태에서 어떻게 살아가야 하는지에 글을 전개해가고 있다.</p>",
  "network1":"<p>『이방인』 : 카뮈의 대표적 소설로 부조리 사상이 담겨있는 문학 작품이다.</p><p>「“아닙니다, 나는 실존주의자가 아닙니다…”」 : 《레 누벨 리테레르Les Nouvelles littéraires》, 1945.11.15. 인터뷰이다. 카뮈는 여기에서 『시지프 신화』에서 밝힌 바 있는 자신의 부조리 사상에 대해 설명한다.</p>",
  "network2":"<p>'시지프 신화' : 카뮈가 자신의 부조리 철학을 설명하기 위해 인용하는 그리스 신화</p><p>'모비 딕' : 미국의 소설가 허먼 멜빌의 작품으로 카뮈는 '시지프 신화󰡕에서 이 작품을 언급하며 부조리의 사상이 담긴 작품이라 말하고 있다.</p><p>'악령' : 카뮈는 '시지프 신화' 중 ‘키릴로프’라는 장에서 이 작품(키릴로프는 이 작품 속 인물임)과 이 작품의 저자 도스토예프스키를 소재로 하여 자살의 문제를 검토하고 있다.</p><p>'심판', '성': 카뮈는 '시지프 신화' 초판에서 앞서 언급한 도스토예프스키에 관한 장(chapter) 대신 카프카에 대한 글을 실었다. 이 글은 카뮈 전집에서는 '시지프 신화'의 부록으로 수록되어 있다. ‘키릴로프’ 장에서처럼 이 글에서도 카뮈는 부조리한 창조에 대한 자신의 생각을 전개하고 있다.</p>",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    이기언, 「까뮈와 실존철학」, 『지중해지역연구』, 제7권 제1호(2005,4). : 철학자와 작가로서의 카뮈의 정체성에 관한 논문이다.
   </div>`,
  "digital_works":`<div>
    <p><a href='https://ko.wikipedia.org/wiki/%EC%8B%9C%EC%A7%80%ED%94%84_%EC%8B%A0%ED%99%94' target='_blank'>『시지프의 신화』에 대한 위키페디아 항목</a></p>
    <p><a href='http://terms.naver.com/entry.nhn?docId=2012359&cid=41773&categoryId=44397' target='_blank'>『시지프의 신화』에 대한 네이버 캐스트 설명</a></p>
  </div>`,
  "legends":"<p>초판 2006, 번역본 초판 2010</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"독일 친구에게 보내는 편지 Lettres à un ami allemand",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1945,
  "language":"프랑스어",
  "t_title":"독일 친구에게 보내는 편지",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2004,
  "keywords1":["나치(nazi)","유럽(europe)","전쟁(guerre)","조국(patrie)","편지(lettre)"],
  "keywords2":["편지교환(correspondance)","서간체 양식(genre épistolaire)","폭력(violence)","저항(résistance)","교전·투쟁(combat)"],
  "story":"<p>네 개의 편지로 이루어진 『독일 친구에게 보내는 편지』는 실존인물에게 보내는 편지가 아닌 상상의 편지라는 문학적 형식을 갖고 있다. 독일의 프랑스 점령 시기에 카뮈가 비밀리에 레지스탕스 신문을 위해 쓴 글이다.</p><p>이 글에서 카뮈는 프랑스로 국한된 것이 아닌 자유로운 유럽인들(nous autres, Européens libres)의 위대함을 옹호하고, 나치의 폭력에 저항하고자 하는 의지를 강하게 드러낸다. </p><br/><p>첫 번째 편지는 1943년 『르뷔 리브르 Revue Libre』에 발표되었고, 두 번째 편지는 1944년 『카이에 드 라 리베라시옹 Les Cahiers de la Libération』에 실렸다. 다른 두 편지는 『르뷔 리브르』에 싣기 위한 것이었지만 해방이 될 때까지 발표되지 않다가, 세 번째 편지는 1945년 『리베르테 Libertés』에 실렸다. 편지 전체의 출판이 이루어졌고 편지는 레지스탕스에 몸담고 있는 동안 알고 지낸 르네 레노 René Leynaud 에게 헌정되었다.</p>",
  "network1":"-",
  "network2":"<p>오베르만, 편지 90 Obermann, lettre 90 (에티엔느 피베르 드 세낭쿠르 Étienne Pivert de Sénancour) - “(인용) 인간은 필멸의 존재다. 그럴지 모른다./그러나 소멸하더라도 저항하면서 소멸하자./그리고 우리를 기다리고 있는 것이 비록 허무라 할지라도/그것이 사필귀정이 되도록 하지는 말자!” 로제 그르니에 Roger Grenier 에 의하면 이 인용구에는 카뮈가 부조리에서 솟아 나오는 반항의 정신이 절실하게 표현되어 있다.</p>",
  "network3":"-",
  "character_network":"<p>르네 레노 René Leynaud – 레지스탕스 활동 기간 중 알고 지낸 사이로 카뮈는 이 책을 󰡔프로그레 드 리옹  Le Progrès de Lyon󰡕의 기자인 르네 레노에게 헌정했다.</p>",
  "social_network":`<div>
    <ul>
      <li>진인혜, 물루드 마므리와 알베르 카뮈 그리고 알제리, 인문과학 , 100 권 (2014), pp.181-206</li>
      <li>John Foley, Albert Camus: from the Absurd to Revolt, Stocksfield, Acumen (2008), 239p. [알베르 카뮈: 부조리에서부터 반항까지]</li>
      <li>Patrick Godon, «Attitudes to war in the writings of Albert Camus 1939-1944», mémoire de maîtrise en histoire, Mc Gill University,  (1985). 114 f. [알베르 까뮈의 글쓰기에서의 전쟁을 대하는 자세 1939-1944]</li>
      <li>Annick Jauer, «Absurde et révolte dans les Lettres à un ami allemand,» dans Lionel Dubois [éd.], Albert Camus : La Révolte. Actes du 3eͤ Colloque international de Poitiers, 27-28-29 mai 1999 (2001), Poitiers, Les  Éditions du Pont-Neuf, pp. 205-218. [『독일 친구에게 보내는 편지』에서의 부조리와 반항]</li>
      <li>Thierry Ozwald, «Lettres à un ami allemand ou la juste mesure de Camus», dans Roman 20-50 : Revue d’Étude du Roman du XXe siècle, décembre (1999), pp. 77-92. [『독일 친구에게 보내는 편지』 또는 카뮈의 중용]</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p>위키(<a href='https://ko.wikipedia.org/wiki/%EC%95%8C%EB%B2%A0%EB%A5%B4_%EC%B9%B4%EB%AE%88' target='_blank'>한국</a>/<a href='https://fr.wikipedia.org/wiki/R%C3%A9flexions_sur_la_peine_capitale' target='_blank'>프랑스</a>)</p>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/lettres_ami_allemand/lettres_ami_allemand.rtf' target='_blank'>원문 자료(텍스트)</a></p>
  </div>`,
  "legends":"<p>개정판 2004</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"시사평론 Actuelles, chroniques 1944 ~ 1948",
  "location":"프랑스 France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1950,
  "language":"프랑스어",
  "t_title":"시사평론",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["저널리즘(journalisme)","역사(histoire)","사회조건(condition sociale)","기사(articles)","정치(politique)","도덕(morale)","자유(liberté)","투쟁(combat)"],
  "keywords2":["도덕(morale)","자유(liberté)","투쟁(combat)","에세이(essai)"],
  "story":`<div>
    <p>카뮈가 1944년부터 1948년까지 4년 동안 가담했던 공적 생활에 관하여 기사와 사설, 논설문 등을 작성한 것을 묶은 책이다.</p>
    <b>파리의 해방</b>
    <p>8월 말, 독일 나치에 의해 점령당했던 파리가 해방되었다. 알베르 카뮈는 레지스탕스 신문 『콩바Combat』지를 통해 파리 해방과 관련된 사설들을 썼다. 프랑스 사람들이 나치에 반대하여 생명과 자유를 바쳐 싸웠던 사람들의 뜻에 함께하던 초심을 버리지 말 것, 진정한 해방은 적이 프랑스를 떠나는 시점이 아니라 지금까지의 사회와 반대되는, 정의롭고 형제애 가득한 사회가 건설되는 시점임을 강조하고 있다.</p>
    <b>비판적 저널리즘</b>
    <p>카뮈가 언론의 직업윤리에 관하여 쓴 사설들과 기사들이다. 정치권력 앞에서 카뮈는 기자로서의 자유와 독립성을 옹호한다. 카뮈는 ‘대중 앞에 선 기자의 책임이란 무엇인가?’, ‘자유 언론을 위하여 희생한 모든 이들을 어떻게 하면 배반하지 않을 수 있는가?’, ‘언론의 실천을 통해 어떻게 정의와 자유의 이념을 전할 수 있는가?’ 등의 질문을 끊임없이 던진다. 또한 말의 가치, 어조와 언어의 적절함을 강조하는 동시에 비판적인 언론이 단순한 도덕주의로 끝나서는 안 된다는 것을 의식한다.</p>
    <b>모럴과 정치</b>
    <p>알베르 카뮈는 1944년 9월 4일자 사설에서 혁명이란 ‘정치를 모럴로 교체하는 것’이라고 정의했다. 그는 모럴과 정치를 교체하는 문제뿐 아니라 둘 사이의 불가분의 관계와 나아가서는 갈등의 문제까지 다루고 있다. 카뮈는 히로시마 원폭의 비극에 대한 비판의 사설을 쓰며, 고삐 풀린 사회적 리얼리즘이 통제 불능의 과학과 결합되었을 때의 대재난에 대해 이야기한다.</p>
    <b>육체</b>
    <p>구체적인 개인의 삶에 관하여 1944년 10월부터 1945년 5월까지 쓴 사설이다. 레지스탕스 활동 중 살해된 르네 레노René Leynaud의 모습을 환기하며, 아직 적의 손아귀 속에 있는 모든 부재자들을 잊지 않도록 권유한다. 초월적 존재를 지향하거나 지나치게 먼 미래나 내세의 약속으로 희망의 신기루를 보여주려는 기독교와 공산주의에 반(反)하여 지금 이 순간의 삶을 귀중하게 여기는 경향을 보이고 있다. 더불어 수용소에 남은 사람들의 석방 과정에서 목격되는 비참한 환경을 고발한다.</p>
    <b>비관주의와 압제</b>
    <p>공산당과 기독교에서 당시 지성인들의 비관주의적 경향을 비판하던 것에 대하여 반론을 편다. 사르트르를 포함한 실존주의자들과 카뮈 등의 지식인들이 보이는 비관적 성향은 성찰의 ‘출발점’일 뿐 ‘귀결’이 아니라는 논리를 처음으로 펴기 시작한다(이는 후에 『시지프 신화』에서 구체적으로 정리하게 된다.) 또한 지성이 전체주의와 본능의 철학을 막는 방파제임을 주장하며 지성인으로서의 역할을 이야기한다.</p>
    <b>2년 후</b>
    <p>전쟁이 끝난 지 2년 후, 초기의 열광적이고 혁명적인 희망은 이제 이 나라가 직면해야만 하는 정치경제적 현실에 자리를 내준 것을 배경으로 쓴 기사와 사설들이다. 시간 순서상 가장 앞에 놓여야 할 마지막 텍스트에서는 환멸감이 지배하고 있다. 카뮈는 증오를 증오하며 진실을 증언하는 사람들과 함께하자며 호소한다.</p>
    <b>피해자도 가해자도 아닌</b>
    <p>여기에서 말하는 ‘가해자bourreaux’는 사형집행인을 의미한다. 그런 맥락으로 카뮈는 국제적인 차원에서 사형을 폐지하는 것을 포함하여 의식적인 살인과 관련된 일체의 이데올로기에 반대한다. 더불어 대화로서의 의사소통이 중요함을 피력하고, 지난 정치적 틀에서 벗어나 새로운 범세계적 공동체를 형성할 것을 주장한다.</p>
    <b>에마뉘엘 다스티에 드 라 비주리Emmanuel d'Astier de La Vigerie에게 보내는 두 개의 답변</b>
    <p>다스티에는 카뮈에게 정치를 기피하고 모럴 뒤에 숨는다고 지적하며, 자본주의와 부르주아의 공모자가 되었다고 비판했다. 이 글은 그런 비판에 대한 답변이다. 카뮈는 ‘폭력’은 피할 수 없는 것이나 어떤 폭력이든 폭력을 정당화하는 것은 거부해야 한다고 주장한다. 지식인으로서 인간의 자유보다 개인적이고 내면적인 생활을 보호하는 쪽에만 신경을 쓴다고 카뮈를 비난하던 다스티에의 말에 부당하다고 반박한다.</p>
    <b>무신앙자들과 기독교인들<b>
    <p>1948년에 라투르-모부르Latour-Maubourg에서 한 발표의 단편들을 모은 것이다. 카뮈는 자신을 비관론자로 비난했던 기독교인들을 비판한다.</p>
    <b>세 개의 인터뷰</b>
    <p>세 가지 인터뷰 중 첫 번째 것만이 실제로 했던 인터뷰다. 두 번째 것은 대화 형태로 작성하고 카뮈의 서명을 붙여 『인간의 옹호 Défense de l’Homme』지 1949년 7월호에 썼던 텍스트이며, 세 번째 것은 카뮈가 미리 썼다가 발표를 포기한 긴 서문에서 발췌한 대목들을 구성해 놓은 것이다.</p>
    <b>왜 스페인인가? (가브리엘 마르셀에게 보내는 답변)</b>
    <p>철학자 가브리엘 마르셀이 『레 누벨 리테레르Les Nouvelles littéraires』지에 카뮈의 희극 『계엄령』 초연에 관하여 평을 남겼다. 카뮈는 그에 답변했다.</p>
    <b>자유의 증인</b>
    <p>예술가와 예술가가 속한 사회의 관계에 관한 성찰을 드러낸 글이다. 그는 예술가의 소명과 그가 속해 있는 공동체의 요청 사이에 발생한 모순으로 고민하는 가운데, 창조 행위는 예술가 자신이나 인간 자체에게는 무엇보다 자유의 행위로서 작용함을 강조한다. 또한 역사를 거부하는 동시에 역사에 참여해야 하는 예술가의 모순된 입장을 의식하고 있다.</p>
  </div>`,
  "network1":"<p>『페스트 (La Peste)』 : 『페스트』의 타루가 『시사평론』 410쪽 첫 단락의 마지막 문장과 똑같은 말로 그의 ‘수첩’의 기록을 끝마쳤다.</p><p>『시지프 신화 (Le Mythe de Sisyphe)』 : 『시사평론』의 &lt;비관주의와 압제&gt;에서 주장한 내용들을 구체적으로 정리된 작품이다.</p><p>『여름(L’Été)』 : 카뮈가 &lt;세 개의 인터뷰&gt;에서 비교한 기독교-그리스 정신이 『여름』의 &lt;헬레네의 추방&gt;에서 보다 폭넓게 발전되어 있다.</p><p>『반항하는 인간(L‘'homme révolté)』 : &lt;자유의 증인&gt;에 나타나는 생각의 흐름이 『반항하는 인간』 속 &lt;창조와 혁명 Création et révolution&gt;에서 더욱 뚜렷하게 등장하고 있다.</p><p>『단두대에 대한 성찰(Réflexions sur la guillotine)』 : &lt;피해자도 가해자도 아닌&gt;에서처럼 사형제도를 비판하고 있다.</p>",
  "network2":"-",
  "network3":"-",
  "character_network":`<div>
    <ul>
      <li>르네 샤르René Char : 카뮈와 깊은 우정을 나누어온 시인. 역자의 말에 따르면, ‘누구보다도 『시사평론』의 혼을 그대로 구현하고 있는’ 인물이다.</li>
      <li>프랑수아 모리악 François Mauriac : 『시사평론』 번역본 448쪽에 등장하는 인물로, ‘숙청’ 문제를 두고 카뮈와 논쟁을 벌인 작가이다.</li>
      <li>장 다니엘 Jean Daniel : 카뮈는 정치적 중립을 지키며 평화를 위해 노력하려는 정치 운동에 참가하며, 장 다니엘이 주도하는 잡지 『칼리방 Caliban』에 &lt;Ni victimes ni bourreaux 피해자도 가해자도 아닌&gt;의 텍스트 전체를 전재하도록 허락했다.</li>
      <li>에마뉘엘 다스티에 Emmanuel d'Astier : 전후 카뮈의 공산당 동반자들 중 한 사람이었다.</li>
      <li>가브리엘 마르셀 Gabriel Marcel : 프랑스의 평론가로서 『레 누벨 리테레르 Les Nouvelles littéraires』지 1948년 11월에 카뮈의 극 『계엄령』 초연에 대하여 평을 남겼고, 카뮈는 『시사평론』의 &lt;왜 스페인인가?&gt;를 통해 그에게 답변하였다. 희극 『계엄령』은 당시 매우 민감한 주제였던 ‘전체주의’의 문제를 다룬 극이다.</li>
    </ul>
  </div>`,
  "social_network":`<div>
    <ul>
      <li>WYRWA, TADEUSZ. “PRISE DE CONSCIENCE DE L'UNITÉ EUROPÉENNE DANS LA RÉSISTANCE POLONAISE ET FRANÇAISE (폴란드와 프랑스의 저항에서의 유럽연합의 인식”. Revue des études slaves 75.2, 2004,</li>
      <li>Debout, Simone. “Sartre Et Camus, Témoins De La Liberté (사르트르와 카뮈, 자유의 증인들”. MLN 112.4, 1997.</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p><a href='https://fr.wikipedia.org/wiki/Actuelles' target='_blank'>위키백과 (시사평론)</a></p>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/actuelles_I/Camus_actuelles_I.pdf' target='_blank'>『시사평론 Actuelles』 불어 원문</a></p>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"반항적 인간 L'homme révolté",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1951,
  "language":"프랑스어",
  "t_title":"반항적 인간",
  "t_author":"신구철",
  "t_publisher":"책세상",
  "t_year":1958,
  "keywords1":["부조리(absurde)","반항(révolte)","형이상학적 반항(révolte métaphysique)","혁명(révolution)","인간조건(condition humaine)"],
  "keywords2":["에세이(essai)","허무주의(nihilisme)","철학·사상(philosophie)","정치적인(politique)","반항(révolte)"],
  "story":`<div>
    <p>『반항적 인간』은 실존적 문제의식을 정리하여 발표한 철학적 에세이로, 반항이 구체적으로 인간들의 행동과 생각 속에서 어떻게 구현되었는가를 연구하고 성찰한 내용이 담겨있다. 여기서 찬양하는 반항은 ‘그 기원의 기억’을 간직하고 있는 반항, 그 본래의 순수성을 간직하고 있는 반항, 반항 그 자체를 신격화하지 않는 반항, 그 안에 어떤 공통된 가치를 긍정하는 반항이며, 카뮈는 이때 비로소 반항이 자유와 정의에 봉사할 수 있다고 보았다. 카뮈 본인의 철학적, 윤리적 성찰을 포함하여 반항의 역사 그리고 다양한 철학자․사상가의 사상 및 견해를 총 5개의 장으로 나누어 다루고 있다.</p>
    <b>머리말</b>
    <p>기존의 통념이나 관념에 대하여 이질감을 느끼게 한다는 점에서 부조리와 반항은 유사하다. ‘형이상학적인 것이든 역사적인 것이든 두 세기에 걸친 반항이 바로 우리의 성찰 대상이다(이하 알베르 카뮈 전집 5(2010), p.404).’ 반항이 이루어낸 결실로 우리가 부조리로부터 얻어낼 수 없었던 행동 규범, 적어도 살인할 권리와 의무에 대한 하나의 지침, 그리고 창조에의 희망이 있었음을 언급한다.</p>
    <p><b>반항적 인간(l'homme révolté)</b> : 카뮈가 생각하는 ‘반항’에 대한 중심적 견해가 나와 있다. 반항은 반감과 긍정을 내포하고 있으며, 역사적 현실이다. 현실로부터 도피할 수 없는 우리는 반항 속에서 가치를 찾아야 하고, 존재하기 위해서 우리가 반항을 해야 함을 주장한다. 데카르트의 명제를 이용한 ‘나는 반항한다. 그러므로 우리는 존재한다.(p.420).’로 반항에 대한 카뮈의 입장을 잘 드러내고 있다.</p>
    <p><b>형이상학적 반항(la révolte métaphysique)</b> : 인간의 조건과 창조에 대하여 이의를 제기하는 형이상학적 반항을 설명한다. 이어 자신에게 주어진 조건을 받아들이기를 거부하며 절대자에게 반항해 온 역사와 계보를 소개한다. 그리고 등장한 허무주의를 통해 카뮈는 반항의 사멸까지를 생각하며 ‘그리고 우리는 혼자다’라는 결론을 낸다.</p>
    <p><b>역사적 반항(la révolte historique)</b> : ‘반항이 단지 개인적 경험에서 사상을 향해가는 운동인 반면, 혁명은 사상을 역사적 경험 속에 편입시키는 일이다(p.542).’ 서구의 혁명운동과 테러리즘을 훑으며 카뮈는 덧붙여, 반항은 현재 있는 그대로의 우리 자신의 됨됨이를 창조하기 위해 스스로 살고, 다른 사람들도 살게 해야 한다고 말한다.</p>
    <p><b>반항과 예술(révolte et art)</b> : 예술은 현실을 찬양하며 부정하는 운동이다. ‘현실에 이의를 제기하면서 통일성을 부여하는 미의 규칙은 반항의 규칙이기도 하다(p.779).’ 카뮈는 복종을 거부하면서도 동시에 충실하며 현실적인 혁명의 길을 밝혀줄 수 있는 유일한 것이라고 이를 표현한다.</p>
    <p><b>정오의 사상(la pensée de midi)</b> : ‘반항은 그것이 바로 생의 운동이라는 것을, 살기를 포기하지 않고서는 반항을 부정할 수 없다는 사실을 입증한다(p.819).' 카뮈는 광란의 운동 속에서 쓰러지지 않고 꿋꿋이 서 있도록 우리를 지탱해주는 것이 바로 반항이라고 말한다.</p>
  </div>`,
  "network1":"<p>작가 수첩 2(Carnets II, 알베르 카뮈 Albert Camus, 1943–1951)</p><p>시지프 신화(Le Mythe de Sisyphe, 알베르 카뮈 Albert Camus, 1942),</p>",
  "network2":"<p>사회계약론(Du Contrat social, 장 자크 루소 Jean Jacques Rousseau, 1762)</p><p>카라마조프의 형제(Братья Карамазовы, 토스토옙스키 Dostoevsky, 1880),</p>",
  "network3":"-",
  "character_network":`<div>
    <p>작품 내에서 작가가 언급한 인물</p>
    <ul>
      <li>사드(Sade) - 미래와 신에 대한 절대적 부정</li>
      <li>도스토옙스키(Dostoevsky) - ‘카라마조프 형제들’에서 등장하는 신에 대한 반항.</li>
      <li>니체(Nietzsche) - 미래에 대한 절대적 긍정, 신에 대한 부정</li>
      <li>루소(Rousseau) - ‘사회계약론’ 권력의 정당성에 대한 탐구</li>
      <li>마르크스(Marx) - 투쟁은 허무주의적 권력의지로 연결 (마르크스의 생각과 다름)</li>
    </ul>
  </div>`,
  "social_network":`<div>
    <ul>
      <li>김화영 (2003), 알베르 카뮈 : 반항과 테러에 대한 성찰, 예술원보 통권 제47호 (2003. 12) pp.168-193</li>
      <li>김화영 (1989), 「반항적 인간」의 쟁점 ; 카뮈와 사르트르의 논쟁을 중심으로, 기독교사상, 33(2), pp. 99-113</li>
      <li>박흠련 (1971),  Camus에 있어서의 不條理와 反抗에 관한 몇 가지 考察, 이화여자대학교 대학원 [카뮈에 있어서의 부조리와 반항에 관한 몇 가지 고찰]</li>
      <li>Avi Sagi (2004), Albert Camus and the Philosophy of the Absurd, Review of Metaphysics Vol. 57 No. 4 pp.865-867 [알베르 카뮈와 부조리의 철학]</li>
      <li>David Sachs (1955), The Rebel by Albert Camus, The Philosophical Review Vol. 64 No. 1 pp.150-152, Duke University Press [알베르 카뮈의 반항적 인간]</li>
      <li>Richard H. Cox, (1965), Ideology, history and political philosophy: Camus' l'homme rèvoltè, Social research v.32 no.1, pp.71-97 [이념, 역사와 정치적 철학 : 카뮈의 반항적 인간]</li>
      <li>P. Horak (1989), Concerning One Metaphysical Critique of the French-Revolution-Camus, Albert and His L'Homme-Revolte, Filosoficky Casopis 37 (3) pp.430-438 [프랑스-혁명-카뮈의 형이상학적 비평과 관련하여, 알베르 그리고 그의 반항적 인간]</li>
      <li>Peter Kampits (1968), La mort et la révolte dans la pensée d'Albert Camus. Giornale di Metafisica 23 pp.19-28. [알베르의 사상 속에서의 죽음과 반항]</li>
      <li>Elvira Cassa Salvi (1952). 'L'homme Révolté' di Albert Camus. Humanitas 7 (7) pp.737-746. [알베르 카뮈의 ‘반항적 인간’]</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://blog.naver.com/toy83/220352523021' target='_blank'>반항은 죄악인가, 희망인가?</a></p>
    <p><a href='http://fr.wikipedia.org/wiki/L'Homme_révolté' target='_blank'>L'homme révolté</a></p>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/homme_revolte/camus_homme_revolte.pdf' target='_blank'>원문 pdf</p>
  </div>`,
  "legends":"<p>초판 신구철 1958년, 번역개정 '반항하는 인간' 김화영 2003년.</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"여름 L'Été",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1954,
  "language":"프랑스어",
  "t_title":"여름",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":1987,
  "keywords1":["지중해(Méditerranée)","여행(voyage)","세계(monde)","바다(mer)","신화(mythe)"],
  "keywords2":["에세이(essai)","알제리(algérie)","기행문(récit de voyage)","모음집(recueil)","신화(mythologie)"],
  "story":"<p>카뮈가 1939년부터 1953년까지 쓴 글을 모아놓은 『여름 L'Été』은 지중해와 관련된 텍스트들이다. 총 여덟 편의 짧은 글로 구성된 『여름』은 각기 다른 시기에 쓴 것이기 때문에 글마다 공통성을 보이지는 않는다.</p><p>「미노타우로스 또는 오랑에서 잠시 Le minotaure ou la halte d'Oran」는 역사와 관련이 없지만 「편도나무들 Les amandiers」은 역사의 중요성에 대해 말하기 시작한다. 「명부의 프로메테우스」에서는 프로메테우스를 반항하는 사람으로 지칭하면서 힘과 폭력에 대항하는 ‘반항’의 의미를 짚어본다. 알제리 여행을 하면서 느낀 감회를 정리한 「과거가 없는 도시들을 위한 간단한 안내 Petit guide pour des villes sans passé」에서는 『결혼 Noces』에서 다룬 「알제의 여름 L'été à Alger」을 느낄 수 있다. 「헬레네의 추방 L'exil d'Hélène」에서는 기독교와 헬레니즘의 관계를 다루고 있으며, 「수수께끼 L'énigme」는 작가로서의 자신에게 던져지는 질문에 답하고 있다. 「티파사에 돌아오다 Retour à Tipasa」역시 『결혼』을 떠올리게 하는 글이다, 「가장 가까운 바다 La mer au plus près」는 미국 여행을 떠올리며 쓴 산문으로 카뮈가 살기를 바란 환경을 알 수 있다.</p>",
  "network1":"<p>『반항적 인간 L'Homme révolté』 : 「명부의 프로메테우스 Prométhée aux Enfers」는 유럽이 겪고 있는 폭력의 문제를 거론하면서 『반항적 인간』을 떠올리게 한다.</p><p>『결혼 Noces』 : 『여름』과 같이 아름다운 묘사가 돋보이는 글들로 이루어진 산문집으로 특히 『결혼』과 마찬가지로 알제에 대한 이야기가 자주 등장한다.</p>",
  "network2":"<p>루키아노스, 카프카스의 프로메테우스 LUCIEN, Prométhée au Caucase – 「명부의 프로메테우스 Prométhée aux Enfers」의 가장 앞에 글귀를 인용했다. “신에 맞설 만한 것이 전혀 존재하지 않는 한 내가 보기에는 신성에 뭔가 결함이 있는 것같이 여겨졌다.”</p>",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    <ul>
      <li>김동현, 「까뮈의 문학적 에쎄 "여름"에 관한 연구」, 『아주대학교 논문집』, Vol. 9, 1986, pp143-152.</li>
      <li>이기언, 「카뮈와 알제리」, 『불어불문학연구』, 제95집 (2013, 가을), pp.267-311.</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <ul>
      <li><a href='https://fr.wikipedia.org/wiki/Albert_Camus' target='_blank'>‘알베르 카뮈’ 위키 백과사전 설명</a></li>
      <li><a href='https://fr.wikipedia.org/wiki/L'Été_(Camus)' target='_blank'>‘여름’ 위키 백과사전 설명</a></li>
      <li><a href='http://classiques.uqac.ca/classiques/camus_albert/ete/camus_ete.rtf' target='_blank'>원문(불어) 텍스트 파일</a></li>
    </ul>
  </div>`,
  "legends":"<p>완역 초판 1987</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"단두대에 대한 성찰 Réflexion sur la guillotine",
  "location":"프랑스 파리 Paris, France",
  "publisher":"칼만-레비 Calmann-Lévy",
  "published_year":1957,
  "language":"프랑스어",
  "t_title":"단두대에 대한 성찰",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2004,
  "keywords1":["사형(peine capitale)","범죄(crime)","형벌(peine)","단두대(guillotine)","사회(société)"],
  "keywords2":["에세이(essai)","사형(peine capitale)","철학·사상(philosophie)","부조리(absurde)","견해·사고(idée)"],
  "story":"<p>「단두대에 대한 성찰」은 사형제도에 대한 카뮈의 비판적인 생각을 담은 에세이로 이 글에서 카뮈는 사형제도의 비합리성을 말한다.</p><p>사형집행을 참관한 아버지의 경험을 통해 현실적인 사형집행의 잔혹함을 밝히면서 글을 시작하는 카뮈는 사형제도가 무용할 뿐만 아니라 대단히 해롭다고 주장한다. “사형 제도의 엄숙한 폐지가 우리 모두가 희망하는 유럽법의 제1조가 되어야 할 것이다.”, “죽음이 불법이 되지 않는 한 개인의 가슴 속에도, 사회의 풍속에도 항구적인 평화는 없을 것이다.”라는 부분에서 카뮈가 강력하게 사형제도의 폐지를 바라고 있음을 알 수 있다.</p><p>「단두대에 대한 성찰」에서 카뮈는 사형을 지지하는 사람들의 주장에 대해 크게 3가지 이유를 들어 반박한다. 사회는 스스로가 내세우는 본보기적 성격을 믿지 않고, 사형제도 때문에 살인을 포기했다는 사례가 없으며, 사형제도는 결과를 예측할 수 없는 혐오스러운 본보기가 된다는 것이다. 이 외에도 설득력 있는 이유를 제시하며, 사형제도에 비판적인 시각을 갖고 논리적으로 접근한다.</p>",
  "network1":"<p>페스트 La peste : 페스트에 등장하는 ‘타루’는 사형 집행 입회 법관이었던 아버지의 이야기를 하면서 혐오감을 느낀다. 「단두대에 대한 성찰」에서는 그 일화가 사실대로 소개된다.</p><p>이방인 L'Étranger : “아버지는 어느 살인범의 사형집행을 보러 갔었다는 것이다. ~ 그래도 아버지는 갔고, 돌아오자 아침에 먹었던 조반의 일부분을 토했다는 것이었다.” 「페스트」와 마찬가지로 사형집행과 관련된 일화가 제시된다.</p>",
  "network2":"<p>교수대와 십자가 La potence et la croix (벨라 쥐스트 Bela Just) : 사형집행에 30회 가까이 입회해 본 신부인 벨라 쥐스트가 쓴 글로 카뮈가 다음을 인용했다. “무고한 사람들이 사지를 와들와들 떨며 죽음을 향해 나아가는 것을 나는 보았다.”</p><p>괴물들 Les monstres (로제 그르니에 Roger Grenier) : 카뮈는 로제 그르니에가 쓴 『괴물들』에서 사형집행을 보조하는 사람에 대해 서술한 내용이 실제와 부합하는지 알고 싶어 했고 실제로 『단두대에 대한 성찰』에 인용했다.</p>",
  "network3":"<p>Albert Camus contre la peine de mort (Ève Morisi) : 카뮈의 소설('이방인', '페스트', '최초의 인간')에서 나타난 사형이라는 소재를 다룸.</p>",
  "character_network":"<p>아르튀르 케스틀러 Arthur Kœstler - 카뮈와 함께 토론을 하던 사이로, 「사형에 대한 성찰 Réflexions sur la peine capitale」의 공동 저자이다. 이 책에서 케스틀러는 「교수형에 대한 성찰 Réflexions sur la pendaison」이라는 글을 썼다.</p>",
  "social_network":`<div>
    <ul>
      <li>류철호, 사형제에 대한 소고, 법제처, 2010.</li>
      <li>이화교지편집위원회, 梨花 No.86, 2013, pp.104-115.</li>
      <li><a href='http://scholarlycommons.law.northwestern.edu/cgi/viewcontent.cgi?article=7189&context=jclc' target='_blank'>Ronald J. Allen, Further Reflections on the Guillotine, Journal of Criminal Law and Criminology Vol.95, No.2, 2005, pp.625-636</a></li>
      <li>Donald Lazere, Camus and his critics on capital punishment, Modern Age, Fall96, Vol. 38 Issue 4, 1996, pp.371-380</li>
      <li>Thomas Molnar, On Camus and Capital Punishment, Modern Age, Summer 1958 (1958), pp.298-306</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p>위키(<a href='https://ko.wikipedia.org/wiki/%EC%95%8C%EB%B2%A0%EB%A5%B4_%EC%B9%B4%EB%AE%88' target='_blank'>한국</a>/<a href='https://fr.wikipedia.org/wiki/R%C3%A9flexions_sur_la_peine_capitale' target='_blank'>프랑스</a>)</p>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/reflexions_guillotine/reflexions_guillotine.pdf' target='_blank'>원문 자료 (텍스트)</a></p>
  </div>`,
  "legends":"<p>신일철, 단두대(1959), 김화영, 단두대에 대한 성찰(2004)</p><p>「단두대에 대한 성찰 Réflexion sur la guillotine」은 Arthur Kœstler와 함께 쓴 『Réflexions sur la peine capitale 사형에 대한 성찰』 (Liberté de l'esprit (1957), Calmann-Lévy)에 수록된 에세이 중 하나다.</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"에세이",
  "author":"알베르 카뮈 Albert Camus",
  "title":"스웨덴 연설 Discours de Suède",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1958,
  "language":"프랑스어",
  "t_title":"스웨덴 연설",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["연설(discours)","예술론(théorie d'art)","작가의 역할(rôle de l'écrivain)"],
  "keywords2":["연설(discours)","예술론(théorie d'art)","노벨상(Prix Nobel)"],
  "story":`<div>
    <p>본 작품은 1957년 12월 10일 노벨상 수여식을 마감하는 연회 장소, 스톡홀롬 시청에서 까뮈가 수상 소감으로 행했던 ‘오슬로 연설’의 원고본 (Le discours de Oslo)과 이후 1957년 12월 14일 웁살라 대학에서 ‘예술가와 그의 시대’(Le discours d'Upsala)라는 제목으로 행한 강연의 원고본이다.</p>
    <b>오슬로 연설 (Le discours de Oslo)</b>
    <p>까뮈는 이 연설에서 예술과 작가의 역할에 관해서 이야기하고자 한다. 까뮈는 “예술이란 고독한 향락이 아니다”(p.350)라고 주장한다. 오히려 예술은 “인간의 공통적인 괴로움과 기쁨의 유별난 이미지를 제시함으로써 최대 다수의 사람들을 감동시키는 수단”(ibid.)이라고 이야기하는데, 예술은 일반 대중과 유리되어 그들을 ‘판단’하는 것이 아닌 ‘이해’하는 것임을 말한다. 또한 작가는 이러한 예술을 따라서 대중에 봉사해야 함을 주장한다. 그리하여 까뮈는 “작가는 오늘날 역사를 만드는 사람이 아니라 역사를 겪는 사람을 위해서 봉사할 수밖에 없습니다.”(p.351)라고 이야기한다.</p>
    <b>예술가와 그의 시대’(Le discours d'Upsala)</b>
    <p>본 작품은 전후 복구, 알제리 전쟁과 동서 냉전의 시작 등 혼란스러운 유럽의 1950년대 상황 속에서 더 이상 예술가가 시대적 상황과 무관 할 수 없음을 지적하며, 사회 속에서의 예술에 대해 논한 강연 원고이다. 까뮈는 당시의 혼란한 시대 상황, “그토록 많은 이데올로기들을 지키는 경찰이 득실대는 세상”(p.358)에서는 예술가가 예술에 대해 회의를 느껴 “살아있는 현실과는 단절된 예술”(p.361)로 귀착하게 됨을 지적한다. 그리하여 예술은 ‘예술을 위한 예술’이 된다는 것이다. 그러나 동시에 예술은 우리의 공통적 현실을 놓치지 않기 위해 리얼리즘에 돌입하였다고 까뮈는 지적한다. 그렇지만 “부르주아 사회와 그 사회의 형식적인 예술을 거부하고 현실을, 오직 현실만을 말하고자 하는 예술가는 고통스러운 곤경에 처하게 된다.” (p.368). 왜냐하면 선택이 아닌 있는 그대로 모든 현실을 재현해 내는 것이란 불가능하기 때문이다. 따라서 까뮈는 이러한 두가지 예술적 견해, “동시대적 상황을 전적으로 거부하라고 권유하는 미학과 동시대적 상황이 아닌 모든 것이면 모두 다 거부한다고 자처하는 미학”(p.373) 모두에 대해 비판적 의문을 제기한다. 그렇다면 예술은 어떠한 방향으로 나아가야 할까? 까뮈는 여기에 대해 “우리 모두 기뻐합시다” (p.381)라는 말로 대답한다. 예술이 오늘날, 사회와 유리된 ‘안락한 장소’에서 시대를 평하던 과거에서 벗어나 시대 상황 속에서 그것에 대해 논해야 함을 받아들이고 오히려 그것에 기뻐한다면 예술과 지혜의 사멸을 막을 수 있으리라는 결론을 내린다.</p>
  </div>`,
  "network1":"<p>&lt;전쟁과 평화&gt;, &lt;파롬의 수도원&gt;, &lt;악령&gt;, &lt;베레니스&gt; : 본문에서 언급되는 작품들</p>",
  "network2":"-",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    <p>김병익(2013), 「까뮈 읽기 (7) - 전집 18 『스웨덴연설 〮문학비평』, 『본질과현상』, Vol.32. pp.242~215.</p>   </div>`,
  "digital_works":`<div>
    <p><a href='https://fr.wikipedia.org/wiki/Discours_de_Su%C3%A8de' target='_blank'>스웨덴 연설에 대한 wikipedia의 프랑스어 기술</a></p>
    <p><a href='http://blog.naver.com/eiron16/30111798922' target='_blank'>스웨덴 연설과 관련한 네이버 블로거 기술</a></p>
  </div>`,
  "legends":"<p>초판 1958, 개정년도 1997</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"장 폴 사르트르의 『구토』 La Nausée, par  Jean-Paul Sartre (20 octobre 1938)",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"장 폴 사르트르의 『구토』",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","장 폴 사르트르(Jean Paul Sartre)","구토(La Nausée)"],
  "keywords2":["카뮈(Camus)","실존주의(existentialisme)","실존(existence)","인간(homme)","휴머니즘(humanisme)"],
  "story":"<p>“한 편의 소설은 이미지로 바꾸어 놓은 철학에 불과하다. 그리고 한 편의 좋은 소설에서 철학은 송두리째 이미지들 속으로 흘러들어가서 표현된다”(p.481) 본 비평문에서 카뮈는 이상과 같이 자신의 소설관을 드러내며 사르트르의 『구토』에 대해 비평한다. 카뮈에 따르면 “끈질긴 생명을 가진 작품은 심오한 사상 없이는 성립될 수 없다.”(p.481) 그러나 사르트르의 『구토』는 “인간에게서 구역질을 자아내는 것에 강조점을 두는 것으로 보아 불안의 진정한 의미를 제시하지 못한 것 같다”(p.484)라고 카뮈는 평가한다. 왜냐하면 삶의 부조리를 확인한다는 것은 목적이 아닌 시작에 불과하기 때문이다. 카뮈는 “우리의 관심사는 부조리의 발견이 아니라 거기서 이끌어 낼 수 있는 귀결들과 규칙들”(p.484)이라고 이야기한다. 이처럼 카뮈는 『구토』에 드러난 한계를 지적함과 동시에 “의식적인 사고의 극단에서 버티며 지탱하는 이토록 자연스러운 유연함, 이토록 고통에 찬 명철함은 곧 무한한 재능을 드러내는 것”이라는 평가를 보내며 『구토』에 대한 비평을 남긴다. </p>",
  "network1":"<p>-『작가수첩 I(Carnets, tome I) : Mai 1935 - février 1942』, Gallimard, 2013. : 이 작품(p.23)에서 청년 시절의 카뮈는 “우리의 사고는 오직 이미지로밖에 이루어지지 않는다. 철학자가 되고 싶거든 소설을 써라”라는 문장을 남기며 본문에서의 소설관과 상응하는 문구를 남김</p><p>-『안과 겉(L'Envers et l'Endroit)』, Gallimard, 1986. : 1958년 이 책의 재판 서문(pp.31-32)에서 카뮈는 “한 인간의 작품이라는 것은 예술의 수단을 빌어서 마음의 문이 처음으로 열리는 두세 개의 단순하면서도 위대한 이미지를 찾아내기 위한 기나긴 여정 이외에 다른 아무것도 아니다”라고 쓰며 자신의 예술관을 드러내고 있다.</p><p>-『작가수첩 II(Carnets, tome II) : Janvier 1942 - mars 1951』, Gallimard, 2013. : 이 작품에서(p.146) 『이방인』과 『시지프스의 신화』를 출간한 후의 카뮈는 “왜 나는 철학자가 아니라 예술가인가? 그것은 내가 이념(idée)에 따라 사고하는 것이 아니라 낱말들(mots)에 따라 사고하기 때문이다”라고 말하며 자신의 예술관을 밝히고 있다.</p>",
  "network2":"<p>『구토La Nausée』 : 해당 작품은 본문에서 카뮈의 비평 대상이 되는 작품이다.</p><p>『인간의 조건』 :카뮈는 “위대한 소설가를 만들어내는 것은 경험과 사상, 생명과 그 의미에 대한 반성의 혼합이다“(p.481)라고 말하며 그 예시로 앙드레 말로의 이 작품을 들고 있다.</p>",
  "network3":"-",
  "character_network":"<p>사르트르 : 본문에서 비평의 대상이 되고 있는 작품 『구토』의 작가이다.</p><p>키르케고르Søren Aabye Kierkegaard, 셰스토프Leo Shestov, 야스퍼스Karl Jaspers, 하이데거Martin Heidegger : 카뮈는 사르트르의 『구토』 속에 이상의 철학자들의 불안의 철학이 내재되어 있다고 보았다. (p.482)</p>",
  "social_network":`<div>
    <p>이기언, 「까뮈와 실존철학」, 『지중해지역연구』, 제7권 제1호(2005,4). : 철학자와 작가로서의 카뮈의 정체성에 관한 논문이다.</p>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://wikilivres.ca/wiki/La_Naus%C3%A9e,_de_Jean-Paul_Sartre' target='_blank'>본문의 원문 자료</a></p>
  </div>`,
  "legends":"<p>갈리마르 초판 2006, 최초 실린 곳 : 1938.10.20. «Alger Républicain»</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"장 폴 사르트르의 『벽』 Le Mur, par Jean-Paul Sartre (12 mars 1939)",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"장 폴 사르트르의 『벽』",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","장 폴 사르트르(Jean Paul Sartre)","벽(Le Mur)"],
  "keywords2":["카뮈(Camus)","실존주의(existentialisme)","실존(existence)","인간(homme)","휴머니즘(humanisme)"],
  "story":"<p>장 폴 사르트르의 『벽』에 대한 카뮈의 비평이다. 카뮈에 따르면 사르트르가 『벽』에서 보여주고자 하는 목적은 “가장 변태적인 인간이 가장 평범한 인간처럼 행동하고 또 그렇게 스스로를 묘사한다는 것을 보여주자는 데 있다”(p.491). 이런 관점에서 비판해야 할 것이 있다면 외설을 이용하는 저자의 태도일 것(p.491)이라고 카뮈는 이야기한다. 문학에 있어서 외설이 위대한 경지에 이르는 것은 가능하다. 하지만 그것은 “적어도 작품이 그것을 요구할 때 그렇다.”(p.491) 카뮈는 『벽』에서 「에로스트라트」의 경우는 거기에 해당되지만 「친밀한 관계」는 그렇지 못한 경우로 성적인 묘사가 대개 무용하다는 느낌을 준다”(p.491)고  말한다. 그러나 종국에 이르러 카뮈는 해당 작품에 대해 “이 밀도 있고 극적인 세계, 빛나면서도 동시에 색채 없는 이 그림은 사르트르의 작품 세계를 잘 규정해주며 그 자체가 작품의 매력이기도 하다.”(p.493)는 말로 『벽』에 대한 평을 남긴다.</p>",
  "network1":"-",
  "network2":"<p>『벽』 : 본 비평문에서 비평의 대상이 되는 사르트르의 작품</p>",
  "network3":"<p>사르트르(Sartre) : 이 비평문이 대상으로 삼고 있는 『벽』의 작가</p>",
  "character_network":"",
  "social_network":`<div>
    <p>윤정임, 「카뮈-사르트르 논쟁사」, 『유럽사회문화』, vol.6, 2011, pp.5-27.<br/>: 카뮈와 사르트르의 관계를 보여주는 논문</p>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://seesunblog.tistory.com/184' target='_blank'>사르트르의 『벽』에 대한 소개가 실린 블로그</a></p>
    <p><a href='http://wikilivres.ca/wiki/Le_Mur,_de_Jean-Paul_Sartre' target='_blank'>『벽』의 불어 원문 텍스트</a></p>
  </div>`,
  "legends":"<p>갈리마르 초판 2006. 최초 실린 곳 : 1939.3.12. «Alger Républicain»</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"이냐치오 실로네의 『빵과 포도주』 Le Pain et le Vin, par Ignazio Silone (23 mai 1939)",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"이냐치오 실로네의 『빵과 포도주』",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","이냐치오 실로네(Ignazio Silone)","빵과 포도주(Pane e vino)"],
  "keywords2":["이냐치오 실로네(Iganzio Silone)","빵과 포도주(Pane e vino)"],
  "story":"<p>이냐치오 실로네의 『빵과 포도주』에 대한 비평문이다. 이 소설은 반파시스트 소설이다. 그러나 카뮈에 따르면 “소설이 담고 있는 메시지는 반파시즘을 넘어선다.”(p.494) 왜냐하면 이 작품이 “승리와 정복을 찬양하는 작품이 아니라 혁명의 가장 고통스러운 갈등 상황들을 노출시키는 작품”(p.495)이기 때문이다. 카뮈는반항자를 그린 『빵과 포도주』라는 소설이 가장 고전적인 형식 속에 녹아 있다고 이야기한다. “짧은 문장, 순진하면서도 사려 깊은 세계관, 자연스럽고 밀도 있는 대화”(p.495)로 구성되어 있기 때문이다. 또한 카뮈는 “진실을 되찾는다는 것, 혁명의 추상적인 철학으로부터 단순 소박함의 빵과 포도주로 되돌아온다는 것이 바로 이냐치오 실로네의 여정과 이 소설의 교훈이다.”(p.496)라는 평가를 내림으로써 이 작품에 대해 이야기한다.</p>",
  "network1":"-",
  "network2":"<p>『빵과 포도주Pane e vino』 : 본 비평문의 비평 대상이 되는 작품이다.</p>",
  "network3":"-",
  "character_network":"<p>이냐치오 실로네Ignazio Silone  : 본 비평문의 해당 작품인 『빵과 포도주』의 저자이다.</p>",
  "social_network":`<div>
    <p><a href='http://armeniantrends.blogspot.fr/2012/11/albert-camus-ignazio-silone.html'>카뮈와 실로네의 관계를 다룬 기사 (불어 번역본)</a> : 지오반니 아콜라(Giovanni F. Accolla), 「카뮈와 실로네, 조국에서의 이방인들, 프랑스와 이탈리아의 뒤흔드는 비판정신들(Etrangers dans leur patreid, Camus et Silone, dérangeantes consciences critiques de France et de'Italie)」, 『토탈리타(Totalita)』, 2012.</p>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://www.imaeil.com/sub_news/sub_news_view.php?news_id=20462&yy=2014' target='_blank'>『빵과 포도주』의 저자 이냐치오 실로네의 관한 소개 기사</a></p>
  </div>`,
  "legends":"<p>갈리마르 초판 2006. 최초 실린 곳 : 1939.5.23. «Alger Républicain»</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"프랑시스 퐁주의 시집 《사물의 편》에 대한 편지 Lettre au sujet du Parti pris.",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"프랑시스 퐁주의 시집 《사물의 편》에 대한 편지",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","프랑시스 퐁주(Francis Ponge)","사물의 편(Le Parti pris des choses)","사물(chose)","부조리(absurde)"],
  "keywords2":["퐁주(Ponge)","사물(chose)"],
  "story":"<p>프랑시스 퐁주의 『사물의 편』과 카뮈 자신의 저작 『시지프 신화』에 대한 프랑시스 퐁주의 의견을 읽은 뒤 카뮈가 N.R.F 통권 45호에 게재한 글이다.</p><p>카뮈는 “『사물의 편』이 순수한 상태의 부조리한 작품”(P.532)라고 말한다. 또한 ”내가 보기에 참으로 값진 것은 당신이 선택한 측면, 즉 표현의 측면에서, 당신의 대가적 기량 그 자체가 당신의 실패의 고백을 설득력 있게 만들고 있다는 사실입니다”(P.532)와 같이 퐁주의 작품에 대해 호의적인 모습을 내보인다. 카뮈에 따르면 퐁주의 작품에서 가장 인상적인 것은 “인간이 배제된 자연, 재료”(P.534) 즉 사물들이다. 퐁주는 “부조리한 세계를 최종적으로 구체화하는”것은 다름 아닌 오브제라는 사실을 증명해 보이고 있다고 카뮈는 말한다. 그에 덧붙여 퐁주의 사상적 견지와 자신의 사상적 견지에 대한 공통점(“좋은 의미의 허무주의는 상대적인 것과 인간적인 것으로 인도하는 허무주의다. 존재론에 대한 나의 취향에도 불구하고 바로 그 점에서 나는 당신과 일치합니다”.P.537)과 퐁주가 나아가야 할 길(“휴머니즘과 정열적인 상대주의로 귀결되는 당신의 경험, 즉 그 집요한 표현의 추구는 그 무엇으로도 바꿀 수 없는 것입니다. 당신은 그것에 어떤 형식을 부여해야 한다고 나는 생각합니다.”P.538)을 언급하며 글을 마친다.</p>",
  "network1":"-",
  "network2":"<p>『사물의 편』 : 본 서신문에서 카뮈가 언급하는 퐁주의 대표적 시집</p>",
  "network3":"-",
  "character_network":"<p>프랑시스 퐁주 : 본 서신문의 수신자인 프랑스 시인</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2006. 최초 발표 시기 : 1943.1, 출간은 늦어져 1956년 9월</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"지성과 단두대 L'Intelligence et l'Échafaud",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"지성과 단두대",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","단두대(Guillotine)"],
  "keywords2":["문학 비평(Eassais critiques)","단두대(Guillotine)"],
  "story":"<p>프랑스 고전 소설에 대한 성찰이 담긴 비평문이다. 카뮈에 따르면 “프랑스 소설의 중심을 이루는 것은 어떤 종류의 집요한 추구”(p.498)라고 말할 수 있다. 그러나 프랑스 소설이 추구하는 것은 “형식을 위한 형식”(p.498)이 아니라 어조와 사상 사이의 정확한 관계에 대한 것이다. 카뮈는 프랑스 고전 소설의 특성을 “통일성과 깊이 있는 단순성”(p.500)에서 찾는다. 따라서 그는 라파예트 부인의 『클레브 공작부인』을 예시로 들어 그 작품이 밀고 나가는 사랑에 대한 정념을 토대로 이러한 논지를 설명하며 프랑스 소설의 요체는 “숙명에 대한 조화 있는 감각과 동시에 개인적 자유에서 통째로 생겨난 기법을 나타낼 수 있다는 것, 그렇게 하여 마침내 운명의 힘들이 인간의 결단과 충돌하는 저 이상적인 영토를 형상화한다는 것”(p.506)이라고 평하며 글을 마친다. </p>",
  "network1":"-",
  "network2":"<p>『클레브 공작부인(La Princesse de Clèves)』, 『아돌프(Adolphe)』 : 카뮈는 프랑스 고전 소설의 특성을 설명하기 위해 해당 작품을 본문에서 예시로 들고 있다.</p>",
  "network3":"-",
  "character_network":"<p>사드(Sade), 프루스트(Proust), 스탕달(Stendhal), 라파예트(La Fayette) : 카뮈는 프랑스 고전 소설의 특성을 설명하기 위해 해당 작가들을 본문에서 예시로 들고 있다.</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://www.alalettre.com/la-fayette-oeuvres-cleves-evoquee.php' target='_blank'카뮈를 비롯해 여러 작가들이 『클레브 공작 부인』에 대해 평가한 글들을 모아놓은 웹페이지</a></p>
  </div>`,
  "legends":"<p>갈리마르 초판 2006, 최초 실린 곳 : 1943.7, «Confluences» 특집호 n°21-24</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"샹포르의 《잠언집》 서문 Introduction aux « Maximes » de Chamfort",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1982,
  "language":"프랑스어",
  "t_title":"샹포르의 《잠언집》 서문",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critique)","샹포르(Chamfort)","잠언집(Maximes)"],
  "keywords2":["문학 비평(Eassais critique)","샹포르(Chamfort)","잠언집(Maximes)"],
  "story":"<p>냉철한 눈으로 구체제 말기의 상류 사회의 인간과 풍속에 신랄한 비평을 가한 18세기의 프랑스 작가 샹포르(Chamfort)의 『잠언집(Maximes)』에 관한 비평문이다. 카뮈에게 있어 모랄리스트들의 잠언이란 “수험생들의 논술 시험 준비에나 적합한 결정적인 공식들을 정성스럽게 다듬어 만드는 것”(p.389)으로 여겨질 뿐이었다. 카뮈는 ‘잠언의 모든 진실은 그것 자체 속에 있을 뿐이어서 대수 공식과 마찬가지로 인간의 구체적인 경험 속에서 그것에 해당하는 항을 찾아낼 수가 없다(ibid.)라고 이야기한다. 왜냐하면 잠언은 현실이 배제된 일반적인 이야기에 대한 것들에 대해서만 논하고 있을 뿐이기 때문이다. 그러나 이러한 잠언의 성격에도 불구하고 샹포르의 『잠언집(Maximes)』에 대해 카뮈는 다음과 같이 긍정적인 평가를 내보이고 있다. “샹포르는 자신의 세상 경험을 공식으로 만들어 보이지 않는다. 아주 대단한 그의 솜씨는 오직 더없을 만큼 적확한 묘사의 필치에 유감없이 동원되고 있다.”(p.391) 이 작품은 이러한 시각에서 샹포르의 작품이 여타 모럴리스트들의 잠언집과는 달리 현대 소설 기법을 사용하고 있음에 주목하며   『잠언집(Maximes)』과 『일화집(Anecdote)』을 분석한다.</p>",
  "network1":"<p>『잠언집(Maximes)』 : 본 비평문이 비평 대상으로 삼고 있는 샹포르의 작품</p>",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>샹포르(Nicolas Sébastien de Chamfort) : 본 비평문에서 비평 대상이 되고 있는 프랑스의 작가</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://terms.naver.com/entry.nhn?docId=882002&cid=43671&categoryId=43671' target='_blank'>본 비평문에서 비평 대상이 되고 있는 작가 샹포르의 백과사전 항목</a></p>
    <p><a href='https://fr.wikipedia.org/wiki/S%C3%A9bastien-Roch_Nicolas_de_Chamfort' target='_blank'>샹포르의 프랑스어 위키백과 항목</a></p>
  </div>`,
  "legends":"<p>갈리마르 초판 1982, 최초 실린 곳 : 1944, Incidences 총서</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"쥘 루아의 『행복한 골짜기』 La Vallée heureuse de Jules Roy (février 1947)",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"쥘 루아의 『행복한 골짜기』",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","쥘 루아(Jules Roy)","행복한 골짜기(La Vallée heureuse)"],
  "keywords2":["문학 비평(Eassais critiques)","쥘 루아(Jules Roy)","행복한 골짜기(La Vallée heureuse)"],
  "story":"<p>쥘 루아의 소설 『행복한 골짜기』에 대한 평론이다. 카뮈에 따르면 당대의 문학은 “예술로부터 등을 돌리고 온통 날것 그대로의 고통 속으로 몸을 던지도록 강요”(p.520)하는 시대로 인해 “푸줏간 도마 위의 생고기 쪽으로의 새로운 경도 현상”(p.519)을 보이고 있었다. 그러나 카뮈는 “예술은 염치가 없이는 성립되지 않는다.”라고 주장하며 이러한 문학적 경향에 대한 비판을 가하며 예시로서 『행복한 골짜기』를 평하고 있다. 해당 작품은 “시대의 모든 절대적 요청을 따르면서도 바로 그 살육의 도살장에 약간의 섬세함을 도입하고 있”(p.520)기 때문이다. 카뮈는 이 작품이 “우리가 늘 요구하는 위대한 휴머니즘 소설들이 아니라 우리가 오랫동안 망각하고 있었던 취향인 힘과 염치의 작품들 가운데 자리하는 것”(p.522)이라 평한다.</p>",
  "network1":"-",
  "network2":"<p>『행복한 골짜기』 : 「쥘 루아의 『행복한 골짜기』」에서 비평 대상이 되고 있는 작품이다.</p>",
  "network3":"-",
  "character_network":"<p>쥘 루아 : 「쥘 루아의 『행복한 골짜기』」에서 비평 대상이 되고 있는 프랑스의 작가</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2006, 최초 실린 곳 : 1947.2, «L'Arche» n°24 </p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"루이 기유의 《민중의 집》 서문 Essais critiques :  Avant-propos à « La Maison du peuple », de Louis Guilloux",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1965,
  "language":"프랑스어",
  "t_title":"루이 기유의 《민중의 집》 서문",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critique)","루이 기유(Louis Guilloux)","민중의 집(La Maison du peuple)","프롤레타리아(prolétaire)"],
  "keywords2":["문학 비평(Eassais critique)","루이 기유(Louis Guilloux)","민중의 집(La Maison du peuple)"],
  "story":"<p>작품의 주제를 평범한 일상생활에서 잡고 인간의 비참함을 감동적으로 그리는 대중적인 경향을 지닌 프랑스의 작가 루이 기유(Louis Guilloux)의 대표작 《민중의 집 La maison du peuple》에 관한 비평문이다.</p><p>카뮈에게 있어 당시의 사회 발전 이론의 전문가들이 발간한 잡지나 서적들은 “흔히 프롤레타리아를 무슨 이상한 풍습을 가진 종족쯤이나 되는 듯이 취급하면서 프롤레타리아가 그 글을 읽는다면 구역질이 솟구칠 것 같은 투로 말을 하고 있”(p.404)었다. 왜냐하면 “프롤레타리아의 이름으로 말하고 글을 쓴다고 자처하는 거의 모든 프랑스 작가들“(p.403)이 부유한 집안의 출신들이었기 때문이다. 그러나 루이 기유는 ”일찍이 가난이라는 수업을 받아보았기에 인간이라는 것에 대하여 꾸밈없는 판단을 내리는 법“(pp404~405)을 배운 작가였기에 프롤레타리아에 대해 아첨하지도 멸시하지도 않는 진실된 시각을 가진 작가였다. 또한 루이 기유는 단순히 ”가난의 소설가”(p.406)가 아니다. 까뮈는 그와 나눈 대화를 토대로 루이 기유가 “거의 언제나 남의 입장이 되어 고통을 생각하는 사람”, “고통의 소설가”(p.407)라는 평을 남기며 《민중의 집》에 대해 이야기한다.</p>",
  "network1":"-",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>발레스(Jules-Louis-Joseph Vallès), 다비(Eugéne Dabit), 로렌스(David Herbert Lawrence), 톨스토이(Lev Nikolayevich Tolstoy), 레닌(Lenin', Vladimir Il'Ich) : 작품 내에서 언급되는 인물들</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://terms.naver.com/entry.nhn?docId=1071384&cid=40942&categoryId=33463' target='_blank'>루이 기유에 대한 네이버 백과사전 기술</a></p>
    <p><a href='https://fr.wikipedia.org/wiki/Louis_Guilloux' target=''_blank'>루이 기유에 대한 wikipedia 프랑스어 기술</a></p>
    <p><a href='https://fr.wikipedia.org/wiki/La_Maison_du_peuple' target='_blank'>《민중의 집》에 대한 wikipedia 프랑스어 기술</a></p>
  </div>`,
  "legends":"<p>갈리마르 초판 1965, 최초 실린 곳 : 1948.1, «Caliban», 이후 1953년 루이 기유의 '민중의 집'이 Grasset에서 출간될 때 서문으로 실림</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"앙드레 지드와의 만남 Essais critiques :  Rencontres avec André Gide",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1965,
  "language":"프랑스어",
  "t_title":"앙드레 지드와의 만남",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critique)","앙드레 지드(André Gide)"],
  "keywords2":["문학 비평(Eassais critique)","앙드레 지드(André Gide)"],
  "story":"<p>앙드레 지드에 대한 《누벨 르뷔 프랑세즈 La Nouvelle Revue Française》지의 추도 특집(p.418)에 실린 카뮈의 추모글이다. 카뮈는 처음 앙드레 지드의 작품을 읽었던 16살의 시절을 회상하며 글을 시작한다. 당시 그가 읽은 《지상의 양식》은 큰 인상을 주지 못했다 “자연이 주는 부에 대한 이 찬가를 읽으면서 나는 어리둥절했다. 알제에 사는 열여섯 살 먹은 나로서는 자연이 주는 그런 풍요는 넘쳐나다 못해 물릴 지경이었던 것이다.”(p.411) 그러나 이후 카뮈가 “진정으로 독서를 하기 시작”(p.413)한 이후 그는 지드의 모든 작품을 다 읽었고 《지상의 양식》으로부터 “남들에게 말로만 들었던 충격”(ibid.)을 받게 되었다.  이후 카뮈는 지드에게 받은 영향들에 대해 서술하며 파리 생활 시절 지드의 이웃집에 살며 겪은 지드에 대한 술회로써 그를 추모한다.</p>",
  "network1":"<p>《여인의 편지Lettres de Femme》, 《파르다양Pardaillan》, 《지상의 양식Nourritures terrestres》, 《고통La Dou-leur》, 《나르시스론Le Traité du Narcisse》, 《사랑의 시도La Tentative amoureuse》, 《탕아 돌아오다Retour de l’enfant prodigue》 : 작품 속에서 언급되고 있는 작품들이다.</p>",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>장 그르니에(Jean Grenier), 앙드레 드 리쇼(André de Richaud): 작품 속에서 언급되고 있는 인물들이다.</p>",
  "social_network":`<div>
    <p>Raymond Gay-Crosier, « André Gide et Albert Camus  : Rencontres », Études littéraires, vol. 2, n° 3, 1969, pp. 335-346.</p>
   </div>`,
  "digital_works":`<div>
    <p><a href='https://ko.wikipedia.org/wiki/%EC%95%99%EB%93%9C%EB%A0%88_%EC%A7%80%EB%93%9C' target='_blank'>앙드레 지드에 대한 wikipedia 한국어 기술</a></p>
    <p><a href='https://fr.wikipedia.org/wiki/Andr%C3%A9_Gide' target='_blank'>앙드레 지드에 대한 wikipedia 프랑스어 기술</a></p>
  </div>`,
  "legends":"<p>갈리마르 초판 1965, 최초 실린 곳 : 1951.11, «N.R.F.»</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"허먼 멜빌 Herman Melville",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2008,
  "language":"프랑스어",
  "t_title":"허먼 멜빌",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","허먼 멜빌(Herman Melville)"],
  "keywords2":["문학 비평(Eassais critiques)","허먼 멜빌(Herman Melville)","유머(humour)","힘(force)"],
  "story":"<p>허먼 멜빌의 문학세계에 대한 카뮈의 평론이다. 카뮈에 따르면 멜빌의 작품들은 “여러 가지 다른 방식으로 읽고 해석할 수 있는 예외적인 작품들에 속한다.”  그리하여 그의 작품들은 “자명한 것 같으면서도 동시에 신비스럽고 환하게 빛이 비치는 듯하면서도 난해하고 그렇지만 깊은 물속처럼 투명한 것”(p.526)처럼 다양한 측면을 가진 문학 작품들인 것이다. 카뮈는 또한 멜빌의 작품들에 대해 “거기서는 건강미, 힘, 뿜어져 나오는 유머, 인간적인 웃음이 폭발하고 있다.”(p.529)고 평한다.</p>",
  "network1":"<p>'시지프 신화' : 카뮈는 '시지프 신화'에서 '모비 딕'을 언급하면서 도스토예프스키나 카프카보다도 멜빌이 더 부조리의 작가라고 언급하고 있다</p>",
  "network2":"<p>『사기꾼』, 『모비 딕』, 『빌리 버드』, 『피에르 혹은 애매성』 : 「허먼 멜빌」에서 비평 대상이 되고 있는 작가 멜빌의 작품들이다.</p>",
  "network3":"-",
  "character_network":"<p>허먼 멜빌 : 「허먼 멜빌」에서 비평 대상이 되고 있는 미국 작가</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2008, 최초 실린 곳 : 1952.11, «Les écrivains célèbres» t. III, Mazenod.</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"감옥에 갇힌 예술가 L'Artiste en prison",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1965,
  "language":"프랑스어",
  "t_title":"감옥에 갇힌 예술가",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","오스카 와일드(Oscar Wilde)","옥중기(De Profundis)"],
  "keywords2":["문학 비평(Eassais critiques)","오스카 와일드(Oscar Wilde)","희극(comédie)","예술(art)"],
  "story":"<p>여러 편의 희극의 성공을 토대로 사교계의 관심 속에 화려한 삶을 살았으나 1895년 미성년자와의 동성애 혐의로 기소되어 2년간 레딩 교도소에 수감된 오스카 와일드에 대한 카뮈의 평론이다. 사교계의 관심과 문단에서의 성공을 토대로 화려한 삶을 살아가던 오스카 와일드에게는 “두 가지의 세계, 즉 일상의 세계와 예술의 세계가 전혀 별개로 존재”(p.419)했다. 그리하여 그는 일상의 현실에서 멀어져 자신의 예술이 주는 아름다움만을 바라보는 삶을 살게 되었다. 그러나 카뮈의 말에 의하면, 미성년자와의 동성애 추문으로 기소된 후의 옥살이는 이러한 오스카 와일드의 삶을 송두리째 바꾸게 된다. 이전에 자신의 현실이었던 ‘빛나는 삶’을 잃고 “허름한 옷차림으로 감옥에 갇혀 노예 취급을 받는 신세”(p.422)가 되었다. 하지만 이러한 고통은 그로 하여금 ‘연민’이라고 하는 것을 발견하게 해주었다. 카뮈에 의하면 연민은 “고통 받는 자의 마음을 움직일 수 있는 유일한 힘”(p.422)이다. 이제 와일드는 이전의 화려한 과거의 삶은 잃었지만 “우리의 아주 가까운 곳, 예술과 노동이 똑같은 궁핍 속에서 친밀감을 맛보는”(p.428)곳에 자리잡게 되었다.</p>",
  "network1":"<p>옥중기(De Profundis), 레딩 감옥의 노래(The Ballad of Reading Gaol), 도리언 그레이의 초상(The Picture of Dorian Gray), 살로메(Salomé), 리어 왕(King Lear), 전쟁과 평화(Вой наимир)</p><p><a href='http://terms.naver.com/entry.nhn?docId=1129234&cid=40942&categoryId=40488' target='_blank'>오스카 와일드에 대한 네이버 백과사전의 기술</a></p><p><a href='http://terms.naver.com/entry.nhn?docId=1128886&ref=y&cid=40942&categoryId=32892' target='_blank'>옥중기에 대한 네이버 백과사전의 기술</a></p>",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>오스카 와일드(Oscar Wilde), 앙드레 지드(André Gide)</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 1965, 최초 실린 곳 : 1952, «Falaize»</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"로제 마르탱 뒤 가르 Roger Martin du Gard",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1965,
  "language":"프랑스어",
  "t_title":"로제 마르탱 뒤 가르",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","로제 마르탱 뒤 가르(Roger Martin du Gard)"],
  "keywords2":["문학 비평(Eassais critiques)","로제 마르탱 뒤 가르(Roger Martin du Gard)"],
  "story":"<p>프랑스의 작가 로제 마르탱 뒤 가르(Roger Martin du Gard)에 대한 비평문이다. 카뮈의 평가에 따르면 당대의 문학은 “가치 있는 것일 경우 톨스토이보다는 도스토옙스키를 준거로 삼고 있다고 볼 수 있”(p.429)었다 그러나 “로제 마르탱 뒤 가르는 그와 반대로 그 세대 사람들 가운데 톨스토이 계열이라고 간주할 수 있는 유일한 문인이다“(p.430)라고 카뮈는 이야기한다. 그리고 뒤이어 마르탱 뒤 가르의 주요 작품인 『변화하라!Deveir!』, 『오래된 프랑스』,  『티보 가의 사람들』 등을 통해서 마르탱 뒤 가르의 문학 세계를 조명한다.</p>",
  "network1":"-",
  "network2":"<p>『변화하라!Deveir!』, 『악령Бесы』, 『전쟁과 평화Вой наимир』, 『티보 가의 사람들Les Thibault』, 『장 바루아Jean Barois』, 『오래된 프랑스Vieille France』, 『아프리카의 비밀Confidence africaine』, 『과묵한 사람Un Taciturne』, 『왕성의 길La Voie royale』, 『1914년 여름L'Été 1914』, 『아버지의 죽음 La mort du père』, 『출범 준비 L'Appareillage』</p>",
  "network3":"-",
  "character_network":"<p>로제 마르탱 뒤 가르(Roger Martin du Gard), 톨스토이(Lev Nikolayevich Tolstoy), 도스토옙스키(Fyodor Mikhailovich Dostoevskii), 플로베르(Gustave Flaubert), 말로(André Malraux)</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 1965, 최초 실린 곳 : 1955.10, 로제 마르탱 뒤 가르 전집(갈리마르, 플레이야드 총서) 서문</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"르네 샤르 PRÉFACE À L’ÉDITION ALLEMANDE DES 《POÉSIES》 DE RENÉ CHAR",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2008,
  "language":"프랑스어",
  "t_title":"문학 비평 – 르네 샤르",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","르네 샤르(René Char)"],
  "keywords2":["문학 비평(Eassais critiques)","르네 샤르(René Char)","반항(révolte)"],
  "story":"<p>프랑스의 시인 르네 샤르에 대한 카뮈의 평론이다. 카뮈의 평가에 따르면 르네 샤르는 “반항과 화내는 것을 혼동하지”(p.475) 않는 시인이었다. 반항에는 두 가지 종류가 있다고 카뮈는 말하는데 “하나는 예속에의 동경을 감추고 있는 반항이고 다른 하나는 샤르의 절묘한 표현처럼 빵이 치유 받게 되는 자유로운 질서의 세계를 절망적으로 요구하는 반항”(pp.475-476)이다. 즉 샤르는 모든 독트린들을 초월하며 우정에 대한 지향을 의미하는 시인이라고(p.476) 카뮈는 말한다.</p>",
  "network1":"-",
  "network2":"<p>『열광과 신비Fureur et Mystère』 『일뤼미나시옹Les Illuminations』 『알코올Alcools』 『홀로 머물다Seuls demeurent』 『흩뿌려진 시Poème pulvérisé』</p><p>『힙노스의 종잇장들Feuillets d'Hypnos』 : 본 작품들은 르네 샤르의 작품으로서 본문에서 언급되고 있다.</p>",
  "network3":"-",
  "character_network":"<p> 르네 샤르(René Char): 본 작품이 비평 대상으로 삼고 있는 인물</p><p>발레리Paul Valéry : p.475에서 다음과 같이 언급 “우리가 한 발 한 발 앞으로 나아가고 있는 어둠 속에서 발레리적인 하늘의 고정되고 둥근 빛은 우리에게 아무런 소용이 되지 못한다.</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2008, 최초 실린 곳 : 1959, 출판사: Dichtungen, 프랑크푸르트</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"문학비평",
  "author":"알베르 카뮈 Albert Camus",
  "title":"장 그르니에의 수상집 『섬』 서문 Préface aux Îles de Jean Grenier",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2008,
  "language":"프랑스어",
  "t_title":"장 그르니에의 수상집 『섬』 서문",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","장 그르니에(Jean Grenier)"],
  "keywords2":["문학 비평(Eassais critiques)","장 그르니에(Jean Grenier)","서문(Préface)","섬(Îles)"],
  "story":"<p>카뮈는 본 평론에서 장 그르니에의 『섬』을 소개하고 있다. 그에 따르면 『섬』은 “감각적인 현실을 부정하지는 않으면서도 그와 병행하여 우리의 젊은 불안이 어디서 오는 것인지를 깨달을 수 있도록 또 다른 현실을 보여주”(pp.467-468)는 작품이다. 카뮈는 그르니에의 『섬』을 “프랑스 말에서 새로운 악센트를 이끌어낸 바 있는 샤토브리앙과 바레스”(pp.471-472)에 비교하며 그의 작품은 “그 언어는 빠르게 흐르지만 그 메아리는 긴 여운을 남긴다.”(p.471)라는 평가를 내리며 해당 작품이 자신의 글쓰기에 있어 스승 격의 역할을 했음을 밝힌다. 서문 말미에서 “이 조그만 책을 다시 접어 가슴에 꼭 껴안고, 마침내 아무도 보는 이 없는 곳에 가서 미친 듯이 읽고 싶다는 일념으로 내 방까지 한달음으로 뛰어가던 그날 저녁으로 나는 되돌아가고 싶다. 나는 아무런 회한도 없이, 부러워한다. 오늘 처음으로 이 《섬》을 펼쳐보게 되는 저 낯모르는 젊은이를.”라는 말로 이 작품에 대한 애정을 드러내고 있다.</p>",
  "network1":"-",
  "network2":"<p>『지상의 양식(Les Nourriture Terrestres)』 : 카뮈는 『섬』이 가져온 충격을 지드의 『지상의 양식』의 그것과 비교하고 있다(p.466)</p><p>『섬(Les Îles)』 : 본 작품에서 소개 대상이 되고 있는 그르니에의 작품이다.</p><p>『마디(Mardi)』 : 본문 중 언급되는 멜빌의 작품이다. (p.468)</p>",
  "network3":"<p>『카뮈 그르니에 서한집 1932~1960』, 김화영 역, 책세상, 2012 : 카뮈와 그르니에 사이의 서신을 묶어 출판한 책이다, 두 인물의 관계를 보다 깊이 있게 포착할 수 있는 작품이다.</p><p>장 그르니에, 『카뮈를 추억하며』, 이규현 역, 민음사, 1998 : 카뮈에 대한 그르니에의 글이다. 그르니에의 시각으로 카뮈를 파악할 수 있는 작품이다.</p>",
  "character_network":"<p> 장 그르니에(Jean Grenier) : 본 비평문의 대상인 『섬』의 작가이다.</p><p>멜빌(Herman Melville) : 본문에서 언급되고 있는 마디(Mardi)의 작가이다.</p><p>샤토브리앙(François-René de Chateaubriand), 바레스(Maurice Barrès) : 카뮈는 그르니에를 소개함에 있어 그를 샤토브리앙, 바레스와 비교하고 있다. (pp.471-472)</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2008, 최초 실린 곳 : 1959.1, «Preuves» n°59, 그 후 그르니에 '섬' 2판 서문으로 실림.</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"인터뷰, 짧은 글",
  "author":"알베르 카뮈 Albert Camus",
  "title":"『기슭』지(지중해 문화 잡지) 소개 Rivages. Revue de culture méditerranéenne",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"『기슭』지(지중해 문화 잡지) 소개",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","기슭(Rivages)","지중해(Méditerranée)"],
  "keywords2":["문학 비평(Eassais critiques)","기슭(Rivages)","지중해(Méditerranée)","잡지(Revue)"],
  "story":"<p>카뮈가 1938년 12월 엠마뉴엘 로블레스(Emmanuel Roblès)와 함께 창간한 지중해 문화 잡지인 『기슭』에 대한 소개문이다. 카뮈에 따르면 “하나의 잡지가 태어나는 데는 항상 나름대로 계절이 있는 법”(p.486)이다. 그러나 카뮈가 여기에 소개하는 『기슭』지의 경우 “그 어떤 시사적인 필연성에 부응하겠다는 야심”(p.486)을 가지고 있지 않다. 왜냐하면 이 잡지의 목표는 바로 자기규정이기 때문이다. 『기슭』은 “정답을 제시하려는 것도 아니고 설득하려는 것도 아니고 오직 독자의 마음을 사로잡고자”(p.487)하는 잡지라고 카뮈는 소개하고 있다.</p>",
  "network1":"<p>『결혼』, 『여름』 : 지중해에 대한 카뮈의 애정을 엿볼 수 있는 작품</p><p>『이방인』, 『페스트』 : 지중해 지역을 배경으로 삼는 작품,</p>",
  "network2":"<p>『기슭』지 : 카뮈는 본문에서 해당 잡지에 대해 소개하고 있다.</p>",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    <p>Syvie Petin, 《Albert Camus sur les “Rivages” de la Méditerranée》, conférence au Forum universitaire de l’Ouest-Parisien, 2003.</p> <p><a href='http://www.forumuniversitaire.com/Joumela/index.php/2013-03-05-23-28-06/litterature/litterature-francaise/albert-camus/122-albert-camus-sur-les-rivages-de-la-mediterranee' target='_blank'>카뮈와 지중해의 연관성을 다른 강연 원고</a></p>
  </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2006, 최초 실린 곳 : 1938.12, «Rivages» 첫 호</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"인터뷰, 짧은 글",
  "author":"알베르 카뮈 Albert Camus",
  "title":"“아닙니다, 나는 실존주의자가 아닙니다…”«Non, je ne suis pas existentialiste...» (15 novembre 1945)",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1965,
  "language":"프랑스어",
  "t_title":"아닙니다, 나는 실존주의자가 아닙니다…",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","인터뷰(interview)","실존주의(existentialisme)","부조리(absurde)"],
  "keywords2":["문학 비평(Eassais critiques)","인터뷰(interview)","실존주의(existentialisme)","부조리(absurde)"],
  "story":"<p><b> “아닙니다, 나는 실존주의자가 아닙니다…” (pp.508-512)</b><p>『레 누벨 리테레르Les Nouvelles littéraires』, (1945.11.15.) 인터뷰이다. 카뮈는 본 인터뷰에서 사르트르와 자신의 철학적 입장 차이를 밝히며 자신은 실존주의자가 아님을 명시하고 있다.(p.508) 또한 카뮈는 『시지프 신화』에서 밝힌 바 있는 자신의 부조리 사상에 대해 다음과 같이 추가적 설명을 덧붙인다. “부조리를 받아들이는 것은 하나의 단계요 필요한 경험입니다. 그것이 막다른 골목이 되어서는 안 됩니다.”(P.509) 이처럼 카뮈는 본 인터뷰에서 자신의 사상적 입장을 드러내고 있다.</p>",
  "network1":"<p>문학 비평 – 장 폴 사르트르의 『구토』, 문학 비평 – 장 폴 사르트르의 『벽』<br/>: “아닙니다, 나는 실존주의자가 아닙니다…”의 인터뷰에 드러난 카뮈의 사상적 견해를, 그 자신이 기록한 사르트르에 대한 평가를 통해 좀 더 면밀하게 살펴 볼 수 있다.</p><p>『시지프 신화』 : 본 인터뷰에서 카뮈는 해당 저서가 자신의 유일한 사상적 책임을 밝히고 있다(P.508). 따라서 『시지프 신화』를 통해 카뮈의 사상을 탐구하는 일이 가능하다.</p><br/><p>『칼리굴라』 : 『세르베르』지와의 인터뷰에서 언급된 카뮈의 희곡 작품,</p>",
  "network2":"장 폴 사르트르, 『실존주의는 휴며니즘이다』, 방곤 역, 문예, 2013.<br/>: 사르트르의 실존주의 사상에 대한 단행본으로 “아닙니다, 나는 실존주의자가 아닙니다…”의 인터뷰에서 언급된 사르트르의 실존주의 사상을 살펴 볼 수 있다.</p>",
  "network3":"-",
  "character_network":"<p>사르트르 : 『세르베르』지와의 인터뷰에서 카뮈와 비교되는 사상가이다.</p>",
  "social_network":`<div>
    <p>이기언, 「까뮈와 실존철학」, 『지중해지역연구』, 제7권 제1호(2005,4). : 철학자와 작가로서의 카뮈의 정체성에 관한 논문이다.</p>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 1965, 최초 실린 곳 : 1945.11.15., «Les Nouvelles Littéraires» n° 954</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"인터뷰, 짧은 글",
  "author":"알베르 카뮈 Albert Camus",
  "title":"『세르비르』지와의 인터뷰 Interview à «Servir» (20 décembre 1945)",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1965,
  "language":"프랑스어",
  "t_title":"『세르베르』지와의 인터뷰 ",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","인터뷰(interview)","실존주의(existentialisme)","부조리(absurde)"],
  "keywords2":["문학 비평(Eassais critiques)","인터뷰(interview)","실존주의(existentialisme)","부조리(absurde)","세르비르지(Servir)"],
  "story":"<p><b>『세르비르』지와의 인터뷰 (PP.513-517)</b></p><p>『세르비르』(1945.12.20.) 인터뷰이다. 본 인터뷰에서 카뮈는 다양한 질문들에 답하고 있다. 카뮈의 실존주의에 대한 문답과 프랑스의 아랍 정책에 대한 문답. 카뮈의 정치적 입장과 그의 희곡 작품 『칼리굴라』에 대한 평론가들의 견해에 대한 답변 그리고 당대의 세계 정치, 윤리적 현상에 대한 진단이 수록되어 있다.</p>",
  "network1":"<p>문학 비평 – 장 폴 사르트르의 『구토』, 문학 비평 – 장 폴 사르트르의 『벽』<br/>: “아닙니다, 나는 실존주의자가 아닙니다…”의 인터뷰에 드러난 카뮈의 사상적 견해를, 그 자신이 기록한 사르트르에 대한 평가를 통해 좀 더 면밀하게 살펴 볼 수 있다.</p><p>『시지프 신화』 : 본 인터뷰에서 카뮈는 해당 저서가 자신의 유일한 사상적 책임을 밝히고 있다(P.508). 따라서 『시지프 신화』를 통해 카뮈의 사상을 탐구하는 일이 가능하다.</p><br/><p>『칼리굴라』 : 『세르비르』지와의 인터뷰에서 언급된 카뮈의 희곡 작품,</p>",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>사르트르 : 『세르비르』지와의 인터뷰에서 카뮈와 비교되는 사상가이다.</p>",
  "social_network":`<div>
    <p>이기언, 「까뮈와 실존철학」, 『지중해지역연구』, 제7권 제1호(2005,4). : 철학자와 작가로서의 카뮈의 정체성에 관한 논문이다.</p>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 1965, 최초 실린 곳 : 1945.12.20, «Servir»</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"인터뷰, 짧은 글",
  "author":"알베르 카뮈 Albert Camus",
  "title":"시몬 베유, 『뿌리내리기』 L'Enracinement de Simone Weil (juin 1949)",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2008,
  "language":"프랑스어",
  "t_title":"시몬 베유, 『뿌리내리기』",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","시몬 베유(Simone Weil)"],
  "keywords2":["문학 비평(Eassais critiques)","시몬 베유(Simone Weil)","뿌리내리기(Enracinement)"],
  "story":"<p>시몬 베유의 저작 『뿌리내리기』를 소개하는 글이다. 『뿌리내리기』는 독일 점령 기간 동안 런던 정부가 시몬 베유에게 프랑스 재건 가능성에 대한 보고서를 작성하도록 의뢰했는데, 거기에 맞춰 작성된 보고서를 책으로 펴낸 저작이다. 카뮈는 이 작품에 대해 우리가 생각하고 판단하고 처신하는 방식에 대해 가장 섬세하고 밀도 있게 매서운 비판을 가하고“(p.571) 그 다음에 ”뿌리내리기“라는 제목하에 프랑스를 재건할 수 있는 비책을 우리에게 전한다며(p.571) 이 책을 소개한다.</p>",
  "network1":"-",
  "network2":"<p>『뿌리내리기』 : 「시몬 베유, 『뿌리내리기』」와 「서문 초안」에서 소개되고 있는 작품</p>",
  "network3":"-",
  "character_network":"<p>시몬 베유(Simone Weil) : 「시몬 베유, 『뿌리내리기』」에서 소개대상이 되고 있는 『뿌리내리기』의 작가</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2008, 최초 실린 곳 : 1949.6, «Bulletin de la N.R.F.»</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"인터뷰, 짧은 글",
  "author":"알베르 카뮈 Albert Camus",
  "title":"나는 왜 연극을 하는가 Albert Camus : 《Pourquoi Je fais du théâtre》",
  "location":"프랑스 파리 Paris, France",
  "publisher":"플레이아드",
  "published_year":2006,
  "language":"프랑스어",
  "t_title":"나는 왜 연극을 하는가",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","연극(Théâtre)","행복(bonheur)","자유(liberté)","연대의식(solidarité)"],
  "keywords2":["문학 비평(Eassais critiques)","연극(Théâtre)","행복(bonheur)","자유(liberté)","연대의식(solidarité)"],
  "story":"<p>1959년 5월 12일 텔레비전의 &lt;클로즈업Gros plan&gt;이라는 프로에서 카뮈가 한 말을 발췌하여 실은 글이다. 카뮈가 자신의 연극 활동의 이유를 밝힌 발췌문이다. 그는 이 점에 있어 우선 “연극의 무대는 내가 행복을 느낄 수 있는 유일한 장소이기 때문”(p.540)이라 답하고 있다. 카뮈에 따르면 당대의 사회적 분위기는 “행복이란 형법상의 범죄와 같은 것이어서 절대로 자백을 해서는 안 되는 것”(p.541)이었다. 왜냐하면 “나는 행복합니다”라는 발언은 주위로부터 “카슈미르 지방의 고아나 뉴헤브리디스의 나병 환자”와 같은 불행한 자들에 대한 방임으로 여겨지기 때문이었다. 그러나 카뮈는 “불행에 처한 사람들을 제대로 도와주기 위해서는 자기 자신이 먼저 튼튼하고 행복해져야 한다”(p.541)라고 주장한다. 따라서 카뮈는 그 자신이 행복감을 느끼는 연극의 무대를 떠날 수 없다고 이야기하는데 거기에는 세간의 관심으로부터 벗어날 수 있는 자유(p.543)가 있으며 또한 지성 사회의 따분한 분위기로부터의 자유(pp.544-545)가 있기 때문이다. 뿐만 아니라 연극의 무대에는 “공동의 모험, 모두가 다 알고 있는 위험이 남자들과 여자들로 이루어진 하나의 공동체를 만들어내는”(pp.546-547) 연대의식이 자리잡고 있다. 또한 카뮈는 “진실로, 내게 있는 이 얼마 안 되는 윤리의식은 축구 경기장과 연극 무대에서 배운 것입니다. 그곳들은 앞으로도 나의 진정한 대학교로 남을 것입니다.”(p.547)라는 말을 하며 오랜 시간 훈련을 하면서 희망과 연대 의식을 갖게 된다는 점에서 축구장과 연극무대가 공통되게 깨달음의 장소임을 밝히기도 한다. 카뮈는 본 녹취록에서 이상과 같이 자신의 연극 활동 이유를 이야기하고 있다.</p>",
  "network1":`<div>
    <p>『칼리굴라』, 『오해』, 『계엄령』, 『정의의 사람들』, : 극작가로서 카뮈가 쓴 희곡들이다.</p><p>다음은 카뮈가 번안, 연출한 희곡들이다.</p>
    <ul>
      <li>『꼬리잡힌 욕망Le Désir attrapé par la queue』 : 6막으로 된 파블로 피카소(Pablo Picasso)의 초현실주의 희곡 『꼬리잡힌 욕망』(1941년)을 1944년 작가 미셸 레리스(Michel Leiris)의 집에서 낭독 형식으로 공연할 때 연출을 맡았다. 이 작품은 1967년 생 트로페 자유표현축제(festival de la Libre expression à Saint-Tropez) 무대에서 초연된다.</li>
      <li>『유령소동Les esprits』 : 피에르 드 라리베(Pierre de Larivey)의 3막 희극작품 『유령소동』(1579년)을 1953년 번안하고 연출하여 앙제 축제(festival d'Angers) 무대에 올렸다.</li>
      <li>『십자가의 신앙La Dévotion de la croix』 : 스페인 작가 칼데론 데 라 바르카(Pedro Calderón de la Barca)의 희곡 작품 『십자가의 신앙』(1634년)을 1953년 번안하였다. 이 작품은 마르셀 에랑(Marcel Herrand)의 연출로 같은 해 앙제 축제(festival d'Angers) 무대에서 공연된다.</li>
      <li>『흥미있는 경우Un cas intéressant』 : 이탈리아 작가 디노 부차티(Dino Buzzati)가 자신의 단편소설을 희곡으로 다시 쓴(2부, 11배경) 동명 작품(1953년)을 카뮈가 1955년 번안하였다. 이 작품은 조르주 비탈리(Georges Vitaly)의 연출로 같은 해 라 브뤼에르(La Bruyère) 극장에서 상연되었다.</li>
      <li>『어느 수녀를 위한 진혼곡Requiem pour une nonne 』 : 미국 작가 윌리엄 포크너(William Faulkner)의 동명 소설(1951년)을 카뮈가 1956년 2부, 7배경의 희곡으로 각색, 연출하여 마튀렝 극장(Théâtre des Mathurins)에서 공연하였다.</li>
      <li>『올메도의 기사Le Chevalier d'Olmedo』 : 스페인 시인, 극작가 로페 데 베가(Lope de Vega)의 동명 희비극(1620-1625년)을 1957년 번역, 각색, 연출하여 앙제 축제(festival d'Angers) 무대에서 공연하였다.</li>
      <li>『악령Les Possédés』 : 도스토예프스키(Fiodor Dostoïevski)의 이 장편소설(1871년)을 카뮈가 1959년 2부의 희곡으로 각색, 연출하여 앙투완 극장(Théâtre Antoine)에서 공연하였다.</li>
    </ul>
  </div>`,
  "network2":`<div>
    <ul>
      <li>『꼬리잡힌 욕망Le Désir attrapé par la queue』(1941년) : 파블로 피카소의 초현실주의 희곡으로 카뮈는 1944년 이 작품의 낭독 공연을 연출하였다.</li>
      <li>『유령소동Les esprits』(1579년) : 르네상스 시기 프랑스의 극작가 피에르 드 라리베의 작품이다. 카뮈는 이 작품을 1953년 번안, 연출하였다.</li>
      <li>『십자가의 신앙La Dévotion de la croix』(1634년) : 17세기 스페인의 극작가 페드로 칼데론 데 라 바르카의 작품이다. 카뮈는 1953년 이 작품을 번안하였다.</li>
      <li>『흥미있는 경우Un cas intéressant』(1953년) : 이탈리아의 작가 디노 부차티의 작품으로 카뮈는 1955년 이 작품을 번안하였다.</li>
      <li>『어느 수녀를 위한 진혼곡Requiem pour une nonne』(1951년) : 미국의 작가 윌리엄 포크너의 소설로 카뮈는 이 작품을 1956년 희곡으로 번안, 각색하였다.</li>
      <li>『올메도의 기사Le Chevalier d'Olmedo』(1620-1625년) : 르네상스 시기 스페인의 극작가 로페 데 베가의 작품이다. 카뮈는 이 작품을 1957년 번역, 각색, 연출한 바 있다.</li>
      <li>『악령Les Possédés』(1871년) : 러시아 대문호 도스토예프스키의 장편소설. 카뮈는 스무살 때 이 작품에서 감명을 받고 1959년 이 작품을 희곡으로 각색한 후 연출한 바 있다.</li>
    </ul>
 </div>`,
  "network3":"<p>1972년 알베르 카뮈가 연출하고 각색한 &lt;악령&gt;이 장 메르퀴르(Jean Mercure) 연출로 파리시립극장(Théâtre de la Ville de Paris)에서 재공연됨.</p>",
  "character_network":`<div>
    <ul>
      <li>파블로 피카소 : 카뮈는 파블로 피카소의 희곡 『꼬리잡힌 욕망Le Désir attrapé par la queue』의 낭독 공연을 연출한 바 있다.</li>
      <li>피에르 드 라리베 : 카뮈는 피에르 드 라리베의 희곡 『유령소동Les esprits』을 번안, 연출한 바 있다.</li>
      <li>칼데론 데 라 바르카 : 카뮈는 칼데론 데 라 바르카의 희곡 『십자가의 신앙La Dévotion de la croix』을 번안한 바 있다.</li>
      <li>디노 부차티 : 카뮈는 디노 부차티의 『흥미있는 경우Un cas intéressant』를 번안한 바 있다.</li>
      <li>윌리엄 포크너 : 카뮈는 윌리엄 포크너의 『어느 수녀를 위한 진혼곡Requiem pour une nonne』를 희곡으로 각색, 연출한 바 있다.</li>
      <li>로페 데 베가 : 카뮈는 로페 데 베가의 『올메도의 기사Le Chevalier d'Olmedo』를 번역, 각색, 연출한 바 있다.</li>
      <li>도스토예프스키 : 카뮈는 도스토예프스키의 『악령Les Possédés』을 희곡으로 각색, 연출한 바 있다.</li>
    </ul>
  </div>`,
  "social_network":`<div>
    <p><a href='http://www.regietheatrale.com/index/index/thematiques/auteurs/Camus/albert-camus-6.html' target='_blank'>연극에 관한 카뮈의 생각을 인용하면서 연출가로서의 카뮈에 관해 소개한 글.</a></p>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://education.francetv.fr/matiere/litterature/premiere/video/albert-camus-pourquoi-je-fais-du-theatre' target='_blank'>본 발췌록의 원본이 된 tv 프로그램의 동영상을 볼 수 있는 웹페이지. 여배우 카트린느 셀러(Catherine Sellers)가 해설함</a></p>
    <p><a href='http://lastrolabe44.over-blog.com/albert-camus-pourquoi-j-aime-le-theatre' target='_blank'>이 인터뷰의 텍스트(스크립트)와 연극 관련 카뮈의 사진이 나오는 웹 페이지.</a></p>
  </div>`,
  "legends":"<p>플레이아드 초판 2006, 최초 실린 곳 : 1959.5.12., TV 프로그램 &lt;Gros Plan&gt; 인터뷰</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"인터뷰, 짧은 글",
  "author":"알베르 카뮈 Albert Camus",
  "title":"장 클로드 브리스빌에게 답하다 Réponses à Jean-Claude Brisville",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2008,
  "language":"프랑스어",
  "t_title":"장클로드 브리스빌에게 답하다",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","장 클로드 브리스빌(Jean-Claude Brisville)"],
  "keywords2":["문학 비평(Eassais critiques)","장 클로드 브리스빌(Jean-Claude Brisville)","인터뷰(interview)"],
  "story":"<p><b>장 클로드 브리스빌에게 답하다</b></p><p>쥘라아르 출판사 편집위원 겸 비평가, 소설가인 장 클로드 브리스빌이 출판한 카뮈에 대한 저서 『카뮈』(Gallimard, 1959)에 수록된 인터뷰이다. 카뮈가 직업으로 작가를 선택한 이유와 작품 창작 당시의 감정, 사생활, 작품 창작 과정 중의 습관 등에 대한 인터뷰가 담겨있다.</p>",
  "network1":"-",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>장 클로드 브리스빌(Jean-Claude Brisville) : 「장 클로드 브리스빌에게 답한다」의 편저자</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2008, 최초 출간일 1959, 실린 곳 : «La bibiliothèque idéale» 2e trimestre</p>"
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"인터뷰, 짧은 글",
  "author":"알베르 카뮈 Albert Camus",
  "title":"알베르 카뮈의 마지막 인터뷰 Dernière Interview (décembre 1959)",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2008,
  "language":"프랑스어",
  "t_title":"알베르 카뮈의 마지막 인터뷰",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","인터뷰(interview)"],
  "keywords2":["문학 비평(Eassais critiques)","인터뷰(interview)","실존주의(existentialisme)"],
  "story":"<p><b>알베르 카뮈의 마지막 인터뷰</b><p>1959년 12월 20일의 인터뷰이다. 카뮈의 작품에 대한 문답과 당대의 정치적 상황에 문답, 그리고 실존주의 사상에 대한 입장과 카뮈가 즐겨 읽는 작품들에 대한 문답이 수록되어 있다.</p>",
  "network1":"-",
  "network2":"-",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"nonfiction",
  "category":"인터뷰, 짧은 글",
  "author":"알베르 카뮈 Albert Camus",
  "title":"우리의 친구 로블레스 Notre ami Roblès",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":2008,
  "language":"프랑스어",
  "t_title":"우리의 친구 로블레스",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["문학 비평(Eassais critiques)","엠마뉘엘 로블레스(Emmanuel Roblès)"],
  "keywords2":["문학 비평(Eassais critiques)","엠마뉘엘 로블레스(Emmanuel Roblès)"],
  "story":"<p><b>우리의 친구 로블레스</b></p><p>알제리 출신의 작가 엠마뉘엘 로블레스에 대한 소개문이다. 카뮈에 따르면 그는 “때로는 섬세하고 때로는 꾸밈이 없는 야성미로 인하여 차별화되는 독특한 작품”(p.553)을 써내려갔으며 “오늘날 프랑스에서 당당하게 인정받으면서 모든 종족의 결합체인 알제리 사람들을 충실하게 대표”(p.554)하는 작가이다.</p>",
  "network1":"-",
  "network2":"-",
  "network3":"-",
  "character_network":"<p>엠마뉘엘 로블레스(Emmanuel Roblès) : 「우리의 친구 로블레스」에서 소개되고 있는 작가</p>",
  "social_network":`<div>
   </div>`,
  "digital_works":`<div>
  </div>`,
  "legends":"<p>갈리마르 초판 2008, 최초 출간일 1959.12, 실린 곳 : 알제리 오랑에서 발간된 «Simoun»의 특집호 n° 30 «Pour saluer Roblès»의 첫 글</p>"
},{ /*********************************************************************************************/
    /** 사적인 글 **/
    /*********************************************************************************************/
  "worktype":"private",
  "category":"편지",
  "author":"알베르 카뮈 Albert Camus",
  "title":"카뮈-그르니에 서한집 Correspondance : [Albert Camus, Jean Grenier] : 1932-1960",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1981,
  "language":"프랑스어",
  "t_title":"카뮈-그르니에 서한집 1932~1960",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2012,
  "keywords1":["장 그르니에(Jean Grenier)","서간집(Correspondace)"],
  "keywords2":["문학 비평(Eassais critiques)","장 그르니에(Jean Grenier)","서문(Préface)","섬(Îles)"],
  "story":"<p>카뮈가 알제 그랑리세(중고등학교)시절 자신의 철학교사였던 장 그르니에와 1932년부터 사망하던 해인 1960년까지 주고 받은 서한집을 엮은 책이다. 카뮈는 각별한 스승이었던 그르니에와 끊임없이 서신을 주고 받았는데 그들이 주고받은 편지는 235통에 달했다. 이 중 카뮈가 112통을 그르니에가 123통의 편지를 보냈다. 그들은 신변잡기적 이야기에서부터 작품에 관한 논의 등 다양한 내용을 주제로 서신을 교환하였다. 카뮈는 특히 자신에게 중요한 문제들이 닥쳐왔을 때 스승인 그르니에에게 자문을 구하는 편지들을 보내기도 하였는데 가령 1933년 7월 13일의 편지에서 카뮈는 이모부와의 불화와 건강 악화로 인한 학업 지속의 불확실성과 같은 문제들에 대해 상담을 요청하기도 하였다. 그러나 그들의 서신은 단순한 고민 상담의 문제로만 그치지 않았다. 카뮈는 1935년 8월 21편지에서 “공산당에 입당하라고 충고하신 선생님의 듯을 잘 알겠습니다. 발레아르 군도에서 돌아온 다음 그렇게 하겠습니다.”라고 이야기하며 자신의 인생에 있어 중요한 결정이 될 공산당 입당 결정이 그르니에와의 서신에서 영향 받았음을 언급하고 있다. 또한 카뮈는 이 서신 활동을 통해 편지를 쓰던 당시 자신이 집필 중인 작품에 대한 경과 보고 등을 그르니에에게 끊임없이 알렸다.</p>",
  "network1":`<div>
    <ul>
      <li>「장 클로드 그리스빌에게 답하다」 : 카뮈는 1932년 5월 20일 그르니에에게 보낸 편지에서 자신의 목표였던 작가가 되기 위한 문제에 대해 그르니에에게 자문을 구한 바 있다. 이 내용은 해당 에세이에서도 “나는 열일곱 살쯤에 작가가 되고 싶다는 새각을 했지요. 도 그와 동시에 막연하게나마 그렇게 되리라는 것을 알았지요”라는 언급에 나타나고 있다.</li>
      <li>「앙드레 지드와의 만남」 : 까뮈는 지드의 작품을 읽으며 많은 영향을 받은 바 있다. 이는 여러 통의 편지에서도 드러나고 있는데 1932년 5월 20일의 편지에서는 “저는 여전히 그를 다른 그 어떤 작가보다도 좋아합니다.”라며 노골적으로 지드에 대한 관심을 언급한 바 있다. 「앙드레 지드와의 만남」에서 카뮈는 지드의 작품을 읽었던 16살 시절의 회상에서 시작해 후일 파리에서 지드의 이웃집에 살며 겪은 일들을 술회하며 그를 추모한다.</li>
      <li>「장 그르니에의 수상집 『섬』 서문」 : 카뮈는 그르니에에게 보내는 편지에서 그의 작품 『섬』을 읽고 느낀 메모를 보낸 바 있다. 후일 이 작품에 대해 카뮈는 정식 서문을 작성하게 된다.</li>
      <li>「가난한 동네의 목소리들」 : 젊은 시절의 글 Le premier Camus suivi de Écrits de jeunesse d'Albert Camus (Cahiers Albert Camus Tome II:)에 수록된 카뮈의 에세이다 카뮈는 1934년 8월 17일 편지에서 이 작품의 구상 완성에 대한 이야기를 언급하고 있다.</li>
      <li>『안과 겉』 :1937년 편지에서 카뮈는 이 에세이집을 그르니에에게 헌정하고자 편지를 남겼다.</li>
      <li>『행복한 죽음』 : 1938년 6월 18일 편지에서 카뮈는 이 소설의 집필의 어려움을 토로한 편지를 남겼다.</li>
      <li>『결혼』 : 카뮈는 1938년 말 보낸 편지에서 이 에세이 작품에 대한 그르니에의 견해를 물었다.</li>
      <li>『시지프 신화』 : 카뮈는 1939년 2월 2일 편지에서 부조리에 관한 에세이를 쓰고 있다 밝히는데 이는 『시지프 신화』가 될 것이다.</li>
      <li>『이방인』 : 1939년 2월 2일 편지에는 이 작품의 구상이 시작되었음이 담겨 있다.</li>
      <li>『칼리굴라』 : 카뮈는 1939년 2월 2일 편지에서 칼리굴라 집필을 해당 년도내로 완료 하겠다 적고 있다.</li>
      <li>「루이 기유의 『민중의 집』 서문」 : 카뮈는 그르니에의 소개로 그의 친구인 루이 기유와 알게 되었는데 이 인연으로 인해 카뮈는 후일 기유의 작품 『민중의 집』의 서문을 작성하게 된다.</li>
      <li>『페스트』 :까뮈는 1942년 9월 10일 편지에서 해당 작품을 집필 중임을 언급하였다.</li>
      <li>「프랑시스 퐁주의 시집 『사물의 편』에 대한 편지」 : 1942년 9월 10일 편지에서 카뮈는 퐁주에 대한 언급을 한다. 카뮈와 퐁주는 개인적 친분이 있어 정기적인 서신 교환을 하기도 했는데 후일 카뮈는 퐁주의 시집 『사물의 편』에 대한 글을 발표하게 된다.</li>
      <li>『반항하는 인간』 : 1942년 9월 22일 편지에서 카뮈는 장차 이 작품이 될 자신의 집필 과정에 대해 언급하고 있다.</li>
      <li>『오해』 : 카뮈의 희곡으로 카뮈는 이 작품의 손질 과정에 대해 1943년 7월 17일 편지에서 언급하고 있다.</li>
      <li>『독일 친구에게 보내는 편지』 : 네 개의 편지로 이루어진 『독일 친구에게 보내는 편지』는 실존인물에게 보내는 편지가 아닌 상상의 편지라는 문학적 형식을 갖고 있다. 독일의 프랑스 점령 시기에 카뮈가 비밀리에 레지스탕스 신문을 위해 쓴 글로 그르니에는 1947년 1월 7일 카뮈에게 보내는 편지에서 이 글에 대한 생각을 적어두었다.</li>
      <li>『르네 샤르』 : 카뮈는 친분이 있던 프랑스 시인 르네 샤르의 시집의 서문을 작성하였다.</li>
      <li>『계염렁』, 『정의의 사람들』 : 카뮈의 희곡으로 카뮈는 1948년 1월 21일 그르니에에게 보내는 편지에서 이 작품들의 집필에 대해 언급했다.</li>
      <li>「앙드레 지드와의 만남」 : 앙드레 지드에 대한 추모문이다. 카뮈는 그르니에에게 보내는 1951년 2월 25일 편지에서 지드의 사망 소식으로 인한 충격을 털어놓고 있다.</li>
      <li>『적지와 왕국』 : 카뮈의 소설집으로 그르니에는 1955년 4월 25일 카뮈에게 보내는 편지에서 여기에 실리게 될 「간부」의 감상을 적어두었다.</li>
      <li>『전락』 : 카뮈는 1955년 8월 24일 그르니에에게 보내는 편지에서 중편소설인 이 작품의 집필에 대해 언급했다.</li>
      <li>『올메도의 기사』 :  로페 데 베가의 원작을 카뮈가 번역 연출한 희곡으로 1957년 6월 21일 앙제 연극제에서 초연되었다. 그르니에가 카뮈에게 보내는 1957년 8월 5일 편지에서 언급된다.</li>
      <li>「시몬 베유, 『뿌리내리기』」 : 시몬 베유의 『뿌리내리기』에 대한 카뮈의 비평문이다. 카뮈는 1957년 8월 그르니에에게 보낸 편지에서 시몬 베유에 대한 언급을 했다.</li>
      <li>『악령Les Possédés』 : 카뮈는 도스토예프스키의 소설 『악령』을 각색하여 1959년 무대에 올렸다. 그르니에는 1959년 10월 30일 카뮈에게 보내는 편지에서 이에 대한 감상을 남겼다.</li>
      <li>「“아닙니다, 나는 실존주의자가 아닙니다…”」 : 《레 누벨 리테레르Les Nouvelles littéraires》, 1945.11.15. 인터뷰이다. 카뮈는 여기에서 『시지프 신화』에서 밝힌 바 있는 자신의 부조리 사상에 대해 설명한다.</li>
    </ul>
  </div>`,
  "network2":`<div>
    <ul>
      <li>『섬』 : 장 그르니에의 산문집으로 카뮈는 그르니에에게 보내는 편지에서 그의 작품 『섬』을 읽고 느낀 메모를 보낸 바 있다. 후일 이 작품에 대해 카뮈는 정식 서문을 작성하게 된다.</li>
      <li>「루르마랭의 예지」 : 장 그르니에가 발표한 에세이로 1936년 7월 26일 편지에서 카뮈는 이 작품에 대한 견해를 늘어놓고 있다.</li>
      <li>『네모난 원』 : 장 그르니에가 1938~1939 사이에 《N.R.F.》지에 기고한 다양한 주제에 관한 짧은 글이다. 카뮈는 편지에서 이 작품에 대한 공감을 나타낸 바 있다.</li>
      <li>「쿰 아파루에리트」 : 장 그르니에가 1930년 『루르마랭의 테라스』에 발표한 글로 카뮈는 이 작품의 감상을 짤막하게 편지에 남겼다.</li>
      <li>『지중해에서 얻은 영감』 : 장 그르니에의 작품으로 카뮈는 편지에서 이에 대한 짧막한 의견을 남겼다.</li>
      <li>『선택』 :그르니에의 저서로 그르니에는 카뮈에게 보내는 편지에서 칼리굴라에 대한 비평 도중 이 작품에 대한 언급을 한다.</li>
      <li>「그리스 묘비명」 : 그르니에가 1941년 5월 《퐁텐》지에 기고한 글로 카뮈는 이 글에 대해 편지에서 언급하고 있다.</li>
      <li>「미노타우로스 또는 오랑에서 잠시 Le minotaure ou la halte d'Oran」 : 카뮈는 1941년 오랑에서 보낸 편지에서 그 곳의 생활에 관해 언급을 하고 있는데 이런 감회는 해당 에세이로 이어지게 된다.</li>
      <li>『푸제 씨의 초상Portrait de M. Pouget』 : 장 기통의 작품으로 카뮈는 그르니에의 요청으로 이 작품의 서평을 작성하였다.</li>
      <li>『플라톤의 로고스에 대한 시론Essai sur le logos platonicien』 : 브리스 파랭의 저서로 카뮈는 그르니에에게 보내는 편지에서 이 저서에 대한 관심을 드러냈다.</li>
      <li>『단장의 아픔Crève-cœur』 : 루이 아라공의 작품으로 그르니에는 카뮈에게 보내는 편지에서 이 작품의 감상을 짧막히 적고 있다.</li>
      <li>『성유물Relique』 : 그르니에는 카뮈에게 보내는 편지에서 에사 데 케이로스(José Maria de Eça de Queirós)의 이 작품을 읽을 것을 강력 추천하고 있다.</li>
      <li>『꿈속의 빵Pain des rêves』 : 루이 기유의 작품으로 카뮈는 그르니에에게 보내는 편지에서 이 작품의 서평을 비난하고 있다.</li>
      <li>「한 인간, 한 작품」 : 《레 카이에 뒤 쉬드》(1943년 2월)에 실린 글. 그르니에가『이방인』에 대해 기고한 글이다.</li>
      <li>『어린아이들처럼Pareils à des enfants』 : 마르크 베르나르의 작품으로 그르니에는 카뮈에게 보내는 편지에서 이 책을 읽어 볼 것을 권하고 있다.</li>
      <li>「부조리의 감정」 : 《코메디아》지에 1942년 11월 21일자로 게재된 글로 그르니에가 『시지프신화』에 대하여 작성한 글이다.</li>
      <li>「『이방인』해설」 : 『이방인』에 대한 사르트르의 글이다. 《레 카이에 뒤 쉬드》에 1943년 2월 실렸다.</li>
      <li>『내적경험Expérience intérieure』 : 조르주 바타이유의 저서로 그르니에는 카뮈에게 이 책을 받아 읽어볼 것을 권했다.</li>
      <li>『어휘집Lexique』 ; 그르니에의 저서로 카뮈는 편지에서 이 작품에 대해 언급하고 있다.</li>
      <li>『섹스투스 엠페리쿠스』 : 장 그르니에와 주느비에브 고롱의 역서다. 카뮈는 편지에서 이 작품에 대해 언급하고 있다.</li>
      <li>『모래톱Les Grèves』 : 장 그르니에의 저서로 카뮈의 편지에서 언급되고 있다.</li>
      <li>『인간적인 것에 관하여A propos de l'humain』 : 그르니에의 저서로 그르니에는 카뮈에게 보내는 편지에서 이 책에 실릴 작품들의 게재목록에 대한 충고를 묻고 있다.</li>
      <li>『카뮈를 추억하며』 : 그르니에의 저서로 카뮈에 대한 회고가 담겨있는 책이다.</li>
    </ul>
  </div>`,
  "network3":"-",
  "character_network":`<div>
    <ul>
      <li>장 그르니에(Jean Grenier) : 알제 그랑리세 시절 카뮈의 철학교사였으며 본 서한집에서 카뮈가 지속적으로 편지를 주고받는 대상이다. 카뮈는 스승인 그르니에와 긴밀한 관계를 일생동안 유지했으며 여러 난관에 봉착할 때마다 그르니에의 의견을 묻는 편지를 보낸 바 있다.</li>
      <li>장 폴 사르트르(Jean Paul Sartre) : 프랑스의 작가이자 철학자. 실존주의 사상의 대표적 거장인 사르트르는 카뮈와 오랜 기간 교류를 유지했다.</li>
      <li>앙드레 지드(André Gide) : 20세기 프랑스의 소설가였다. 카뮈가 큰 흥미를 가진 작가였으며 후일 카뮈는 그의 이웃집에 살았던 경험과 그의 작품을 읽었던 사실들을 토대로 추모문 「앙드레 지드와의 만남」을 작성하였다.</li>
      <li>막스 자콥(Max Jacob) : 유태계 프랑스인 시인으로 카뮈와 편지를 주고 받았던 인물이다. 1932년 8월 25일 편지에서 카뮈는 이에 대해 언급한다.</li>
      <li>에드몽 샤를로(Edmon Charlot) : 장 그르니에의 제자로 고등학교를 졸업하고 서점을 열었다. 그르니에는 샤를로에게 지중해에서 영감을 받은 작품들을 전문 출판하는 것을 제안한 바 있는데 이로 인해 샤를로 출판사가 탄생하였다. 카뮈는 그르니에의 소개로 샤를로를 알게 되어 편집자문 역을 맡게 되었는데 이 인연으로 ‘지중해 문화에 관한 잡지’ 《리바주》와 ‘지중해 총서’를 기획하는데 일조하였다.</li>
      <li>파스칼 피아(Pascal Pia) : 아폴리네르 전문가인 피아는 아랍인의 권익 옹호에 앞장선 「알제 레퓌블리캥 Alger Républicain」을 창간했다 카뮈는 이 신문사에 신문기자로 취직하였다.</li>
      <li>라셸 베스팔로프(Rachel Bespaloff) : 불가리 태생의 미국 작가이자 철학자 그르니에의 지인이었으며 카뮈는 그르니에가 보내 준 베스팔로프의 책에 대한 감사인사를 편지로 보냈다.</li>
      <li>장 기통(Jean Guitton) : 프랑스의 철학자이자 작가이다. 카뮈는 그르니에의 요청으로 그의 작품 『푸제 씨의 초상Portrait de M. Pouget』의 서평을 작성하였다.</li>
      <li>브리스 파랭(Brice Parain) : 프랑스의 언어철학자, 에세이스트, 소설가이다. 장 그르니에와 카뮈의 가까운 친구였으며 카뮈는 『작가수첩2』에서 그의 말을 자주 인용했다. 그르니에는 카뮈에게 보낸 편지에서 파랭이 카뮈의 책을 몹시 좋아함을 알렸다.</li>
      <li>마르셀 아를랑(Marcel Arland) : 프랑스의 작가이자 문학비평가이다. 주간지 《코메디아》에 〈한 작가의 탄생, 알베르 카뮈〉(1942년 7월 11일자)라는 글을 기고하였다. 그르니에는 카뮈에게 보내는 편지에서 이 점을 알리고 있다.</li>
      <li>루이 기유(Louis Guilloux) : 프랑스의 소설가로 장 그르니에의 친구다. 그르니에의 소개로 카뮈와 알게 되었다. 카뮈는 후일 기유의 『민중의 집』 서문을 작성하였다.</li>
      <li>마르크 베르나르(Marc Bernard) : 프랑스의 작가이자 공쿠르 상 수상자이다. 그르니에가 카뮈에게 보내는 편지에서 언급된다.</li>
      <li>장 블랑자(Jean Blanzat) : 프랑스의 소설가이다. 그르니에가 카뮈에게 보내는 편지에서 언급된다.</li>
      <li>가스통 바슐라르(Gaston Bachelard) : 프랑스의 철학자이다. 그르니에가 카뮈에게 보내는 편지에서 언급된다.</li>
      <li>에사 데 케이로스(José Maria de Eça de Queirós) : 포르투갈의 작가이다. 그르네에는 카뮈에게 보내는 편지에서 그의 작품 『성유물Relique』을 읽어볼 것을 강력히 권하고 있다.</li>
      <li>프랑시스 퐁주(Francis Ponge) : 프랑스의 시인이다. 카뮈는 퐁주를 개인적으로 알고 있었는데 이 두 작가 사이에는 규칙적인 서신 교환이 있었다.</li>
      <li>조르쥬 바타이유(Georges Bataille) ; 프랑스의 작가이자 철학자, 인류학자. 그르니에는 카뮈에게 보내는 편지에서 바타이유의 『내적경험Expérience intérieure』을 읽어볼 것을 권하고 있다</li>
      <li>르네 샤르(René Char) : 프랑스의 시인이다. 카뮈는 갈리마르 출판사에서 르네 샤르와 알게 되었는데 1946년 9월 르네 샤르로부터 그의 고향으로 초대를 받아 대접 받는 등 그들은 깊은 친분을 유지했다.</li>
      <li>시몬 베유(Simone Weil) : 프랑스의 사상가이다. 카뮈가 그르니에에게 보내는 편지에서 언급하고 있다.</li>
      <li>알렉상드르 뒤마(Alexandre Dumas) : 프랑스의 극작가이자 소설가이다. 카뮈는 그르니에에게 보내는 편지에서 뒤마의 작품들을 읽고 있음을 이야기했다.</li>
    </ul>
  </div>`,
  "social_network":`<div>
    <ul>
      <li>이소연, 「알베르 카뮈의 『안과 겉』에서 나타난 지중해적 요소 연구」 (2008), 연세대학교.</li>
      <li>이경해, 「Camus의, Noces, L'Envers et l'Endroit, L'Etranger에 나타난 태양의 양면성과 삶과 죽음의 관계」, 『한국프랑스학논집』 제47집(2004) pp.221-234.</li>
      <li>Syvie Petin, 《Albert Camus sur les “Rivages” de la Méditerranée》, conférence au Forum universitaire de l’Ouest-Parisien, 2003.</li>
      <li><a href='http://www.forumuniversitaire.com/Joumela/index.php/2013-03-05-23-28-06/litterature/litterature-francaise/albert-camus/122-albert-camus-sur-les-rivages-de-la-mediterranee' target='_blank'>카뮈와 지중해의 연관성을 다른 강연 원고</a></li>
      <li>이기언, 「까뮈와 실존철학」, 『지중해지역연구』, 제7권 제1호(2005,4). : 철학자와 작가로서의 카뮈의 정체성에 관한 논문이다.</li>
      <li>Yves Millet, 「Jean Grenier critique d’art Une autre leçon de liberté</li>
      <li>예술비평가 장 그르니에 : 또 다른 자유의 방식」, 『프랑스문화예술』, Vol.54 (2015), pp.37-61. : 그르니에의 사상에 관한 논문이다.</li>
      <li>Yves Millet, 「JLa rêverie de la nature chez Jean Grenier</li>
      <li>장 그르니에에게 있어서의 자연의 몽상」, 『프랑스문화예술』, Vol.21(2007), pp.759-775. : 그르니에의 작품에 드러난 사상을 포착할 수 있는 논문 자료이다.</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p><a href='https://fr.wikipedia.org/wiki/Correspondance_Albert_Camus,_Jean_Grenier' target='_blank'>카뮈-그르니에 서한집에 대한 위키페디아 항목</a></p>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"private",
  "category":"일기, 메모",
  "author":"알베르 카뮈 Albert Camus",
  "title":"작가수첩 I Carnets I",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1962,
  "language":"프랑스어",
  "t_title":"작가수첩 1",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":1998,
  "keywords1":["어머니(mère)","부조리(absurde)","이방인(étranger)","행복(bonheur)","삶(vie)"],
  "keywords2":["일기(journal)","짧은 메모(note)","수첩(carnet)","작품 구상(pensée d'un ouvrage)","의식·인식(connaissance)"],
  "story":`<div>
    <p>카뮈가 작품을 구상하기 위하여 적어놓은 단편적 감상과 메모를 엮은 『작가수첩』은 카뮈의 내면을 잘 드러내고 있어 그의 의식을 파악하는 데에 중요한 역할을 한다.</p>
    <p>총 3권의 공책으로 이루어진 『작가수첩 1』의 기록은 1935년(당시 카뮈 나이 22살)부터 시작된다.</p>
    <br/>
    <b>공책 제 1권 : 1935년 5월 ~ 1937년 9월</b>
    <p>이 시기의 카뮈는 알제 대학에서 철학 공부를 하고 있었는데 『안과 겉』을 집필하고, 이후 ‘노동극장 Théâtre du Travail’을 창단해 활동했다. “가난하게 살아온 수년간의 세월은 어떤 감수성을 형성하기에 충분하다. 이런 특별한 경우에 있어서 아들이 그의 어머니에 대하여 품고 있는 기이한 감정이 그의 감수성 전체를 이룬다.” 공책 제 1권에는 카뮈가 다양한 작품에서 내보이는 ‘어머니’에 대한 주제가 계속해서 나타나고 있다. 또, 후에 『안과 겉』, 『행복한 죽음』, 『결혼』에 쓸 구절과 표현이 나와 있고, 『시지프 신화』, 『칼리굴라』를 예고하는 노트가 적혀있다.</p>
    <b>공책 제 2권 : 1937년 9월 ~ 1939년 4월</b>
    <p>공책 제 2권에서는 카뮈가 『행복한 죽음』과 『이방인』을 구상하고, 이 소설들을 집필하고자 노력한 흔적을 많이 발견할 수 있다. 특히, 『이방인』에 쓰인 첫 문장은 1938년에 그대로 등장한다. “오늘 엄마가 죽었다. 어쩌면 어제. 모르겠다. 양로원으로부터 전보를 한 통 받았다.”</p>
    <b>공책 제 3권 : 1939년 4월 ~ 1942년 2월</b>
    <p>전쟁이 발발한 시기, 『알제-레퓌블리캥 Alger Républicain』의 기자로 활동하던 카뮈는 알제리를 떠나 프랑스 파리로 이동한다. 하지만 파리에서 낯섦을 계속해서 느끼고, 1941년 1월 알제리 오랑으로 귀환한다. 얼마 지나지 않아서 1941년 2월 “세 가지 ‘부조리’가 완성되었다.”라는 기록을 남기는데, 이는 『이방인』, 『시지프 신화』, 『칼리굴라』의 완성을 의미한다. 또, 3권의 말미에는 비극에 관한 에세이의 아이디어 “1. 프로메테우스의 침묵 - 2. 엘리자베스 시대 사람들 - 3. 몰리에르 - 4 반항의 정신”을 통해 카뮈의 후일 작품 구상을 알아볼 수 있다.</p>
  </div>`,
  "network1":`<div>
    <p>1935-1942년 사이 카뮈가 집필한 작품들</p>
    <ul>
      <li>『행복한 죽음 La mort heureuse』</li>
      <li>『안과 겉 L'Envers et l'Endroit』</li>
      <li>『결혼 Noces』</li>
      <li>『미노타우로스 또는 오랑에서 잠시 (여름) Le Minotaure ou mla halte d'Oran (dans L'Été)』</li>
      <li>『이방인 L'Étranger』</li>
      <li>『시지프 신화 Le Mythe de Sisyphe』</li>
      <li>『아스튀리의 반항 Révolte dans les Asturies』</li>
      <li>『칼리굴라 Caligula』</li>
    </ul>
  </div>`,
  "network2":"<p>니체 Nietzsche 『우상의 황혼 Crépuscule des idoles』, 『인간적인, 너무나 인간적인 Humain, trop humain』 : 카뮈는 니체의 글에서 다양한 구절을 인용했다. </p>",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    <p>- Hiroyuki Takatsuka, «Les retouches et l'avant-textualité des Carnets d'Albert Camus», 『フランス語フランス文学研究』 No.102, 2013,  pp.153-169.</p>
    <p><a href='http://ci.nii.ac.jp/els/110009597281.pdf?id=ART0010056253&type=pdf&lang=jp&host=cinii&order_no=&ppv_type=0&lang_sw=&no=1451886555&cp=' target='_blank'>[알베르 카뮈의 퇴고과 작가수첩의 전(前)-텍스트성]</a></p>
   </div>`,
  "digital_works":`<div>
    <p>‘알베르 카뮈’ 위키백과 항목(<a href='https://ko.wikipedia.org/wiki/%EC%95%8C%EB%B2%A0%EB%A5%B4_%EC%B9%B4%EB%AE%88' target='_blank'>한국어</a>/<a href='https://fr.wikipedia.org/wiki/Albert_Camus' target='_blank'>프랑스어</a></p>
    <p><a href='https://ko.wikipedia.org/wiki/%EC%95%8C%EB%B2%A0%EB%A5%B4_%EC%B9%B4%EB%AE%88' target='_blank'>‘작가수첩’ 위키백과 항목</a></p>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/carnets_I/camus_carnets_t1.pdf' target='_blank'>원문(불어) 텍스트 파일</a></p>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"private",
  "category":"일기, 메모",
  "author":"알베르 카뮈 Albert Camus",
  "title":"작가수첩 II Carnets II",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1964,
  "language":"프랑스어",
  "t_title":"작가수첩 2",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2002,
  "keywords1":["반항(révolte)","사랑(amour)","소설(roman)","정의(justice)"],
  "keywords2":["일기(journal)","짧은 메모(note)","수첩(carnet)","작품 구상(pensée d'un ouvrage)","의식·인식(connaissance)"],
  "story":`<div>
    <p>카뮈가 작품을 구상하기 위하여 적어놓은 단편적 감상과 메모를 모아놓은 작가수첩은 카뮈의 내면을 잘 드러내고 있어 그의 의식을 파악하는 데에 중요한 역할을 한다.</p>
    <p>작가수첩 2는 총 3권의 공책으로 이루어져 있다.</p>
    <br/>
    <b>공책 제 4권 : 1942년 1월 ~ 1945년 9월</b>
    <p>“나를 죽이는 것이 아니면 무엇이든 다 나를 더욱 강하게 만든다.”라는 머리말로 시작하는 4권은 『페스트』의 저작을 위한 오랜 구상을 담고 있다. “『페스트』와 관련하여 나의 의도를 분명하게 요약할 것.”에서 보듯이 글을 쓰기 위해 카뮈가 어떻게 생각을 정리해 나갔는지 잘 알려준다. 그리고 반항에 관한 에세이 집필에 대한 언급도 하고 있다.</p>
    <br/>
    <b>공책 제 5권 : 1945년 9월 ~ 1948년 4월</b>
    <p>전쟁 직후의 5권은 “우리 시대의 유일한 문제 : 이성의 절대적인 권능을 믿지 않는 채로 세계를 변화시킬 수 있는 것인가?”라는 질문을 하며 시작한다.  그 외에 많은 질문을 자신에게 던지며 반항의 미학에 대해 개인적 생각을 정리하고 『반항적(반항하는) 인간』의 토대를 다졌다.</p>
    <br/>
    <b>공책 제 6권 : 1948년 4월 ~ 1951년 3월</b>
    <p>다른 시기에 비해 사랑이라는 단어의 사용이 많고, 사랑을 소재로 할 소설에 쓰일 구절이 많이 등장한다. 하지만 여전히 반항에 대한 카뮈의 생각은 계속된다. 『반항적(반항하는) 인간』의 초고 탈고를 알리고 작품 완성을 속박에 비유하며 카뮈는 여섯 번째 공책을 마무리 한다.</p>
    <br/>
    <p>이 시기(1942년~1951년)의 카뮈는 세계대전을 거치면서 레지스탕스 조직에 참여하기도 했으며 이념에 대한 고민을 하고, 지병인 결핵이 재발하여 고통을 겪었다. 작가수첩 2를 통해 이런 고난을 이겨내면서 이 시기에 카뮈가 작품을 어떻게 구상했는지, 어떤 성찰을 했는지를 알 수 있다.</p>
  </div>`,
  "network1":`<div>
    <ul>
      <li>『페스트』 (La Peste, 알베르 카뮈 Albert Camus, 1947)</li>
      <li>『계엄령』(L'État de siège, 알베르 카뮈 Albert Camus, 1948)</li>
      <li>『정의의 사람들』 (Les Justes, 알베르 카뮈 Albert Camus, 1950)</li>
      <li>『반항적 인간』 (L'Homme révolté, 알베르 카뮈 Albert Camus, 1951)</li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":"<p>앙드레 지드(André Gide) - 실질적인 교류를 하는 사이</p><p>스탕달(Stendhal) - 스탕달의 문장, 본문에서 자주 언급되는 인물</p>",
  "social_network":`<div>
    <p>- Hiroyuki Takatsuka (2013), Les retouches et l'avant-textualité des Carnets d'Albert Camus, フランス語フランス文学研究 No.102 pp.153-169</p>
    <p><a href='http://ci.nii.ac.jp/els/110009597281.pdf?id=ART0010056253&type=pdf&lang=jp&host=cinii&order_no=&ppv_type=0&lang_sw=&no=1451886555&cp=' target='_blank'>[알베르 카뮈의 퇴고과 작가수첩의 전(前)-텍스트성]</a></p>
   </div>`,
  "digital_works":`<div>
    <p><a href='https://fr.wikipedia.org/wiki/Carnets_(Camus)#Carnets_II_:_janvier_1942-mars_1951_.281964.29' target='_blank'>Carnets (Camus)</a></p>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/carnets_II/camus_carnets_t2.pdf' target='_blank'>원문 pdf</a></p>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"private",
  "category":"일기, 메모",
  "author":"알베르 카뮈 Albert Camus",
  "title":"작가 수첩 Ⅲ Carnets Ⅲ",
  "location":"프랑스 파리 Paris, France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1964,
  "language":"프랑스어",
  "t_title":"작가수첩 3",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["소설(roman)","기록(enregistrement)","초안(esquisse)","구상(plan)","일기(journal)"],
  "keywords2":["카뮈(Camus)","무신론(athéisme)","무신론자(athée)","인간의 정의(justice humaine)","인간의 인간성(humanité de l’homme)","인간에 대한 믿음(foi en l’homme)","휴머니스트(humaniste)"],
  "story":`<div>
    <p>카뮈가 작품을 구상하기 위하여 적어놓은 단편적 감상과 메모를 모아놓은 작가수첩은 카뮈의 내면을 잘 드러내고 있어 그의 의식을 파악하는 데에 중요한 역할을 한다.</p>
    <p>작가수첩 3은 총 3권의 공책으로 이루어져 있다.</p>
    <br/>
    <b>공책 제7권 : 1951년 3월 ~ 1954년 7월</b>
    <p>『안과 겉』의 서문을 위한 메모, 『적지와 왕국』, 『최초의 인간』 집필을 위한 메모 등 작품 구상에 대한 메모가 들어 있다. 또한 꿈 일기와 정치적 의견, 신변잡기적 메모 등 다양한 사적인 글들이 수록되어 있다.</p>
    <br/>
    <b>공책 제8권 : 1954년 8월 ~ 1958년 7월</b>
    <p>작품 구상을 위한 메모와 신변잡기적 기록들이 이어지고 있다. 공책 제7권에서와 마찬가지로 『적지와 왕국』, 『최초의 인간』에 대한 메모가 담겨 있으며 일기의 형식이 늘어난 것이 특징적이다. 그 밖에도 독서에 대한 감상, 정치적 사견, 편지 초고 등의 메모가 포함되어 있다.</p>
    <br/>
    <b>공책 제9권 : 1958년 7월 ~ 1959년 12월</b>
    <p>공책 제8권에서와 같이 일기문이 다수 수록되어 있다. 공책 제9권은 카뮈 사망 직전까지의 메모들이 수록되어 있기 때문에 카뮈의 말년의 생각과 사상들을 이해하기 위한 자료로 활용될 수 있다.</p>
  </div>`,
  "network1":`<div>
    <ul>
      <li>『안과 겉』 : 공책 제7권에는 카뮈가 이 작품의 서문을 위한 메모를 적어 두었다.</li>
      <li>「감옥에 갇힌 예술가」 : 공책 제7권에 카뮈는 오스카 와일드의 예술에 관한 단상을 기록했다. 와일드에 대한 카뮈의 비평은 『감옥에 갇힌 예술가』에서 확인할 수 있다.</li>
      <li>『적지와 왕국』 : 카뮈의 단편 소설집으로 공책 제7권에서는 이 작품을 준비하기 위한 단상들이 다수 적혀 있다.</li>
      <li>『최초의 인간』 : 카뮈의 장편 소설집으로 공책 제7권에서 이 작품을 구상하기 위한 메모들이 수록되어 있다.</li>
      <li>「이냐치오 실로네의 『빵과 포도주』」 : 공책 제8권에는 이냐치오 실로네에 대한 언급이 있다. 카뮈는 그의 소설 『빵과 포도주』에 대한 비평문을 발표한 바 있다.</li>
      <li>「장 그르니에의 『섬』 서문」 : 카뮈는 공책 제8권에서 그르니에의 『섬』 서문에 관한 생각을 기록해 두었다.</li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    <p>김병익, 「카뮈 읽기 (3)」, 『본질과 현상』, Vol.27 No.- [2012], pp.196~217. : 작가수첩 Ⅲ에 대한 해설이 담긴 글.</p>
   </div>`,
  "digital_works":`<div>
    <p><a href='http://classiques.uqac.ca/classiques/camus_albert/carnets_III/camus_carnets_t3.pdf' target='_blank'>작가수첩 Ⅲ의 불어 원문</a></p>
  </div>`,
  "legends":""
},{ /*********************************************************************************************/
  "worktype":"private",
  "category":"일기, 메모",
  "author":"알베르 카뮈 Albert Camus",
  "title":"여행일기 (Journaux de voyage) 1",
  "location":"프랑스 France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1978,
  "language":"프랑스어",
  "t_title":"여행일기 1",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["일기(journal)","여행(Voyage)","글쓰기(écriture)","미국(amérique)","도덕(morale)","바다(Mer)"],
  "keywords2":["일기(journal)","여행(voyage)","삶(vie)","사랑(amour)","휴머니티(humanité)","아이러니(ironie)"],
  "story":"<p>1946년 3월부터 5월까지의 북아메리카 여행을 기록했다.</p><p>카뮈는 기자의 자격으로 3개월에 걸쳐 미국을 여행하면서, 주로 강연이나 토론에 참여했다.  3월 10일, 종전 직후에 폐허가 된 르아브르 항구를 출발해 뉴욕 항구에 도착한 그는 미국의 발전된 모습에 놀라면서도 '구체적이고 사소한 것들' 에 관심을 갖는다.</p><p> 5월 25일, 캐나다 여행을 떠난다. 6월 21일, 카뮈는 열흘의 바다 여행을 거쳐 프랑스 보르도에 도착한다.</p>",
  "network1":`<div>
    <ul>
      <li>「뉴욕의 비 (Les pluies de New-York)」 : 북미 여행의 기록을 활용해 작품으로 다루었다.</li>
      <li>「형태와 색채 (Formes et Couleurs, 1947)」, 플레이아드 전집 2권, p.1829 참조</li>
      <li>『반항하는 인간 (L’'homme révolté)』 : 『여행일기』에서 이미 ‘반항’의 주제가 표출되었다.<br/>번역본 44p. 성스러운 것에 대한 반항인 그리스적인 사고를 다시 해보고 재창조할 것. 그러나 낭만주의자들의 성스러운 것에 대한 반항이 아니라-반항 자체가 성스러움의 한 형식이다-성스러운 것을 제자리에 갖다 놓는 것으로서의 반항.</li>
      <li>『계엄령 (L'État de siège)』 : 『여행일기』에서 언급한 ‘관료주의’에 대한 관심을 확인할 수 있다.<br/>번역본 45p. 관료주의에 대한 희곡을 한 편 쓸 것(미국에서나 다른 어느 곳에서나 마찬가지로 어리석은 관료주의).</li>
    </ul>
  </div>`,
  "network2":"-",
  "network3":"-",
  "character_network":`<div>
    <p>카뮈가 북미 여행을 하며 만난 인물들 (번역본 각주 참조)</p>
    <ul>
      <li>키아로몬테 Chiaromonte : 이탈리아의 작가 겸 비평가로 카뮈의 친구</li>
      <li>오브라이언 O’Brien : 미국에서 카뮈의 작품을 번역했다.</li>
      <li>시프린 Chiffrin : 미국의 출판업자</li>
      <li>크노프 Knopf : 미국에서 카뮈의 작품들을 펴내는 대표 출판업자</li>
      <li>리오넬 아벨 Lionel Abel : 하버드 대학에서 카뮈의 강연을 통역한 기자</li>
      <li>W. 프랭크 Waldo Frank : 카뮈와 오랫동안 서신을 교환한 미국 작가</li>
    </ul>
  </div>`,
  "social_network":`<div>
    <h2>리뷰</h2>
    <ul>
      <li>Kanes, M.. (1979), «Review of Journaux de voyage», World Literature Today, 53(2), 254–255.</li>
      <li>Detrez, C., & Camus, C. A.. (1978), «Review of JOURNAUX DE VOYAGE; FRAGMENTS D'UN COMBAT». Esprit (1940-), (18 (6)), 147–147</li>
      <li>Sarocchi, J.. (1980), «Review of Journaux de voyage Fragments d 9 un combat, 1938-1940. Alfer Républicain», Revue D'histoire Littéraire De La France, 80(3), 505–507.</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p><a href='https://fr.wikipedia.org/wiki/Journaux_de_voyage' target='_blank'>위키백과</a></p>
  </div>`,
  "legends":"<p>원래는 북미 여행을 기록한 노트와 남미 여행을 기록한 노트가 개별적으로 존재하였으나 로제 키요Roger Quillot가 두 노트를 한 권으로 편집함.</p>"
},{ /*********************************************************************************************/
  "worktype":"private",
  "category":"일기, 메모",
  "author":"알베르 카뮈 Albert Camus",
  "title":"여행일기(Journaux de voyage)2",
  "location":"프랑스 France",
  "publisher":"갈리마르 Gallimard",
  "published_year":1978,
  "language":"프랑스어",
  "t_title":"여행일기 2",
  "t_author":"김화영",
  "t_publisher":"책세상",
  "t_year":2010,
  "keywords1":["일기(journal)","병(maladie)","글쓰기(écriture)","가난(pauvreté)","휴머니티(humanité)","비관론(pessimisme)","몰이해(incompréhension)","남미(amérique du sud)","바다(Mer)","여행(voyage)"],
  "keywords2":["일기(journal)","여행(voyage)","삶(vie)","사랑(amour)","휴머니티(humanité)","아이러니(ironie)"],
  "story":"<p>1949년 6월부터 8월까지의 남아메리카 여행을 기록했다.</p><p>프랑스 외무성의 로제 세두Roger Seydoux가 카뮈에게 남미 행을 제안한다. 그는 마르세유에서 배를 타고 떠나는데 2주간의 배 여행은 「가장 가까운 바다」에서 형상화된다.</p><p>카뮈는 자신에게 '질식'의 느낌을 주는 비행기 여행에 대한 혐오를 감추지 않는다. 좋지 않은 건강 상태에서 비롯된 것으로 보인다.</p><p>여행 후, 카뮈의 건강은 더욱 악화되고 『반항하는 인간』 집필을 이어가는 것 외에는 다른 일을 하지 못하게 된다. 이 기간을 이용해 카뮈는 자신의 작품 세계의 전반을 반성한다.</p>",
  "network1":"<p>「가장 가까운 바다 (Mer au plus près)」 , 「자라나는 돌 (La pierre qui pousse)」 : 남미 여행의 기록을 활용해 작품으로 다루었다.</p><p>「가장 가까운 바다」 - 『여름 (L'Été)』, 알베르 카뮈 전집 특별판 6 참조</p><p>「자라나는 돌」 - 『적지와 왕국』에 실린 단편.</p>",
  "network2":"<p>샹포르(Chamfort), 《잠언집(Maximes)》</p>",
  "network3":"-",
  "character_network":"",
  "social_network":`<div>
    <ul>
      <li>Kanes, M.. (1979), «Review of Journaux de voyage», World Literature Today, 53(2), 254–255.</li>
      <li>Detrez, C., & Camus, C. A.. (1978), «Review of JOURNAUX DE VOYAGE; FRAGMENTS D'UN COMBAT». Esprit (1940-), (18 (6)), 147–147</li>
      <li>Sarocchi, J.. (1980), «Review of Journaux de voyage Fragments d 9 un combat, 1938-1940. Alfer Républicain», Revue D'histoire Littéraire De La France, 80(3), 505–507.</li>
      <li>Fernande Bartfeld, «Albert Camus, voyageur et conférencier : le voyage en Amérique du Sud», Lettre Modernes, 1991.</li>
    </ul>
   </div>`,
  "digital_works":`<div>
    <p><a href='https://fr.wikipedia.org/wiki/Journaux_de_voyage' target='_blank'>위키백과</a></p>
  </div>`,
  "legends":"<p>원래는 북미 여행을 기록한 노트와 남미 여행을 기록한 노트가 개별적으로 존재하였으나 로제 키요Roger Quillot가 두 노트를 한 권으로 편집함.</p>"
}
];

