$(() => {
  function view_content(title) {
    // Recover hidden rows
    $(".content section.book tr").css('display','');

    let search_result = _.filter(books, { 't_title': title });
    let book = {};
    if(search_result.length >= 1) {
      book = search_result[0];
    }

    $('.work-type').css('display','none');
    $('.submenu2 li').removeClass('active');
    switch(book.worktype) {
      case 'fiction':
        $('.fiction-only').css('display','');
        $('.submenu2 li.fiction').addClass('active');
        break;
      case 'nonfiction':
        $('.nonfiction-only').css('display','');
        $('.submenu2 li.nonfiction').addClass('active');
        break;
      case 'private':
        $('.private-only').css('display','');
        $('.submenu2 li.private').addClass('active');
        break;
    }

    // Hide submenu 3
    $('.submenu3').css('visibility','hidden');

    for(var prop in book) {
      let str = "";
      if(_.isArray(book[prop])) {
        str = book[prop].join(", ");
      } else {
        str = book[prop];
      }
      $(".content section.book ."+prop).html(str);

      // Remove empty row
      if(!str || _.isString(str) && (str.trim() == "-" || str.trim() == "")) {
        //console.log("Removed: "+prop);
        $(".content section.book tr:has(td."+prop+")").css('display','none');
      }
    }

    // Clean previous chart image
    $('.content section.book img.chart').attr('src', '');
    if(book.character_net) {
      let chart = "";
      if(book.character_net.chart) {
        chart = book.character_net.chart;
      }

      let loader = $("<img>")
        .attr('src', '../assets/character-network-chart/'+book.character_net.chart)
        .on('load', function() {
          $('.content section.book img.chart').attr('src', $(this).attr('src'));
        });

      let character_net = "";

      for(var c in book.character_net.character) {
        let who = c;
        let what = book.character_net.character[c];

        character_net += "<li>"+who+": "+what+"</li>";
      }

      $('.content section.book .characters').empty().append($(character_net));
    }

    // Adjust footer position
    //$('.footer').css('top', $('hr.bottom-anchor').css('top'));
  }

  const sections_max_height =  _.max($('.content section').map(function() { return $(this).height(); }));
  $('.content').css('height', sections_max_height);

  $('.submenu2').on('mouseover', function() {
    $('.submenu3').css('visibility','visible');
  });

  $('.banner, .menu, .content').on('mouseover', function() {
    $('.submenu3').css('visibility','hidden');
  });

  $(".submenu3 .col").on('mouseover', function() {
    const section = $(this).attr('data-section');
    $('.submenu2 li').removeClass('hover')
    $('.submenu2 li.'+section).addClass('hover')
  }).on('mouseout', function() {
    $('.submenu2 li').removeClass('hover')
  });

  $('.submenu3').on('click', function() {
     $('.submenu3').css('visibility','hidden');
  });

  // Content Navigation
  function getCurrentBookIndex() {
    let current_route = $(location).attr('hash') || '#이방인';
    let title = decodeURIComponent(current_route.substring(1));
    let search_result = _.filter(books, { 't_title': title });
    if(search_result.length >= 1) {
      return _.indexOf(books, search_result[0]);
    }

    return -1; // error
  }

  function gotoPrev() {
    const index = getCurrentBookIndex();

    if(index > 0) {
      location.hash = encodeURIComponent(books[index-1].t_title);
      // Animate effect
      $(".content section.book")
        .addClass('animated fadeInLeft')
        .one('animationend', function() {
          $(this).removeClass('animated fadeInLeft');
        });
    }
  }

  function gotoNext() {
    const index = getCurrentBookIndex();

    if(index < books.length - 1) {
      location.hash = encodeURIComponent(books[index+1].t_title);
      // Animate effect
      $(".content section.book")
        .addClass('animated fadeInRight')
        .one('animationend', function() {
          $(this).removeClass('animated fadeInRight');
        });
    }
  }

  $('section.navigation .goto-previous').on('click', gotoPrev);
  $('section.navigation .goto-next').on('click', gotoNext);
  $(window).on('keydown', function(e) {
    switch(e.keyCode) {
      case 37: // Left
        e.preventDefault();
        gotoPrev();
        break;
      case 39: // Right
        e.preventDefault();
        gotoNext();
        break;
    }
  });

  $(window).on('hashchange', function() {
    let route = $(location).attr('hash') || '#이방인';
    route = route.substring(1);
    view_content(decodeURIComponent(route));
  });

  $(window).trigger('hashchange');
});