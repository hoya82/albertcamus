window.parseQueryString = function() {
  let m = {};

  _.each(location.search.substring(1).split('&'), (i) => {
    let ii = i.split('=');
    let name = ii[0].trim();
    let value = ii[1].trim();

    m[name] = value;
  });

  return m;
}

$(() => {
  $(".submenu-item").on('mouseover', function() {
    $(".mainmenu-item").removeClass("hover");
    $(".mainmenu-item."+$(this).attr("data-parent")).addClass("hover");
  })
  .on('mouseout', function() {
    $(".mainmenu-item."+$(this).attr("data-parent")).removeClass("hover");
  });

  $(".submenu2 li").on('mouseover', function() {
    $(this).addClass('hover');
  }).on('mouseout', function() {
    $(this).removeClass('hover');
  });

  $('.top-floater a').on('click', function(e) {
      e.preventDefault();

      $('html, body').animate({
          scrollTop: 0
      }, 500);
  });
});